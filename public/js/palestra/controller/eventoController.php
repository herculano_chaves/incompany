<?php 
	include_once("components/crud.php");
	include_once("appController.php");
	class Evento extends Crud {
		var $nome;
		var $email;
		var $id;
		var $pessoa;
		var $category;
		
		function authUser($email, $pass, $valid=NULL){
			if(isset($valid)):
			$q = Crud::_read('customers',array('email="'.mysql_real_escape_string($email).'" and senha = "'.md5($pass).'" and valido="1" '));	
			else:
			$q = Crud::_read('customers',array('email="'.mysql_real_escape_string($email).'" and senha = "'.md5($pass).'" '));	
			endif;
			
			$u = mysql_fetch_object($q);
			if(isset($u->id)):
				return true;	
			else:
				return false;
			endif;
		}
		function forget($email){
			$q = Crud::_read('customers',array('email="'.mysql_real_escape_string($email).'" and valido="1" '));
			$u = mysql_fetch_object($q);
			if(isset($u->id)):
				$this->id = $u->id;
				$this->email = $u->email;
				return true;	
			else:
				return false;
			endif;
		}
		function authUserHash($id,$hash){
			$q = Crud::_read('customers',array('MD5(id)="'.$id.'" and hash = "'.$hash.'" and valido = "0" '));
			$u = mysql_fetch_object($q);
			if(isset($u->id)):
				Crud::_update('customers',array('valido = "1" WHERE id="'.$u->id.'"'));
				$_SESSION['user_site'] = $u->id;
				Site::redirect('projetos');
			else:
				Site::redirect('');
			endif;
		}
		//function getByID($id){}
		function login($email, $pass){
			
			if($this->authUser($email, $pass, '1') == true):
				$q = Crud::_read('customers',array('email="'.mysql_real_escape_string($email).'" and senha = "'.md5($pass).'" '));
				$u = mysql_fetch_object($q);
				$_SESSION['user_site'] = $u->id;
				Site::redirect('projetos');
			elseif($this->authUser($email, $pass) == true):
				Site::redirect('not-active?email='.$email);
			else:
				$_SESSION['warn'] = "<b>X</b> Login ou senha incorretos.";
				Site::redirect('#erro');
			endif;
		}
		function logout(){
			session_destroy();	
			Site::redirect('');
		}
		function checkAuth(){
			if(isset($_SESSION['user_site'])):
				return $_SESSION['user_site'];
			else: 
				Site::redirect(''); 
			endif;
		}	
		function set($session=NULL){
			//var_dump($session);
			$q = Crud::_read('customers',array('id="'.$session.'"'));
			$u = mysql_fetch_object($q);
			$this->id = $u->id;
			$this->nome = $u->nome;
			$this->email = $u->email;
			$this->category = $u->id_category;
			$this->pessoa = $u->id_pessoa;
		}
		function sign($categoria, $pessoa, $email, $senha, $nome, $cpf, $profissao, $cep, $endereco, $complemento, $bairro, $cidade, $estado, $ddd_tel, $tel, $ddd_cel,$cel, $razao, $fantasia, $cnpj){
			$senha = md5($senha);
			$hash = uniqid(rand());
			
			$create = Crud::_create("customers","'NULL','$categoria','$pessoa','$email','$profissao','$cpf','$senha','$nome','$cep','$endereco','$complemento','$bairro','$cidade','$estado','$ddd_tel','$tel','$ddd_cel','$cel','$razao','$fantasia','$cnpj','$hash','0'");
			if($create):
				return true;
			else:
				return false;
			endif;
		}
		function Edit($params){
			$edit = Crud::_update("knaufevento_inscricoes",$params);
			if($edit): return true; else: return false; endif;
		}
		function show($params=NULL){
			$user=array();
			$read = Crud::_read("knaufevento_inscricoes",$params);
			while($u = mysql_fetch_array($read)):
				$user[] = $u;
			endwhile;
			return $user;
		}
		function getPesquisa($params=NULL){
			$user=array();
			$read = Crud::_read("knaufevento_pesquisa",$params);
			while($u = mysql_fetch_array($read)):
				$user[] = $u;
			endwhile;
			return $user;
		}
		function getPesquisaResponse($pergunta, $user_res){
			switch ($pergunta) {
				case 1:
					if($user_res == '1'):
						$r = 'apresentação das ilhas';
					elseif($user_res == '2'):
						$r = 'workshops';
					elseif($user_res == '3'):
						$r = 'mini cursos';
					else:
						$r = 'todos';
					endif;
				break;
				case 2:
					if($user_res == '1'):
						$r = 'Sim das ilhas';
					else:
						$r = $user_res;
					endif;
				break;
				case 3:
					if($user_res == '1'):
						$r = 'Sim das ilhas';
					else:
						$r = $user_res;
					endif;
				break;
				case 4:
					if($user_res == '1'):
						$r = 'nunca trabalhei';
					elseif($user_res == '2'):
						$r = 'trabalho eventualmente';
					else:
						$r = 'trabalho só com drywall';
					endif;
				break;
				case 5:
					if($user_res == '1'):
						$r = 'todas atendem';
					else:
						$r = $user_res;
					endif;
				break;
				case 6:
					if($user_res == '1'):
						$r = 'muito bons';
					elseif($user_res == '2'):
						$r = 'bons';
					elseif($user_res == '3'):
						$r = 'regulares';
					elseif($user_res == '4'):
						$r = 'ruins';
					elseif($user_res == '5'):
						$r = 'muito ruins';
					else:
						$r = 'Não tenho como avaliar';
					endif;
				break;
				case 7:
					$r = $user_res;
				break;
				case 8:
					$r = $user_res;
				break;
				case 9:
					$r = $user_res;
				break;
				case 10:
					if($user_res == '1'):
						$r = 'através do site Knauf';
					elseif($user_res == '2'):
						$r = 'através de e-mail';
					elseif($user_res == '3'):
						$r = 'por indicação';
					elseif($user_res == '4'):
						$r = 'anúncio em rádio';
					elseif($user_res == '5'):
						$r = 'anúncio em publicação';
					else:
						$r = 'redes sociais';
					endif;
          
				break;
				
				default:
					# code...
					break;
			}
			return $r;
		}
		function getEventos($params=NULL){
			$user=array();
			$read = Crud::_read("knaufevento",$params);
			while($u = mysql_fetch_array($read)):
				$user[] = $u;
			endwhile;
			return $user;
		}
		function getHorario($id_evento){
			$read = Crud::_read("knaufevento_horarios",array(' id_evento="'.$id_evento.'" AND id != "6" '));
			if($read):
			while($user = mysql_fetch_array($read)){
				$u[] = $user;	
			}
				return $u;
			else:
				return false;
			endif;
		}
		function checkPresenceOf($id=NULL){
			$read = Crud::_read("knaufevento_inscricoes",array(' knaufEvento_presence = "1" AND knaufEvento_id = "'.$id.'" '));
			$u = mysql_fetch_object($read);
			if(isset($u->knaufEvento_id)):
				return true;	
			
			else:
				return false;
			endif;
		}
		function delete($id=NULL){
			if(Crud::_delete("knaufevento_inscricoes",$id)):
				return true;
			else:
				return false;
			endif;
		}
		function sendEmail($uid,$template=NULL){
				include_once("../vendors/sendEmail/class.phpmailer.php");
				include_once("../vendors/sendEmail/class.smtp.php");
				$user = $this->show(array("knaufevento_id='".$uid."'"));
				
				$mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch


				$mail->IsSMTP(); // telling the class to use SMTP
				
				$mail->SMTPAuth   = true;                  // enable SMTP authentication
				$mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
				$mail->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server
				$mail->Port       = 465;                   // set the SMTP port for the GMAIL server
				$mail->Username   = "knaufcleaneo@gmail.com";  // GMAIL username
				$mail->Password   = "concursocleaneo";            // GMAIL password

				#Define o remetente 
				$mail->SetFrom("knaufcleaneo@gmail.com", "Knauf");

				
				$mail->CharSet = 'utf-8';
				
				#Define o remetente 
				

				
				# Define os destinatário(s) 
				$mail->AddAddress($user[0]['knaufEvento_email'], $user[0]['knaufEvento_nome']);
				
				# Define os dados técnicos da Mensagem 
				$mail->IsHTML(true); // Define que o e-mail será enviado como HTML
				
				
				if($template == 'agradecimento'):
						
						include_once('../vendors/sendEmail/agradecimento.php');

						$mail->Body = $body;
					
						# Texto e Assunto 
						$mail->Subject  = "Knauf Agradecimento"; // Assunto da mensagem
						
						$enviado = $mail->Send();
				endif;
				
			
				if($enviado): return true; else: return false; endif;
		}
		function getCategory($catid){
			if($catid == '1'):return "Arquiteto"; else: return "Instalador"; endif;	
		}
		function getType($typid){
			if($typid == '1'):return "F&iacute;sica"; else: return "Jur&iacute;dica"; endif;	
		}
		function getField($field){
			$res = NULL;
			$q = Crud::_read('customers',array('id = "'.$this->id.'" '));
			$res = mysql_fetch_object($q);
			return $res->$field;
		}
	}
?>