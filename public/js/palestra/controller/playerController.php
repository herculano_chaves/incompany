<?php 

	include_once("components/crud.php");

	include_once("appController.php");

	class Player extends Crud {

		var $nome;

		var $email;

		var $id;

		var $pessoa;

		var $category;

		
		function getAreas($params=null){
			$user = array();
			$read = Crud::_read('areas',$params);
			while($u = mysql_fetch_array($read)):

				$user[] = $u;

			endwhile;
			if(empty($user)):
				return false;
			else:	
				return $user;
			endif;
		}
		function getEnterprise($params=null){
			$user = array();
			$read = Crud::_read('enterprises',$params);
			while($u = mysql_fetch_array($read)):

				$user[] = $u;

			endwhile;

			if(empty($user)):
				return false;
			else:	
				return $user;
			endif;

		}
		function importFile($arr=null, $enterprise=null, $area = null){

			$now = date('Y-m-d h:i:s');
			$create = Crud::_create('players',"'NULL', '".$enterprise."', '".$arr[0]."','','".$arr[1]."','','','".$area."', '0', '".$now."', '".$now."' ");
			if($create):

				return true;

			else:

				return false;

			endif;
		}
		function authUser($email, $pass, $valid=NULL){

			if(isset($valid)):

			$q = Crud::_read('players',array('email="'.mysql_real_escape_string($email).'" and senha = "'.md5($pass).'" and valido="1" '));	

			else:

			$q = Crud::_read('players',array('email="'.mysql_real_escape_string($email).'" and senha = "'.md5($pass).'" '));	

			endif;

			

			$u = mysql_fetch_object($q);

			if(isset($u->id)):

				return true;	

			else:

				return false;

			endif;

		}

		function forget($email){

			$q = Crud::_read('players',array('email="'.mysql_real_escape_string($email).'" and valido="1" '));

			$u = mysql_fetch_object($q);

			if(isset($u->id)):

				$this->id = $u->id;

				$this->email = $u->email;

				return true;	

			else:

				return false;

			endif;

		}

		function authUserHash($id,$hash){

			$q = Crud::_read('players',array('MD5(id)="'.$id.'" and hash = "'.$hash.'" and valido = "0" '));

			$u = mysql_fetch_object($q);

			if(isset($u->id)):

				Crud::_update('players',array('valido = "1" WHERE id="'.$u->id.'"'));

				$_SESSION['user_site'] = $u->id;

				Site::redirect('projetos');

			else:

				Site::redirect('');

			endif;

		}

		//function getByID($id){}

		function login($email, $pass){

			

			if($this->authUser($email, $pass, '1') == true):

				$q = Crud::_read('players',array('email="'.mysql_real_escape_string($email).'" and senha = "'.md5($pass).'" '));

				$u = mysql_fetch_object($q);

				$_SESSION['user_site'] = $u->id;

				Site::redirect('projetos');

			elseif($this->authUser($email, $pass) == true):

				Site::redirect('not-active?email='.$email);

			else:

				$_SESSION['warn'] = "<b>X</b> Login ou senha incorretos.";

				Site::redirect('#erro');

			endif;

		}

		function logout(){

			session_destroy();	

			Site::redirect('');

		}

		function checkAuth(){

			if(isset($_SESSION['user_site'])):

				return $_SESSION['user_site'];

			else: 

				Site::redirect(''); 

			endif;

		}	

		function set($session=NULL){

			//var_dump($session);

			$q = Crud::_read('players',array('id="'.$session.'"'));

			$u = mysql_fetch_object($q);

			$this->id = $u->id;

			$this->nome = $u->nome;

			$this->email = $u->email;

			$this->category = $u->id_category;

			$this->pessoa = $u->id_pessoa;

		}

		function sign($categoria, $pessoa, $email, $senha, $nome, $cpf, $profissao, $cep, $endereco, $complemento, $bairro, $cidade, $estado, $ddd_tel, $tel, $ddd_cel,$cel, $razao, $fantasia, $cnpj){

			$senha = md5($senha);

			$hash = uniqid(rand());

			

			$create = Crud::_create("players","'NULL','$categoria','$pessoa','$email','$profissao','$cpf','$senha','$nome','$cep','$endereco','$complemento','$bairro','$cidade','$estado','$ddd_tel','$tel','$ddd_cel','$cel','$razao','$fantasia','$cnpj','$hash','0'");

			if($create):

				return true;

			else:

				return false;

			endif;

		}
		function create($post){
			$now = date('Y-m-d h:i:s');
			$create = Crud::_create("players","'NULL', '".$post['enterprise']."', '".$post['name']."','','".$post['email']."','','','".$post['area']."','0', '".$now."', '".$now."' ");
			if($create):

				return true;

			else:

				return false;

			endif;
		}

		function Edit($params){

			$edit = Crud::_update("players",$params);

			if($edit): return true; else: return false; endif;

		}

		function show($params=NULL){

			$user=array();

			$read = Crud::_read("players",$params);

			while($u = mysql_fetch_array($read)):

				$user[] = $u;

			endwhile;

			return $user;

		}

		function getPesquisa($params=NULL){

			$user=array();

			$read = Crud::_read("knaufevento_pesquisa",$params);

			while($u = mysql_fetch_array($read)):

				$user[] = $u;

			endwhile;

			return $user;

		}

	

		function getEventos($params=NULL){

			$user=array();

			$read = Crud::_read("knaufevento",$params);

			while($u = mysql_fetch_array($read)):

				$user[] = $u;

			endwhile;

			return $user;

		}

		function getHorario($id_evento){

			$read = Crud::_read("knaufevento_horarios",array(' id_evento="'.$id_evento.'" AND id != "6" '));

			if($read):

			while($user = mysql_fetch_array($read)){

				$u[] = $user;	

			}

				return $u;

			else:

				return false;

			endif;

		}

		function checkPresenceOf($id=NULL){

			$read = Crud::_read("players",array(' knaufEvento_presence = "1" AND knaufEvento_id = "'.$id.'" '));

			$u = mysql_fetch_object($read);

			if(isset($u->knaufEvento_id)):

				return true;	

			

			else:

				return false;

			endif;

		}

		function delete($id=NULL){

			if(Crud::_delete("players",$id)):

				return true;

			else:

				return false;

			endif;

		}

		function sendEmail($uid,$template=NULL){

				include_once("../vendors/sendEmail/class.phpmailer.php");

				include_once("../vendors/sendEmail/class.smtp.php");

				$user = $this->show(array("id='".$uid."'"));

	
				$mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch

				$mail->IsSMTP(); // telling the class to use SMTP

				
				$mail->SMTPAuth   = true;                  // enable SMTP authentication
				$mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
				$mail->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server
				$mail->Port       = 465;                   // set the SMTP port for the GMAIL server
				$mail->Username   = "desafiovirtualdizain@gmail.com";  // GMAIL username
				$mail->Password   = "9n6sdi2e";            // GMAIL password

				#Define o remetente 
				$mail->SetFrom("desafiovirtualdizain@gmail.com", "Desafio Virtual");
				$mail->CharSet = 'utf-8';

				# Define os destinatário(s) 

				$mail->AddAddress($user[0]['email'], $user[0]['name']);

				

				# Define os dados técnicos da Mensagem 

				$mail->IsHTML(true); // Define que o e-mail será enviado como HTML

				

				

				if($template == 'convite'):

						

						include_once('../vendors/sendEmail/facaparte.php');



						$mail->Body = $body;

					

						# Texto e Assunto 

						$mail->Subject  = "Desafio Virtual - Faça parte deste time!"; // Assunto da mensagem

						

						$enviado = $mail->Send();

				endif;

				

			

				if($enviado): return true; else: return false; endif;

		}

		function getCategory($catid){

			if($catid == '1'):return "Arquiteto"; else: return "Instalador"; endif;	

		}

		function getType($typid){

			if($typid == '1'):return "F&iacute;sica"; else: return "Jur&iacute;dica"; endif;	

		}

		function getField($field){

			$res = NULL;

			$q = Crud::_read('players',array('id = "'.$this->id.'" '));

			$res = mysql_fetch_object($q);

			return $res->$field;

		}

	}

?>