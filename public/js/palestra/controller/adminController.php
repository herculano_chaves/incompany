<?php 

	include_once("components/crud.php");

	//include_once("appController.php");

	include_once("controller/playerController.php");

	class Admin extends Player{

		var $id;

		var $username;

		var $role;

		var $id_enterprise;

		function checkAuth(){

			if(isset($_SESSION['admin_user'])):

				return $_SESSION['admin_user'];

			else: 

				Site::redirect('admin'); 

			endif;

		}	

		function getAlbum($id_evento){

			$u=NULL;

			$read = Crud::_read("knaufevento_albuns",array(' id_evento="'.$id_evento.'" '));

				if($read):

			while($user = mysql_fetch_array($read)){

				$u[] = $user;	

			}

				return $u;

			else:

				return false;

			endif;

		}

		function set($session=NULL){

			$q = Crud::_read('users',array('id="'.$session.'"'));

			$u = mysql_fetch_object($q);

			$this->id = $u->id;

			$this->username = $u->username;

			$this->role = $u->role;

			$this->id_enterprise = $u->id_enterprise;

		}

		function login($login,$pass,$ip=NULL,$cookie=NULL, $id_enterprise = NULL){

			$CB = $this->checkBrute($ip,$cookie);

			if($CB == false):

				$q = Crud::_read("users",array(' username="'.mysql_real_escape_string($login).'" and password = "'.md5($pass).'" '));

				$u = mysql_fetch_object($q);

				if(isset($u->id)):

					Crud::_update("login_attempts",array('times = "0" WHERE ip="'.mysql_real_escape_string($ip).'" and cookie = "'.$cookie.'" '));

					$_SESSION['admin_user'] = $u->id;

					Site::redirect('dashboard');

				else:

					$_SESSION['warn'] = "Login ou senha incorretos";

					Site::redirect('');

				endif;

			else:

				$_SESSION['warn'] = "Mais de 3 tentativas de login, favor tente mais tarde";

				Site::redirect('');

			endif;

			//echo $_SESSION['warn'];

		}

		function checkBrute($ip,$cookie){
			
			$q = Crud::_read("login_attempts",array(' ip="'.mysql_real_escape_string($ip).'" and cookie = "'.$cookie.'" '));

			$temp = mysql_fetch_object($q);

			if(isset($temp->id)):

				Crud::_update("login_attempts",array('times = "'.($temp->times + 1).'" WHERE id="'.$temp->id.'" '));
				$n = $temp->times;

			else:

				Crud::_create("login_attempts","NULL,'$ip','$cookie','1'");
			$n = 0;

			endif;

			

			if($n>2): return true; else: return false; endif;

		}

		function show($params=NULL){

			$users=NULL;

			$read = Crud::_read("users",$params);

			while( $res = mysql_fetch_array($read)){

				$users[] = $res;	

			}

			return $users;

		}


		function checkUser($login=NULL){

			$u = $this->show(array('login="'.$login.'"'));

			if(isset($u[0]['id'])):

				return true;

			else:

				return false;

			endif;

		}

		function Update($params){

			$edit = Crud::_update("users",$params);

			if($edit): return true; else: return false; endif;	

		}



		function Delete($id=NULL){

			if(Crud::_delete("users",array('MD5(id)="'.$id.'"'))==true):return true; else: return false; endif;	

		}

		function getType($type){

			$res=NULL;

			switch($type){

				case 1:

					$res = "Administrador";

				break;

				case 2:

					$res = "Jurado categoria Arquiteto";

				break;

				case 3:

					$res = "Jurado categoria Instalador";

				break;

				default;

			}	

			return $res;

		}

		function logout(){

			session_destroy();

			Site::redirect('');

		}

	}

?>