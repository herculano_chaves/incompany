<?php 
	include_once("components/crud.php");
	include_once("appController.php");
	class Stats{
		function check($id){
			$v = Crud::_read("projects",array('id = "'.$id.'"'));
			if($v): return true; else: return false; endif;
		}
		function create($id_costumer,$id_category,$id_pessoa,$nome,$cep,$endereco,$bairro,$cidade,$estado,$ddd_tel,$tel,$contato,$status){
			$c = Crud::_create("projects","NULL,'$id_costumer','$id_category','$id_pessoa','$nome','$cep','$endereco','$bairro','$cidade','$estado','$ddd_tel','$tel','$contato',$status");
			if($c): return true; else: return false; endif;
		}
		function setDate($d,$sep=NULL,$sep_res=NULL){
			if(isset($sep)):
				$date = explode($sep,$d);
				return $date[2].$sep_res.$date[1].$sep_res.$date[0];
			else:
				$date = explode('/',$d);
				return $date[2].'-'.$date[1].'-'.$date[0];
			endif;
			
		}
		function Search($from=NULL,$to=NULL,$by=NULL){
			//real dates
			$rdf=$this->setDate($from);
			$rdt=$this->setDate($to);
			switch($by):
				case 1:
					$sql = "SELECT  DATE(data_agendamento) Data, COUNT(DISTINCT id) Quantidade FROM  pacientes WHERE data_agendamento >= '".$rdf."' AND data_agendamento <= '".$rdt."' GROUP   BY  DATE(data_agendamento)";
					$q = mysql_query($sql);
					while($r = mysql_fetch_array($q)):
						$res[]=$r;
					endwhile;
					return $res;
				break;
				case 2:
				$sql="SELECT p.id_medico, m.id, m.nome, m.crm, m.sobrenome, m.cidade, m.estado, DATE( p.data_agendamento ) Data, COUNT( DISTINCT p.id ) Quantidade
FROM pacientes p
INNER JOIN medicos m ON ( p.id_medico = m.id ) 
WHERE p.data_agendamento >= '".$rdf."' AND p.data_agendamento <= '".$rdt."' GROUP BY p.id_medico";
				
					$q = mysql_query($sql);
					while($r = mysql_fetch_array($q)):
						$res[]=$r;
					endwhile;
					return $res;
				break;
				case 3:
				$sql = "SELECT m.estado, m.cidade, COUNT(DISTINCT p.id) Quantidade, DATE(p.data_agendamento) Data  FROM pacientes p, medicos m WHERE  p.data_agendamento >= '".$rdf."' AND p.data_agendamento <= '".$rdt."' and p.id_medico=m.id GROUP   BY DATE(p.data_agendamento) ORDER BY m.estado";
					$q = mysql_query($sql);
					while($r = mysql_fetch_array($q)):
						$res[]=$r;
					endwhile;
					return $res;
				break;
				case 4:
				$sql = "SELECT DATE(data_cadastro) Data, COUNT(DISTINCT id) Quantidade FROM  medicos  WHERE  data_cadastro >= '".$rdf."' AND data_cadastro <= '".$rdt."' GROUP   BY DATE(data_cadastro)";
					$q = mysql_query($sql);
					while($r = mysql_fetch_array($q)):
						$res[]=$r;
					endwhile;
					return $res;
				break;
				case 5:
				$sql = "SELECT * FROM medicos";
					$q = mysql_query($sql);
					while($r = mysql_fetch_array($q)):
						$res[]=$r;
					endwhile;
					return $res;
				break;
				default;
			endswitch;
		}
		function Show($params=NULL){
			$projects=NULL;
			$c = Crud::_read("projects",$params);
			while( $res = mysql_fetch_array($c)){
				$projects[] = $res;	
			}
			return $projects;
		}
		function edit($params=NULL){
			$e = Crud::_update("projects",$params);
			if($e): return true; else: return false; endif;
		}
		function Delete(){
			
		}
		function Last(){
			$v = Crud::_order("projects","id","desc","1");
			$res = mysql_fetch_object($v);
			return $res;
		}
		function counter($id,$status=NULL){
			if(isset($status)):
			$n = Crud::_counter("projects",array('id_customer = "'.$id.'" and status = "'.$status.'" and status <> "3"'));
			else:
			$n = Crud::_counter("projects",array('id_customer = "'.$id.'" and status <> "3"'));
			endif;
			return $n;
		}
		function status($id, $status=NULL){
			$n = Crud::_read("projects",array('id = "'.$id.'" and status = "'.$status.'"'));
			$res = mysql_fetch_object($n);
			if(isset($res->id)): return true; else: return false; endif;
		}
		function getThumb($id){
			$q = Crud::_read("archives",array('id_project = "'.$id.'" and id_type = "1" ORDER BY id DESC LIMIT 1'));
			$t = mysql_fetch_array($q);
			
			return $t;
		}
		function getThumbs($id){
			$arr_thumb = NULL;
			$q = Crud::_read("archives",array('id_project = "'.$id.'" and id_type = "1" '));
			while($t = mysql_fetch_array($q)):
				$arr_thumb[] = $t;
			endwhile;
			return $arr_thumb;
		}
		function getFile($id,$type=NULL){
			$q = Crud::_read("archives",array('id_project = "'.$id.'" and id_type = "'.$type.'" '));
			$t = mysql_fetch_array($q);
			return $t;	
		}
	
		function maskNumber($id,$cat){
				if($cat=='1'){
					$num = '125.000.000.'.$id;
				}else{
					$num = '157.000.000.'.$id;
				}
				return $num;
		}
		function getCustomer($uid){
			$q=Crud::_read("customers",array('id="'.$uid.'" '));	
			$u = mysql_fetch_array($q);
			return $u;
		}
		function slug($id){
			$q=Crud::_read("projects",array('id="'.$id.'" '));	
			$p = mysql_fetch_array($q);	
			
			$res = Site::slug($p['nome']);
			return $res;
		}
		function setVoted($id){
			$p = $this->Show(array("id='".$id."'"));
			if(empty($p[0]['voted']) or $p[0]['voted']=='0'):
				$set = $this->edit(array("voted = '1' WHERE id='".$id."'"));
			else:
				$set = $this->edit(array("voted = '".($p[0]['voted'] + 1)."' WHERE id='".$id."'"));
			endif;
			if($set==true):
				return true;
			else:
				return false;
			endif;
		}
		function getRanking($id){
			$q=Crud::_read("projects_criteria",array('id_project="'.$id.'" '));
			while($p = mysql_fetch_array($q)):
				$arr[] = $p['points'];
			endwhile;
			if($this->edit(array("points = '".array_sum($arr)."' WHERE id='".$id."' "))==true):
				return array_sum($arr);
			else: return false;
			endif;
		}
		function checkSelection($id_project, $id_user){
			$q=Crud::_read("projects_criteria",array('id_project="'.$id_project.'" AND id_user="'.$id_user.'" '));
			$n = mysql_num_rows($q);
			if($n >= '1'):
				return true;
			else:
				return false;
			endif;
		}
		function getHistory($pid, $uid){
			$q=Crud::_read("projects_criteria",array('id_project="'.$pid.'"AND id_user="'.$uid.'" '));	
			$j = mysql_fetch_array($q);
			foreach($j as $val){
				$q1=Crud::_read("project_rules",array('id="'.$val['id_project_optio']	.'"'));
				$r1 = mysql_fetch_array($q1);
				$q1=Crud::_read("project_rules_items",array('id="'.$val['id_project_item']	.'"'));
				$r1 = mysql_fetch_array($q1);
			}
		}
		function sendEmail($uid,$pid,$template=NULL){
				include_once("vendors/sendEmail/class.phpmailer.php");
				include_once("vendors/sendEmail/class.smtp.php");
				$project = $this->show(array("id='".$pid."'"));
				
				$u = Crud::_read("customers",array('id = "'.$uid.'" '));
				$user = mysql_fetch_array($u);
				
				$mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch

				$mail->IsSMTP(); // telling the class to use SMTP
				
				$mail->SMTPAuth   = true;                  // enable SMTP authentication
				$mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
				$mail->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server
				$mail->Port       = 465;                   // set the SMTP port for the GMAIL server
				$mail->Username   = "knaufcleaneo@gmail.com";  // GMAIL username
				$mail->Password   = "concursocleaneo";            // GMAIL password

				#Define o remetente 
				$mail->SetFrom("knaufcleaneo@gmail.com", "Knauf");

				
				$mail->CharSet = 'utf-8';
				
				#Define o remetente 
				
				
				
				# Define os destinat�rio(s) 
				$mail->AddAddress($user['email'], $user['nome']);
				
				# Define os dados t�cnicos da Mensagem 
				$mail->IsHTML(true); // Define que o e-mail ser� enviado como HTML
				
			if($template =='envio-projeto'):
				include("vendors/sendEmail/email_envio.php");
					# Texto e Assunto 
						$mail->Subject  = "Envio de projeto sucedido!"; // Assunto da mensagem
						$mail->Body = $body;
						
						$enviado = $mail->Send();
				endif;
				if($template == 'esc-projeto'):
				include("vendors/sendEmail/email_esc.php");
					# Texto e Assunto 
						$mail->Subject  = "Exclus�o de projeto confirmado"; // Assunto da mensagem
						$mail->Body = $body;
						
						$enviado = $mail->Send();
				
				endif;
				if($enviado): return true; else: return false; endif;
	}
		function serial($arr, $char=NULL){
		if(isset($char)):
		$r = implode($char,$arr);
		else:
		$r = implode(' ',$arr);
		endif;
		return $r;
	}
	}
?>