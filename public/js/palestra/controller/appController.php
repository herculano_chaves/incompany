<?php

	ob_start();

	class Site{

		function redirect($page){

			header('Location:'.ROOT.'/'.$page);	

		}	
	function ConverterBR($data, $justdate=false) {
        if($justdate==false){
            list($dat,$hor) = explode(" ", $data);
            list($a, $m, $d) = explode("-", $dat);
        }else{
            list($a, $m, $d) = explode("-", $data);
        }
        
        
        return "$d/$m/$a";
    }
    function ConverterUS($data) {
        list($d, $m, $a) = explode("/", $data);
        return "$a-$m-$d";
    }
	function slug($text)

		{ 

		  // replace non letter or digits by -

		  $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

		

		  // trim

		  $text = trim($text, '-');

		

		  // transliterate

		  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

		

		  // lowercase

		  $text = strtolower($text);

		

		  // remove unwanted characters

		  $text = preg_replace('~[^-\w]+~', '', $text);

		

		  if (empty($text))

		  {

			return 'n-a';

		  }

		

		  return $text;

		}

		static function flash($msg=NULL,$session="flash"){

			$_SESSION[$session] = $msg;

			return $_SESSION[$session];

		}

		

		static function get_flash($session="flash"){

			if(isset($_SESSION[$session])): 

				return $_SESSION[$session]; 

			else: 

				return false; 

			endif; 	

			 

		}

   static function end_flash($session="flash"){

      unset($_SESSION[$session]);

    }

     static function destroy_flash(){

      session_destroy();

    }

	   static function upload($file, $w = null, $h=nulll){
	    	require_once('../vendors/uploads/class.upload.php');

	    	$image = new Upload($file);
			$image->file_safe_name = true;
			$image->image_resize          = true;
			$image->image_ratio_crop      = true;
			if(isset($w) and isset($h)){
				$image->image_y               = $h;
				$image->image_x               = $w;
			}else{
				$image->image_y               = 50;
				$image->image_x               = 50;
			}
			
			$image->process('../archives');
			if($image->processed): 
				$image->image_resize          = true;
				$image->image_ratio_crop      = true;
				$image->image_y               = 50;
				$image->image_x               = 50;
				$image->process('../archives/thumbs');
				if($image->processed):
					return $image;
				else:
					echo $image->error;
					return false;
				endif;
			else:
				echo $image->error;
				return false;
			endif;

	    }

	}

	

?>