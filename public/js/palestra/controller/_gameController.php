<?php 

	include_once("components/crud.php");

	include_once("appController.php");

	class Game extends Crud {
		function getRank($player_id){
			
			$res = Crud::_read("games",NULL);
			
			$points = array(0);
			$arr = 0;

			while($r = mysql_fetch_array($res)){
				//echo $r['id'].'<br/>';
				$myres = Crud::_read("player_game",array("id_player = '".$player_id."' AND id_game = '".$r['id']."' "));
				if($myres){
					while($p = mysql_fetch_array($myres)){
						if($p['result'] == $r['result']){
							array_push($points, $arr+1);
						}
						if($p['result_ext'] == $r['result_ext']){
							array_push($points, $arr+2);
						}	
					}
				}
				
			}
			$sum = null;	
			$sum = array_sum($points);

			$read = null;
			$read = Crud::_read("rankings",array("id_player = '".$player_id."'"));
			$read_res = mysql_fetch_object($read);
			if(isset($read_res->id)){

				$create = Crud::_update("rankings",array("points = '".$sum."' WHERE id_player = '".$player_id."' "));
				
			}else{
				$create = Crud::_create('rankings',"'NULL', '".$player_id."','".$sum."','','' ");
			}
		}
		function getPlayerRank($player_id=null){
			$user=array();
			if(isset($player_id)){
				$read = Crud::_read("rankings",array("id_player = '".$player_id."'"));
			}else{
				$read = Crud::_read("rankings",array("id !='' ORDER BY points DESC"));
			}
			while($u = mysql_fetch_array($read)):

				$user[] = $u;

			endwhile;

			return $user;
			
		}
		function updatePlayerRank(){

				$player_rank = $this->getPlayerRank();

				//if(isset($player_rank[0]['id'])):

				$update = null;
				$sql = "SELECT id_player, last_position, now_position, points, @curRank := @curRank + 1 AS pos FROM rankings R , (SELECT @curRank := 0) r ORDER BY  points DESC";
				$q = mysql_query($sql);
				if(mysql_num_rows($q) > 0):
				for($i=0; $res_all = mysql_fetch_array($q); $i++){
					if($res_all['id_player'] == $player_rank[$i]['id_player']){
						if(isset($player_rank[$i]['now_position']) and $player_rank[$i]['now_position'] != '0'){
							$update = Crud::_update("rankings",array("last_position = '".$player_rank[$i]['now_position']."', now_position = '".$res_all['pos']."' WHERE id_player = '".$player_rank[$i]['id_player']."' "));
						}else{
							$update = Crud::_update("rankings",array("last_position = '".$res_all['pos']."', now_position = '".$res_all['pos']."' WHERE id_player = '".$player_rank[$i]['id_player']."' "));
						}
					}
					
				}
				if($update): return true; else:	return false; endif;
				
				endif;
		}
		function actionRank($player_id=null, $status=null){
			$user_rank = $this->getPlayerRank($player_id);
			$diff = null;
			$diff = $user_rank[0]['last_position'] - $user_rank[0]['now_position'];
			
			if(isset($status) and $status == 'ranking_page'){
				if($diff == 0){
					return '<span class="status-posicao">Posição Inalterada</span>';
				}elseif($diff > 0){
					$pos = ($diff>1) ? "posições" : "posição";
					return '<span class="status-posicao green">Subiu '.$diff.' '.$pos.' </span>';
				}else{
					$pos = ($diff < (-1)) ? "posições" : "posição";
					return '<span class="status-posicao red">Desceu '.substr($diff, 1).' '.$pos.'</span>';
				}
			}else{
				if($diff == 0){
					return false;
				}elseif($diff > 0){
					return '<div class="notificacao green">'.$diff.'</div>';
				}else{
					return '<div class="notificacao red">'.$diff.'</div>';
				}
			}	
		}
		function getTurns($fase_id, $rows=null){
			$user=array();
			if(isset($rows)){
				$read = Crud::_read("turns",array("id_fase = '".$fase_id."'"));
				$n = mysql_num_rows($read);
				return $n;
			}else{
				$read = Crud::_read("turns",array("id_fase = '".$fase_id."'"));
				while($u = mysql_fetch_array($read)):

				$user[] = $u;

			endwhile;

			return $user;
			}
		}
		function getFases($params=null){
			$user=array();

			$read = Crud::_read("fases",$params);

			while($u = mysql_fetch_array($read)):

				$user[] = $u;

			endwhile;

			return $user;
		}
		function show($params=NULL){

			$user=array();

			$read = Crud::_read("games",$params);

			while($u = mysql_fetch_array($read)):

				$user[] = $u;

			endwhile;

			return $user;

		}
		function create($post){
			$date = explode('/',$post['dateGame']);
			$format_date = $date[2].'-'.$date[1].'-'.$date[0];
			$ext_date = $format_date.' '.$post['hourGame'];
			if(isset($post['turn'])): $turn = $post['turn']; else: $turn = null; endif;
			$create = Crud::_create('games',"'NULL', '".$post['time1']."', '".$post['time2']."', '".$ext_date."', '".$post['local']."', '".$post['fase']."','".$turn."', '', '' ");
			if($create):

				return true;

			else:

				return false;

			endif;
		}
		function Edit($params){
			$date = explode('/',$params['dateGame']);
			$format_date = $date[2].'-'.$date[1].'-'.$date[0];
			$ext_date = $format_date.' '.$params['hourGame'];
			if(isset($params['turn'])): $turn = $params['turn']; else: $turn = null; endif;
			
			$result = null;
			$result_ext = null;
			
			if(($params['result1'] != '') and ($params['result1'] != '')){

			$result = $this->writeResult($params['result1'],$params['result2']);
			$result_ext = serialize(array($params['result1'],$params['result2']));

			}
			
			$edit = Crud::_update("games",array("team1 = '".$params['time1']."', team2 = '".$params['time2']."', date = '".$ext_date."', local = '".$params['local']."', id_fase = '".$params['fase']."', id_turn = '".$turn."', result = '".$result."', result_ext = '".$result_ext."' WHERE id = '".$params['edit_game']."' "));
			
			if($edit): 
				
				
				//alterando rankeamento dos usuarios
				$games = Crud::_read("player_game",array("id_game = '".$params['edit_game']."'"));
				$sum=0;
				while($gm = mysql_fetch_array($games)){
					
					$this->getRank($gm['id_player']);

					$player_rank = $this->getPlayerRank($gm['id_player']);
					$player_aliance = $this->getPlayerAliance($player_rank[0]['id_player']); $aliance = $this->getAliance($player_aliance[0]['id_aliance']);

					$aliance_ranking = $this->getAlianceRank($aliance[0]['id']);
					if($player_rank[0]['id'] !=''){
				
					//print_r($aliance_ranking);
					if(isset($aliance_ranking[0]['id'])): $aliance_ID = array($aliance_ranking[0]['id']); endif;
					if(!isset($aliance_ranking[0]['id'])){
						//create aliance ranking
						$sum = $player_rank[0]['points']++;
						$action = Crud::_create('rankings_aliance',"'NULL', '".$aliance[0]['id']."', '".$sum."' ");
					}else{
						//update aliance ranking
						/*$soma = array($aliance_ranking[0]['points']);
						$som = array_sum($soma);*/
						$sum = $player_rank[0]['points']++;
						
						
						
					}
					}
					
					//$this->updateAlianceRank($gm['id_player']);
				}
				
				if(isset($aliance_ID[0])){
					$action = Crud::_update('rankings_aliance',array("points = '".$sum."' WHERE id = '".$aliance_ID[0]."' "));
				}

				if($this->updatePlayerRank() == true):	return true; else: return false; endif;
			
			else: 
				return false; 
			endif;
		}
		function updateAlianceRank($player_id){
			$sum = 0;
			$player_rank = $this->getPlayerRank($player_id);
			if($player_rank[0]['id'] !=''){
				
				//print_r($aliance_ranking);
				if(!isset($aliance_ranking[0]['id'])){
					//create aliance ranking
					$action = Crud::_create('rankings_aliance',"'NULL', '".$aliance[0]['id']."', '".$player_rank[0]['points']."' ");
				}else{
					//update aliance ranking
					/*$soma = array($aliance_ranking[0]['points']);
					$som = array_sum($soma);*/
					$sum += $player_rank[0]['points'];
					return $sum;
					$action = Crud::_update('rankings_aliance',array("points = '".$sum."' WHERE id = '".$aliance_ranking[0]['id']."' "));
				}

				//if($action): return true; else: return false; endif;
			}else{
				return false;
			}
		}
		function getAliance($id=null){
			$user = array();

			if($id != null){
				$read = Crud::_read('aliances',array('id = "'.$id.'" '));
			}else{
				$read = Crud::_read('aliances',null);
			}
			
			while($u = mysql_fetch_array($read)):

				$user[] = $u;

			endwhile;

			return $user;
		}
		function getPlayerAliance($player_id=null, $aliance_id=null){
			$user = array();
			if(isset($player_id)){
				$read = Crud::_read('player_aliance',array('id_player = "'.$player_id.'" '));
			}else{
				$read = Crud::_read('player_aliance',array('id_aliance = "'.$aliance_id.'" '));
			}
			while($u = mysql_fetch_array($read)):

				$user[] = $u;

			endwhile;

			return $user;
		}
		function getPlayers($params=null){
			$user=array();

			$read = Crud::_read("players",$params);

			while($u = mysql_fetch_array($read)):

				$user[] = $u;

			endwhile;

			return $user;
		}
		function getAlianceRank($aliance_id=null){
			$user=array();
			if(isset($aliance_id)){
				$read = Crud::_read("rankings_aliance",array("id_aliance = '".$aliance_id."'"));
			}else{
				$read = Crud::_read("rankings_aliance",array("id !='' ORDER BY points DESC"));
				
			}
			while($u = mysql_fetch_array($read)):

				$user[] = $u;

			endwhile;

			return $user;
		}
		function getAliRank(){
			$aliances = $this->getAliance();

			$player_rankings = $this->getPlayerRank();

			
			if($aliances){
				foreach($aliances as $a){


		
				}
			}
			foreach ($player_rankings as $r){
						
						$player_aliance[] = $this->getPlayerAliance($r['id_player']);
						
						/*$player_aliance = $this->getPlayerAliance($a['id']);

						if($player_aliance[0]['id_player'] == $r['id_player']){
							echo $player_aliance['id_aliance'];
							echo '<br/>';
						}*/
					}

			
		}
		function writeResult($result1,$result2)
		{
			if($result1>$result2)
			{
				$res = 1;
			}
			elseif($result2>$result1)
			{
				$res = 2;
			}
			elseif($result1 == $result2)
			{
				$res = 3;
			}
			else
			{
				$res = false;
			}
			return $res;
		}
		function createTeam($post, $file){
			
			//subindo arquivo
			$img_uploaded = Site::upload($file['flag']);

			if($img_uploaded != false):
				//populando a base
				$create = Crud::_create('teams',"'NULL', '".$post['name']."', '".$post['abv']."', '".$img_uploaded->file_dst_name."', '".$post['group_id']."' ");
				if($create):

					return true;

				else:

					return false;

				endif;
			else:
				return false;
			endif;
		}
		function getTeams($params=null)
		{
			$user=array();

			$read = Crud::_read("teams",$params);

			while($u = mysql_fetch_array($read)):

				$user[] = $u;

			endwhile;
			
			return $user;

		}
		function getGroups($params=null){
			$user=array();

			$read = Crud::_read("groups",$params);

			while($u = mysql_fetch_array($read)):

				$user[] = $u;

			endwhile;
			
			return $user;

		}
		function getFlag($id = null)
		{
			$read = Crud::_read("teams",array('id = "'.$id.'"'));
			$u = mysql_fetch_object($read);
			return $u->flag;
		}
		function delete($id=NULL){

			if(Crud::_delete("games",$id)):

				return true;

			else:

				return false;

			endif;

		}
		function deleteTeam($id=NULL){

			if(Crud::_delete("teams",$id)):

				return true;

			else:

				return false;

			endif;

		}

		function createGamePlay($player_id=null, $game_id=null, $res_one=null, $res_two=null)
		{
			$read = Crud::_read("player_game",array("id_player='".$player_id."' AND id_game = '".$game_id."'"));
			$u = mysql_fetch_object($read);
			
			$result = $this->writeResult($res_one,$res_two);
			$result_ext = serialize(array($res_one,$res_two));

			if(isset($u->id)){
				$update = Crud::_update("player_game",array("result = '".$result."', result_ext = '".$result_ext."' WHERE id = '".$u->id."'"));
				if($update): return true; else:	return false; endif;
			}else{
				$create = Crud::_create('player_game',"'NULL', '".$player_id."', '".$game_id."', '".$result."', '".$result_ext."' ");
				if($create): return true; else:	return false; endif;
			}

		}
	}