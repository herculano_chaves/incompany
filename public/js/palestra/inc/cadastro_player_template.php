			<table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Email</th>
                            <th>Setor</th>
                            <th>Empresa</th>
                            <th>Convidar</th>
                            <th>Editar</th>
                            <th>Apagar</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($players as $p): ?>

							  <tr>
							    <td valign="middle"><?php echo $p['name']; ?></td>
							    <td valign="middle"><?php echo $p['email']; ?></td>
							    <td valign="middle"><?php $area = $Player->getAreas(array(" id = '".$p['id_area']."' ")); echo $area[0]['name']; ?></td>
							    <td valign="middle"><?php $enterprise = $Player->getEnterprise(array(" id = '".$p['id_enterprise']."' ")); echo $enterprise[0]['name']; ?></td>
							    <td><?php if($p['active'] == "1"): echo '<span style="color:navy;">Enviado</span>'; elseif($p['active'] == "2"): echo '<span style="color:green;">Confirmado</span>'; else: ?><a href="<?php echo ROOT; ?>/admin/actions?c=true&id=<?php echo $p['id']; ?>">Enviar</a><?php endif; ?></td>
							    <td><a href="<?php echo ROOT; ?>/admin/player/edit/<?php echo $p['id']; ?>">Editar</a></td>
							    <td><a href="<?php echo ROOT; ?>/admin/actions?d=1&t=players&id=<?php echo $p['id']; ?>">Apagar</a></td>
							  </tr>

  						<?php endforeach; ?>
                    </tbody>
                </table>
                <h3>Adicionar Jogador</h3>
                <form method="post" id="frmCreateNewPlayer" action="actions.php">
				<input type="hidden" name="create_new_player" value="1">
                <table class="table table-striped table-bordered">
                	<tr>
                		<th>Nome</th>
                		<th>Email</th>
                		<th>Area</th>
                		<th>Empresa</th>
                		<th>Adicionar</th>
                	</tr>
                	<tr>
                		<td><input type="text" name="name"></td>
                		<td><input type="text" name="email"></td>
                		<td>
                			<select name="area" id="">
								<option value="">Selecione setor</option>
								<?php $areas = $Player->getAreas(); 
									foreach ($areas as $a):
										?>	
									<option value="<?php echo $a['id']; ?>"><?php echo $a['name']; ?></option>
									<?php
									endforeach;
								?>
							</select>
						</td>
                		<td>
                			<?php if($user->role == '1'): ?>
                			<select name="enterprise" id="">
								<option value="">Selecione Empresa</option>
								<?php $empresas = $Player->getEnterprise(); 
								foreach ($empresas as $e):
									?>	
								<option value="<?php echo $e['id']; ?>"><?php echo $e['name']; ?></option>
								<?php
								endforeach;
								?>
							</select>
							<?php else:  $empresa = $Player->getEnterprise(array("id = '".$user->id_enterprise."'")); ?>
								<input type="text" readonly="readonly" value="<?php echo $empresa[0]['name']; ?>" name="enterprise_fake">
								<input type="hidden"  value="<?php echo $empresa[0]['id']; ?>" name="enterprise">
							<?php endif; ?>
                		</td>
                		<td><input type="submit" value="Adicionar"></td>
                	</tr>
                </table>
            </form>
				<h4>Importar Jogadores</h4>
				<form action="<?php echo ROOT; ?>/admin/import" enctype="multipart/form-data" method="post">
					<input type="hidden" name="Import" value="1">
					<table class="table table-striped table-bordered">
						<tr>
                		<th>Empresa</th>
                		<th>Setor</th>
                		<th>Dados Colaboradores (apenas .CSV)</th>
                		<th>Adicionar</th>
                	</tr>
					
					  <tr>
					  	
                        <th>
                        <?php if($user->role == '1'): ?>
						<select name="enterprise" id="">
							<option value="">Selecione</option>
						<?php $empresas = $Player->getEnterprise(); 
							foreach ($empresas as $e):
								?>	
							<option value="<?php echo $e['id']; ?>"><?php echo $e['name']; ?></option>
							<?php
							endforeach;
						?>
						</select>
						<?php else: $empresa = $Player->getEnterprise(array("id = '".$user->id_enterprise."'"));  ?>
						<input type="text" readonly="readonly" value="<?php echo $empresa[0]['name']; ?>" name="enterprise_fake">
						<input type="hidden"  value="<?php echo $empresa[0]['id']; ?>" name="enterprise">
						<?php endif; ?>
						</th>
					  
					 
                        <th>
							<select name="area" id="">
								<option value="">Selecione</option>
								<?php $areas = $Player->getAreas(); 
									foreach ($areas as $a):
										?>	
									<option value="<?php echo $a['id']; ?>"><?php echo $a['name']; ?></option>
									<?php
									endforeach;
								?>
							</select>
						</th>
					
					
                        <th>
						
						<input name="file" type="file">	
							</th>
					<th><input type="submit" value="Importar"></th>
					</tr>
				</table>
			