<?php $id = $_GET['id'];

  $p = $Player->show(array('id="'.$id.'"'));
 ?>
<h1>Editar Jogador</h1>
<form action="<?php echo ROOT; ?>/admin/actions.php" method="post" enctype="multipart/form-data">
	<input type="hidden" name="edit_player" value="<?php echo $p[0]['id']; ?>">
<table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Email</th>
                            <th>Setor</th>
                           <?php if($user->role == '1'): ?>
                            <th>Empresa</th>
                          <?php endif; ?>
                          
                            <th>Editar</th>
                           
                        </tr>
                    </thead>
                    <tbody>
                       
                <tr>
                  <td valign="middle"><input type="text" name="name" value="<?php echo $p[0]['name']; ?>" ></td>
                  <td valign="middle"><input type="text" name="email" value="<?php echo $p[0]['email']; ?>" ></td>
                  <td valign="middle">
                    <select name="area" id="">
                      <option value="">Selecione setor</option>
                      <?php $areas = $Player->getAreas(); 
                     
                        foreach ($areas as $a):
                            if($a['id'] == $p[0]['id_area']):
                              echo ' <option selected="selected" value="'.$a['id'].'">'.$a['name'].'</option>';
                            else:
                              echo ' <option value="'.$a['id'].'">'.$a['name'].'</option>';
                            endif; 
                        endforeach;
                      ?>
                    </select>
                  </td>
                  <?php if($user->role == '1'): ?>
                  <td valign="middle">
                     <select name="enterprise" id="">
                      <option value="">Selecione empresa</option>
                      <?php $empresas = $Player->getEnterprise();  
                     
                        foreach ($empresas as $e):
                            if($e['id'] == $p[0]['id_enterprise']):
                              echo ' <option selected="selected" value="'.$e['id'].'">'.$e['name'].'</option>';
                            else:
                              echo ' <option value="'.$e['id'].'">'.$e['name'].'</option>';
                            endif; 
                        endforeach;
                      ?>
                    </select>
                  </td>
                  <?php else: ?>
                    <input type="hidden" name="enterprise" value="<?php echo $user->id_enterprise; ?>">
                  <?php endif; ?>
                  <td><input type="submit" value="Editar"> </td>
                  
                </tr>

           
                    </tbody>
                </table>
</form>
	