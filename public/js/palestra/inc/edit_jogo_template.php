<?php $id = $_GET['id'];
	$g = $game->show(array('id="'.$id.'"'));

	$res_ext = null;
	$res_ext = unserialize($g[0]['result_ext']);

 ?>
<form action="<?php echo ROOT; ?>/admin/actions.php" method="post" enctype="multipart/form-data">
	<input type="hidden" name="edit_game" value="<?php echo $g[0]['id']; ?>">
	<?php $times = $game->getTeams();  ?>
	<table class="table table-striped table-bordered">
		<tr>
			<td><label for="time1">Jogo</label></td>
			<td><label for="">Data do jogo</label></td>
			<td><label for="">Hora do jogo</label></td>
			<td><label for="">Fase</label></td>
			<td><label for="">Turno</label></td>
			<td><label for="">Local</label></td>
			<td>Editar</td>
		</tr>
		<tr>
			<td>
				<select style="width:90px;" name="time1" id="time1">
					<?php foreach($times as $time): ?>
						<?php if($g[0]['team1'] == $time['id']): ?>
							<option selected="selected" value="<?php echo $time['id']; ?>"><?php echo $time['name']; ?></option>
						<?php else: ?>
							<option value="<?php echo $time['id']; ?>"><?php echo $time['name']; ?></option>
						<?php endif; ?>
					<?php endforeach; ?>
				</select>
				<label for="time1"><img style="height: 19px;vertical-align: -4px;" src="<?php echo ROOT; ?>/archives/<?php echo $game->getFlag($g[0]['team1']); ?>"></label>
				<input style="text-align:center;" type="text" value="<?php echo $res_ext[0]; ?>" name="result1" maxlength="3" size="2">	
				<span> X </span>
				<input style="text-align:center;" type="text" value="<?php echo $res_ext[1]; ?>" name="result2" maxlength="3"  size="2">
				<label for="time2"><img style="height: 19px;vertical-align: -4px;" src="<?php echo ROOT; ?>/archives/<?php echo $game->getFlag($g[0]['team2']); ?>"></label>
				<select style="width:90px;" name="time2" id="time2">
					<?php foreach($times as $time): ?>
						<?php if($g[0]['team2'] == $time['id']): ?>
							<option selected="selected" value="<?php echo $time['id']; ?>"><?php echo $time['name']; ?></option>
						<?php else: ?>
							<option value="<?php echo $time['id']; ?>"><?php echo $time['name']; ?></option>
						<?php endif; ?>
					<?php endforeach; ?>
				</select>	
			</td>
			<td><input size="10" value="<?php echo HMask::datetime($g[0]['date'],'date','%s/%s/%s'); ?>" type="text" id="dateGame" name="dateGame"></td>
			<td><input size="5" value="<?php echo HMask::datetime($g[0]['date'],'hour','%s:%s'); ?>" type="text" id="hourGame" name="hourGame"></td>
			<td>
				<?php $fases = $game->getFases();?>
				<select name="fase" id="selfase">
				<option value="">Selecione fase</option>
					<?php foreach($fases as $f): 
						if($f['id'] == $g[0]['id_fase']):
					?>
						<option selected="selected" value="<?php echo $f['id']; ?>"><?php echo $f['name']; ?></option>
					<?php else: ?>
						<option value="<?php echo $f['id']; ?>"><?php echo $f['name']; ?></option>
					<?php endif; ?>
					<?php endforeach; ?>
				</select>	
			</td>
			<td>
				<?php 
					$turns = $game->getTurns($g[0]['id_fase']);

					if($turns):
					$res = '<select name="turn">
						<option>Selecione</option>';
						foreach ($turns as $t) {
							if($t['id'] == $g[0]['id_turn']){
								$res .='<option selected="selected" value="'.$t['id'].'">'.$t['name'].'</option>';
							}else{
								$res .='<option value="'.$t['id'].'">'.$t['name'].'</option>';
							}
							
						}
					$res .='</select>';

					echo $res;
					endif;
				?>
			</td>
			<td><input type="text" name="local" value="<?php echo $g[0]['local']; ?>"></td>
			<td><input type="submit" value="Editar jogo"></td>
		</tr>
</form>	