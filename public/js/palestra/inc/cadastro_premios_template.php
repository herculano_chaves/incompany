<h1>Cadastrar prêmios</h1>
<form action="actions.php" method="post" enctype="multipart/form-data">
	<input type="hidden" name="create_new_prize" value="1">
  <input type="hidden" name="id_enterprise" value="<?php echo $user->id_enterprise; ?>">
	<table class="table table-striped table-bordered">
  <tbody>
      <tr>
        <th>Módulo</th>
        <th>Colocação</th>
        <th>Nome</th> 
        <th>Descrição</th>
        <th>Imagem</th>
        <th>&nbsp;</th>
      </tr>
      <tr>
        <td><select name="module"><option>Selecione</option><option value="1">Geral</option>
        <?php if($user->id_enterprise == '3' or $user->id_enterprise == '0'): echo '<option value="2">Time de Valor</option>'; endif; ?>
        </select></td>
        <td><input name="position" size="3" type="text"></td>
        <td><input name="name" type="text"></td>
        <td><textarea name="description"></textarea></td>
        <td><input name="image" type="file"></td>
        <td><input type="submit" value="Cadastrar prêmio"></td>
      </tr>
    </table>
</form>
<?php if($all): ?>
<h3>Grid de prêmios</h3>
<table class="table table-striped table-bordered">

  <tbody>
  <tr>
    <th>Módulo</th>
    <th>Colocação</th>
	  <th>Nome</th>
    <th>Imagem</th>
  	<th>Editar</th>
  	<th>Apagar</th>
  </tr>

  <?php foreach($all as $e):   ?>
  <tr height="80">
    <td valign="middle"><?php echo $e['module']; ?></td>
    <td valign="middle"><?php echo $e['position']; ?></td>

    <td valign="middle"><?php echo $e['name']; ?></td>

    <td valign="middle"><?php echo ($e['image']) ? '<img src="'.ROOT.'/archives/'.$e['image'].'" />' : 'Não possui'; ?></td>

  
    <td><a href="<?php echo ROOT; ?>/admin/premio/edit/<?php echo $e['id']; ?>">Editar</a></td>
    <td><a href="<?php echo ROOT; ?>/admin/actions?d=1&t=prizes&id=<?php echo $e['id']; ?>">Apagar</a></td>

  </tr>


  <?php endforeach;  ?>

  </tbody>
</table>
<?php endif; ?>