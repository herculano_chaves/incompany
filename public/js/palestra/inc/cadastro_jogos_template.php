<h1>Cadastrar novo jogo</h1>
<form action="actions.php" method="post" enctype="multipart/form-data">
	<input type="hidden" name="create_new_game" value="1">
	<table class="table table-striped table-bordered">
		<tr>
			<td><label for="time1">Time 1</label></td>
			<td><label for="time2">Time 2</label></td>
			<td><label for="">Data</label></td>
			<td><label for="">Hora</label></td>
			<td><label for="">Fase</label></td>
			<td><label for="">Turno</label></td>
			<td><label for="">Local</label></td>
			<td>Cadastrar</td>
		</tr>
		<tr>
			<td>
				<?php $times = $game->getTeams();  ?>
				<select name="time1" id="time1">
					<option value="">Selecione</option>
					<?php foreach($times as $time): ?>
						<option value="<?php echo $time['id']; ?>"><?php echo $time['name']; ?></option>
					<?php endforeach; ?>
				</select>	
			</td>
			<td>
				<select name="time2" id="time2">
				<option value="">Selecione</option>
					<?php foreach($times as $time): ?>
						<option value="<?php echo $time['id']; ?>"><?php echo $time['name']; ?></option>
					<?php endforeach; ?>
				</select>	
			</td>
			<td><input type="text" id="dateGame" size="10" name="dateGame"></td>
			<td><input type="text" id="hourGame" size="4" name="hourGame"></td>
			<td>
				<?php $fases = $game->getFases(); ?>
				<select name="fase" id="selfase">
				<option value="">Selecione fase</option>
					<?php foreach($fases as $f): ?>
						<option value="<?php echo $f['id']; ?>"><?php echo $f['name']; ?></option>
					<?php endforeach; ?>
				</select>	
			</td>
			<td>
				<span id="selTurn"></span>
			</td>
			<td><input type="text" name="local"></td>
			<td><input type="submit" value="Cadastrar jogo"></td>
		</tr>
	</table>
	
</form>	

<h2>Jogos</h2>
<table class="table table-striped table-bordered">

  <tbody>
  <tr>
	<th>Jogo</th>
    <th>Time 1</th>
    <th>Time 2</th>
    <th>Data</th>
	<th>Hora</th>
	<th>Editar</th>
	<th>Apagar</th>
  </tr>

  <?php foreach($games as $g): 

  if( ($game->getTeams(array("id = '".$g['team1']."'")) == true) and ($game->getTeams(array("id = '".$g['team2']."'")) == true) ):
  ?>
  <tr height="80">

    <td valign="middle"><?php echo $g['id']; ?></td>

    <td valign="middle"><?php $time1 = $game->getTeams(array("id = '".$g['team1']."'")); echo $time1[0]['name']; ?></td>

    <td valign="middle"><?php $time2 = $game->getTeams(array("id = '".$g['team2']."'")); echo $time2[0]['name']; ?></td>

    <td valign="middle"><?php echo HMask::datetime($g['date'],'date','%s/%s/%s'); ?></td>
    <td valign="middle"><?php echo HMask::datetime($g['date'],'hour','%s:%s'); ?></td>

    <td><a href="<?php echo ROOT; ?>/admin/game/edit/<?php echo $g['id']; ?>">Editar</a></td>
    <td><a href="<?php echo ROOT; ?>/admin/actions?d=1&t=games&id=<?php echo $g['id']; ?>">Apagar</a></td>

  </tr>


  <?php 
  else:

  	for($i=0; $i <= sizeof(array($g['id'])); $i++){
  		$game->delete(array('id = "'.$g['id'][$i].'"'));
  	}
  	
  endif;
 endforeach; 
 ?>

  </tbody>

</table>