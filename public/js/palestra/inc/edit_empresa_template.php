<?php $id = $_GET['id'];
	$e = $empresa->show(array('id="'.$id.'"'));
 ?>
<h1>Editar empresa</h1>
<form action="<?php echo ROOT; ?>/admin/actions.php" method="post" enctype="multipart/form-data">
	<input type="hidden" name="edit_enterprise" value="<?php echo $e[0]['id']; ?>">
	<table class="table table-striped table-bordered">
  <tbody>
  <tr>
    <td><label for="">Nome</label></td><td><input value="<?php echo $e[0]['name']; ?>" name="name" type="text"></td>
  </tr>
  <tr>
    <td><label for="">Email</label></td>
    <td><input value="<?php echo $e[0]['email']; ?>" name="email" type="text"></td>
  </tr>
  <tr>
    <td><label for="">Logo</label></td>
    <td><?php echo '<img src="'.ROOT.'/archives/'.$e[0]['logo'].'" />'; ?><input name="logo" type="file"></td>
  </tr>
  <tr>
    <td><label for="">Cor de fundo</label></td>
    <td><input name="primary_color" value="<?php echo $e[0]['primary_color']; ?>" id="primary_color" type="text"></td>
  </tr>
  <tr>
    <td><label for="">Cor secundária</label></td>
    <td><input name="secundary_color" value="<?php echo $e[0]['secundary_color']; ?>" id="secundary_color" type="text"></td>
  </tr>
  <tr>
    <td><label for="">Cor dos links</label></td>
    <td><input name="link_color" value="<?php echo $e[0]['link_color']; ?>" id="link_color" type="text"></td>
  </tr>
   <tr>
    <td><label for="">Cor dos links ativos (mouseover)</label></td>
    <td><input name="link_hover_color" value="<?php echo $e[0]['link_hover_color']; ?>" id="link_hover_color" type="text"></td>
  </tr>
  <tr>
    <td><label for="">Cor dos textos dos links</label></td>
    <td><input name="link_text_color" value="<?php echo $e[0]['link_text_color']; ?>" id="link_text_color" type="text"></td>
  </tr>
  <tr>
    <td><label for="">Habilitar</label></td>
    <td>
      Sim<input type="radio" <?php if($e[0]['mode_on'] == '1'): echo 'checked="checked"'; endif; ?> name="mode_on" value="1"> 
Não<input type="radio" <?php if($e[0]['mode_on'] == '0'): echo 'checked="checked"'; endif; ?> name="mode_on" value="0">
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><input type="submit" value="Editar empresa"></td>
  </tr>
    </table>
</form>
	