<h1>Cadastrar nova empresa</h1>
<form action="actions.php" method="post" enctype="multipart/form-data">
	<input type="hidden" name="create_new_enterprise" value="1">
	<table class="table table-striped table-bordered">
  <tbody>
  <tr>
    <td><label for="">Nome</label></td><td><input name="new_enterprise" type="text"></td>
  </tr>
  <tr>
    <td><label for="">Email</label></td>
    <td><input name="email" type="text"></td>
  </tr>
  <tr>
    <td><label for="">Logo</label></td>
    <td><input name="logo" type="file"></td>
  </tr>
  <tr>
    <td><label for="">Cor de fundo</label></td>
    <td><input name="primary_color" id="primary_color" type="text"></td>
  </tr>
  <tr>
    <td><label for="">Cor secundária</label></td>
    <td><input name="secundary_color" id="secundary_color" type="text"></td>
  </tr>
  <tr>
    <td><label for="">Cor dos links</label></td>
    <td><input name="link_color" id="link_color" type="text"></td>
  </tr>
   <tr>
    <td><label for="">Cor dos links</label></td>
    <td><input name="link_color" id="link_color" type="text"></td>
  </tr>
  <tr>
    <td><label for="">Cor dos textos dos links</label></td>
    <td><input name="link_text_color" id="link_text_color" type="text"></td>
  </tr>
  <tr>
    <td><label for="">Habilitar</label></td>
    <td>Sim<input type="radio" name="mode_on" value="1"> Não<input type="radio" name="mode_on" value="0"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><input type="submit" value="Cadastrar empresa"></td>
  </tr>
    </table>
</form>

<h3>Grid de empresas</h3>
<table class="table table-striped table-bordered">

  <tbody>
  <tr>
	<th>Nome</th>
    <th>Email contato</th>
    <th>Logo</th>
    <th>Habilitada</th>
	<th>Editar</th>
	<th>Apagar</th>
  </tr>

  <?php foreach($all as $e):   ?>
  <tr height="80">

    <td valign="middle"><?php echo $e['name']; ?></td>

    <td valign="middle"><?php echo $e['email']; ?></td>

    <td valign="middle"><?php echo ($e['logo']) ? '<img src="'.ROOT.'/archives/'.$e['logo'].'" />' : 'Não possui'; ?></td>

    <td valign="middle"><?php echo ($e['mode_on'] == '1') ? "Sim" : "Não"; ?></td>
  
    <td><a href="<?php echo ROOT; ?>/admin/empresa/edit/<?php echo $e['id']; ?>">Editar</a></td>
    <td><a href="<?php echo ROOT; ?>/admin/actions?d=1&t=enterprises&id=<?php echo $e['id']; ?>">Apagar</a></td>

  </tr>


  <?php endforeach;  ?>

  </tbody>
</table>