<?php
require_once("controller/enterpriseController.php");
?>
<div class="app-header">
		<div class="app-header-logo text-center" style="background:#000;">
			<img src="<?php echo ROOT; ?>/assets/images/img-logo-bn.png" height="44" width="165" alt="">
		</div>
		<div class="app-header-userinfo"><?php echo $user->username; ?></div>
		<div class="app-header-logout"><a href="<?php echo ROOT; ?>/logout">logout</a></div>
</div>