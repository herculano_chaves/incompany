<?php 
    require("../view/template_admin.php");

    require_once("controller/playerController.php");

    $player = new Player;
if(isset($_POST["Import"]))
{
   if(isset($_POST['area'])): $area = $_POST['area']; else: $area = null; endif;
   if(isset($_POST['enterprise'])): $enterprise = $_POST['enterprise']; else: $enterprise = null; endif;

   $filename=$_FILES["file"]["tmp_name"];
    if($_FILES["file"]["size"] > 0)
    {
        $r = true;
        $file = fopen($filename, "r");
        $cont=0;
        while (($emapData = fgetcsv($file, 10000, ";")) !== FALSE)
        {

           
            $cont++;
            if($cont>1):

                if($player->importFile($emapData, $enterprise, $area) == true):
                   $r = true;
                else:
                    $r = false;
                endif;
            endif;
        }
        fclose($file);

        if($r = false):
            Site::flash('Falha ao importar arquivos.');
        else:
             Site::flash('Importado com sucesso!');
        endif;
      
    }
    
}else{
    echo "Não foi";
}
Site::redirect('admin/players');
?>