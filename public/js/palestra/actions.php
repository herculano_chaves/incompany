
<?php 

require("../view/template_admin.php");

require_once("controller/adminController.php");

require_once("controller/playerController.php");

require_once("controller/enterpriseController.php");

require_once("controller/gameController.php");

//User

$user = new Admin();

$check = $user->checkAuth();

$user->set($check);


//players

$Player = new Player;
$empresa = new Enterprise;
$game = new Game;


if(isset($_POST['ids'])):

	$ids = $_POST['ids'];

	$uid = $_POST['uid'];

	for($i=0; $i<sizeof($ids); $i++){

		if($inscritos->Edit(array('knaufEvento_presence = "1" WHERE knaufEvento_id = "'.$ids[$i].'"')) == true):

				//Site::flash('Editado com sucesso!');

				

				//enviando email

				if($inscritos->sendEmail($ids[$i],'agradecimento') == true):

					Site::flash('Editado com sucesso!');



				else:

					Site::flash('Erro ao enviar E-mail!');

				endif;

		else:

			Site::flash('Erro ao editar');

		endif;

	}



	//$inscritos->Edit(array('knaufEvento_presence = "0" WHERE knaufEvento_id NOT IN ("'.implode('","',$ids).'") '));

elseif(isset($_GET['d'])):
	
		switch($_GET['t']):
			case 'players':
				if($Player->delete(array("id = '".$_GET['id']."' ")) == true):
					Site::flash("Apagado com sucesso!");
					Site::redirect('admin/players');
				endif;
			break;
			case 'games':
				if($game->delete(array("id = '".$_GET['id']."' ")) == true):
					Site::flash("Apagado com sucesso!");
					Site::redirect('admin/games');
				endif;
			break;
			case 'teams':
				if($game->deleteTeam(array("id = '".$_GET['id']."' ")) == true):
					Site::flash("Apagado com sucesso!");
					Site::redirect('admin/teams');
				endif;
			break;
			case 'enterprises':
				if($empresa->delete(array("id = '".$_GET['id']."' ")) == true):
					Site::flash("Apagado com sucesso!");
					Site::redirect('admin/empresas');
				endif;
			break;
			case 'prizes':
				if($empresa->deletePrize(array("id = '".$_GET['id']."' ")) == true):
					Site::flash("Apagado com sucesso!");
					Site::redirect('admin/premios');
				endif;
			break;
		endswitch;
		
elseif(isset($_GET['c'])):
	if($Player->sendEmail($_GET['id'],'convite') == true):
		if($Player->Edit(array("active = 1 WHERE id = '".$_GET['id']."'")) == true):
			Site::flash('Convite enviado com sucesso');
		else:
			Site::flash('Erro ao atualizar status de envio');
		endif;
	else:
		Site::flash('Erro ao enviar convite');
	endif;
	Site::redirect('admin/players');
elseif(isset($_POST['create_new_prize'])):
	if($empresa->createPrize($_POST, $_FILES) == true):
		Site::flash('Prêmio criado com sucesso!');
	else:
		Site::flash('Erro ao criar prêmio');
	endif;
	Site::redirect('admin/premios');
elseif(isset($_POST['create_new_player'])):
	if($Player->create($_POST)):
		Site::flash('Jogador criado com sucesso!');
	else:
		Site::flash('Erro ao criar jogador');
	endif;
	Site::redirect('admin/players');
elseif(isset($_POST['edit_player'])):
	$ent = null;
	$area = null;
	if($_POST['enterprise']){
		$ent = $_POST['enterprise'];
	}
	if($_POST['area']){
		$area = $_POST['area'];
	}
	if($Player->edit(array("name = '".$_POST['name']."', email = '".$_POST['email']."', id_area = '".$area."', id_enterprise = '".$ent."' WHERE id = '".$_POST['edit_player']."' ")) == true):
		Site::flash('Jogador editado com sucesso!');
	else:
		Site::flash('Erro ao editar jogador');
	endif;
	Site::redirect('admin/player/edit/'.$_POST['edit_player']);
elseif(isset($_POST['create_new_team'])):
	if($game->createTeam($_POST, $_FILES)==true):
		Site::flash('Time criado com sucesso!');
	else:
		Site::flash('Erro ao criar time');
	endif;
	Site::redirect('admin/teams');
elseif(isset($_POST['create_new_enterprise'])):
	if($empresa->create($_POST, $_FILES) == true):
		Site::flash('Empresa criada com sucesso!');
	else:
		Site::flash('Erro ao criar empresa');
	endif;
	Site::redirect('admin/empresas');
elseif(isset($_POST['create_new_game'])):
	if($game->create($_POST) == true):	
		Site::flash('Jogo criado com sucesso!');
	else:
		Site::flash('Erro ao criar jogo');
	endif;
	Site::redirect('admin/games');
elseif(isset($_POST['edit_enterprise'])):
	if($empresa->edit($_POST, $_FILES) == true):
		Site::flash('Salvo com sucesso!');
	else:
		Site::flash('Erro ao editar empresa');
	endif;
	Site::redirect('admin/empresa/edit/'.$_POST['edit_enterprise']);
elseif(isset($_POST['edit_premio'])):
	if($empresa->editPrize($_POST, $_FILES) == true):
		Site::flash('Salvo com sucesso!');
	else:
		Site::flash('Erro ao editar prêmio');
	endif;
	Site::redirect('admin/premio/edit/'.$_POST['edit_premio']);
elseif(isset($_POST['edit_game'])):
	//editando JOGO e gerando RANKING
	if($game->edit($_POST) == true):
		Site::flash('Salvo com sucesso!');
	else:
		Site::flash('Erro ao editar game');
	endif;
	Site::redirect('admin/game/edit/'.$_POST['edit_game']);
else:
	Site::redirect('admin');
endif;




?>