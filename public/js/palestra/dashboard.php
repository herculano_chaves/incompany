<?php 

	require("view/template.php");

	require_once("controller/adminController.php");
	require_once("controller/customerController.php");
	include_once('behavior/paginator.class.php');
	
	Head('');

	//user
	
$user = new Admin();

$check = $user->checkAuth();

$user->set($check);

$customer = new Customer();

if(isset($_GET['s']) and $_GET['s'] != ''): 
	$all_limited = $customer->show(array("cpf = '".$_GET['s']."'"));
   $all = $all_limited;
else:


//paginacao
$pages = new Paginator();

//$pages->labels('','');
$pages->selectable_pages(4);
$records_per_page = 20;

if(isset($_GET['promo']) and $_GET['promo'] == '1'):
	$all_limited = $customer->show(array(" id<>'' and promo = '1' ORDER BY id DESC LIMIT ". (($pages->get_page() - 1) * $records_per_page) . ", " . $records_per_page));
	$all = $customer->show(array(" id<>'' and promo = '1'"));
	$all_sql = $customer->show(array(" id<>'' and promo = '1'"), true);

elseif(isset($_GET['filter'])):
	$msql = null;

	$filial = isset($_GET['filial']) ? $_GET['filial'] : null;
	$nome = isset($_GET['nome']) ? $_GET['nome'] : null;
	$from = isset($_GET['from']) ? $_GET['from'] : null;
	$to = isset($_GET['to']) ? $_GET['to'] : null;

	if($filial!=null){$msql .="estado LIKE '%".$filial."%' AND ";}
	if($nome!=null){$msql .="nome LIKE '%".$nome."%' AND ";}
	if($from!=null){$msql .="data_inscricao >= '".Site::ConverterUS($from)."'  AND ";}
	if($to!=null){$msql .="data_inscricao <= '".Site::ConverterUS($to)."' AND ";}
	
	$msql .="id<>''";

	$all_limited = $customer->show(array("".$msql." ORDER BY id DESC LIMIT ". (($pages->get_page() - 1) * $records_per_page) . ", " . $records_per_page));
	$all = $customer->show(array("".$msql.""));
	echo $all_sql = $customer->show(array("".$msql.""), true);
else:
	$all_limited = $customer->show(array(" id<>'' ORDER BY id DESC LIMIT ". (($pages->get_page() - 1) * $records_per_page) . ", " . $records_per_page));
	$all = $customer->show();
	$all_sql = $customer->show(null, true);
endif;

$pages->records(count($all));
// records per page
$pages->records_per_page($records_per_page);

endif;
?>

	<?php include('inc/header.php'); ?>
	<div class="app-aside">

		<nav>
			<?php include('inc/menu.php'); ?>
		</nav>
	</div>
	<div class="content">
		<div class="panel">
			<div class="panel-heading">
				<strong>Cadastrados <?php if(isset($_GET['action']) and $_GET['action'] == 'true'): echo " - Utilizaram "; elseif(isset($_GET['action']) and $_GET['action'] == 'false'): echo " - Pendentes "; else: echo ""; endif;  ?>(<?php echo count($all); ?>)</strong>
				
				<span style="float:right; display:none;">
					
					<form id="search_cpf" action="<?php echo ROOT; ?>/dashboard" method="get">
						<input type="text" placeholder="CPF" value="<?php if(isset($_GET['s'])): echo $_GET['s']; endif; ?>" name="s">
						<input type="submit" value="Buscar">
					</form>
				</span>

			
				<span style="float:left; margin-top:-10px;">
					<form action="<?php echo ROOT; ?>/excel" method="post">
						<input type="hidden" name="sid" value="<?php echo $all_sql; ?>">
						<input type="image" src="<?php echo ROOT; ?>/assets/images/ico-excel.png" id="bt_excel" value="">
					</form>
				</span>
			</div>
			<div class="panel-body">
				<span style="float:right; width:100%; margin:10px 0;">
					<form  action="<?php echo ROOT; ?>/dashboard" method="get">
						<input type="hidden" name="filter" value="1">
						De <input type="text" name="from" id="from" value="<?php if(isset($_GET['from'])): echo $_GET['from']; endif; ?>">
						Até <input type="text" name="to" id="to" value="<?php if(isset($_GET['to'])): echo $_GET['to']; endif; ?>">
						<input type="text" style="width:180px; margin-right:4px;" name="filial" value="<?php if(isset($_GET['filial'])): echo $_GET['filial']; endif; ?>" placeholder="Filtar por filial">
						
						<input type="text" style="width:180px; margin-right:4px;" name="nome" value="<?php if(isset($_GET['nome'])): echo $_GET['nome']; endif; ?>" placeholder="Filtar por nome">
						
						<input type="submit" value="Filtrar">
					</form>
				</span>
				<?php if($all_limited): ?>
				<table class="table table-striped table-bordered"  data-width="92%" data-min="10" data-max="25" cellpadding="0" cellspacing="0">
					<tr>
						<th>Nome</th>
						<th>Email</th>
					
						<th>Telefone</th>
						<th>Nascimento</th>
						<th>Sexo</th>
						<th>Filial</th>
						<th>Data Inscrição</th>
						<th>Promo</th>
						
					</tr>
					<?php foreach($all_limited as $c): ?>
						<tr>

							<td><?php echo $c['nome'] ?></td>
							<td><?php echo $c['email'] ?></td>
						
							<td><?php echo $c['tel'] ?></td>
							<td><?php echo $c['nasc'] ?></td>
							<td><?php echo $c['sexo'] ?></td>
							<td><?php echo $c['estado'] ?></td>
					
							<td><?php echo Site::ConverterBR($c['data_inscricao'], true); ?></td>
							<td><?php echo $c['promo'] == '1' ? 'Sim' : '--'; ?></td>
								
					<?php endforeach; ?>
					
				</table>
				<?php if(!isset($_GET['s'])): echo $pages->render(); endif; 

				else:
					echo '<p>Não foram encontrados resultados neste busca!</p>';
				endif;

				?>


			</div>
		</div>
	</div>
<?php Footer(); ?>