<?php 
	require("view/template.php");

	Head('Administrador','','login');

	if(isset($_SESSION['admin_user'])): header('Location:'.ROOT.'/dashboard'); endif;

?>
<div class="login-header">
		<h1 class="login-header-title text-center">Beleza Natural Inaugurações</h1>
	</div>
	<div class="login-section">
		<div class="login-section-content">
			<div id="erro"><?php if(isset($_SESSION['warn'])):	echo $_SESSION['warn'];	unset($_SESSION['warn']); endif; ?></div>
			<form action="<?php echo ROOT; ?>/login" method="post" class="login-section-content-form">
				<input type="hidden" name="adminUser" value="1" >
				<div class="input-group row">
					<span class="input-group-lb">
						<span class="icon icon-mail"></span>
					</span>
					<input type="text" name="login" class="form-control" placeholder="Usuário">
				</div>
				<div class="input-group row">
					<span class="input-group-lb">
						<span class="icon icon-lock"></span>
					</span>
					<input type="password" name="pass" class="form-control" placeholder="Senha">
				</div>
				<input class="btn-login row" type="submit" value="Logar">
				<div class="login-links text-center">
					<!-- <a href="#" class="forgetpsw">Esqueceu sua senha?</a> -->
				</div>
				   
			</form>
		</div>
	</div>
<?php Footer(); ?>