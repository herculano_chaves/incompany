<?php 

	require("../view/template_admin.php");

	require_once("controller/adminController.php");
	require_once("controller/enterpriseController.php");
	Head();

//user
$user = new Admin();
$check = $user->checkAuth();
$user->set($check);

$empresa = new Enterprise;
$all = $empresa->show();
?>

<?php include('inc/header.php'); ?>
	<div class="app-aside">
		<nav>
			<?php include('inc/menu.php'); ?>
		</nav>
	</div> 
	<div class="content">
		<div class="panel">
			<div class="panel-heading">
				<strong>Empresas</strong>
			</div>

			<div class="panel-body">
			<?php if(Site::get_flash()): echo '<div id="status">'.Site::get_flash().'</div>'; Site::end_flash(); endif; ?>
			<?php
			if(isset($_GET['edit'])):
				include('inc/edit_empresa_template.php');
			else:
				include('inc/cadastro_empresa_template.php');
			endif;
			?>
		</div>
		</div>
	</div>

<?php Footer(); ?>