// JavaScript Document
	if(location.host == 'localhost')
	{
		//var SITE_URL = '/knauf/projektag/admin/';
	} else {
		//var SITE_URL = '/projektag/admin/';
	}
$(document).ready(function(){
	$('.bt_conf').click(function(){
		var id  = $(this).attr('name');
		var object = $(this);
		var confirma= confirm('Deseja confirmar este Usuário?');
		if(confirma == true){	
			$.ajax({
				url: 'response.php',
				data:{id:id},
				type:'post'
			}).done(function(data){
				if(data){
					var formatData = data[0].data_confirmacao.split('-');
					object.parent('td').html('<span style="color:green;">Confirmado</span>').siblings('.utl').html(formatData[2]+"/"+formatData[1]+"/"+formatData[0]);
					
					alert("Usuário "+data[0].nome+" confirmado!");
				}else{
					alert("Erro ao confirmar!");
				}
				
			});
		}
	});
	$('input[name="datepicker"]').datepicker({
		onSelect: function(date){
			var d = date.split("/")
			var nd = d[2]+"-"+d[1]+"-"+d[0];
			window.location.href="/apps/caravanas/dashboard?action=date&v="+nd;
		}
	});


	$('#search_cpf input[name="s"]').mask('999.999.999-99');

	//$('table.table').responsiveTables();

	 $( "#from" ).datepicker({
      changeMonth: true,
      numberOfMonths: 3,
      dateFormat: 'dd/mm/yy',
      onClose: function( selectedDate ) {
        $( "#to" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#to" ).datepicker({
      changeMonth: true,
      numberOfMonths: 3,
      dateFormat: 'dd/mm/yy',
      onClose: function( selectedDate ) {
        $( "#from" ).datepicker( "option", "maxDate", selectedDate );
      }
    });
});
