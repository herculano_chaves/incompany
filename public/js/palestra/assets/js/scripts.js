// JavaScript Document
	if(location.host == 'localhost')
	{
		var SITE_URL = '/knauf/cleaneo/admin/';
	} else {
		var SITE_URL = '/concursocleaneo/admin/';
	}
	$(document).ready(function(){
		
		$('#customers_filter').change(function(){
			var valor = $(this).val();
			if(valor=='todos'){
				window.location.href= SITE_URL+"customers";
			}else{
				window.location.href="?cat="+valor;
			}
			
		});
		$('input[name="datepicker"]').datepicker();

		//$('input[name="c"]').
		$('.bt_conf').click(function(){
			var id  = $(this).attr('name');
			var object = $(this);
			
			$.ajax({
				url: 'response.php',
				data:{id:id},
				type:'post'
			}).done(function(data){
				if(data){
					object.parent('td').html('');
					object.parent('td').append('<span style="color:green;">Confirmado</span>');
					alert("Usuário "+data.nome+" confirmado!");
				}else{
					alert("Erro ao confirmar!");
				}
				
			});
		});

		$('table.table').responsibleTables();
});
