<?php
session_start();
require("config.php");
include_once("components/function.php");
include_once("behavior/utils.php");
function Head($pageTitle=NULL, $template = NULL, $bodyclass = NULL){
	if(empty($bodyclass)): $bodyclass = 'home'; endif;
	$h = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Beleza Natural Inaugurações - Administrador ';

$h .= $pageTitle ? ' - '.$pageTitle : NULL;
$h .='</title>
<link rel="stylesheet" type="text/css" href="'.ROOT.'/css/reset.css">
<link href="'.ROOT.'/assets/stylesheets/icons.css" rel="stylesheet" type="text/css" />
<link href="'.ROOT.'/assets/stylesheets/screen.css" rel="stylesheet" type="text/css" />
<link href="'.ROOT.'/assets/stylesheets/colorpicker.css" rel="stylesheet" type="text/css" />
<link href="'.ROOT.'/assets/stylesheets/jquery-ui.css" rel="stylesheet" type="text/css" />
<link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic" rel="stylesheet" type="text/css">
<link rel="Shortcut Icon" href="'.ROOT.'/archives/favicon.ico" />
<script type="text/javascript" src="'.ROOT.'/assets/js/jquery.js"></script>
<script type="text/javascript" src="'.ROOT.'/assets/js/jquery-ui.js"></script>
<script type="text/javascript" src="'.ROOT.'/assets/js/colorpicker.js"></script>
<script type="text/javascript" src="'.ROOT.'/assets/js/jquery.mask.js"></script>
<script type="text/javascript" src="'.ROOT.'/assets/js/jquery.validate.js"></script>
<script type="text/javascript" src="'.ROOT.'/assets/js/responsibletable.js"></script>
<script type="text/javascript" src="'.ROOT.'/assets/js/responsibletext.js"></script>
<script type="text/javascript" src="'.ROOT.'/assets/js/jquery.scripts.js"></script>
</head>
<body>';

echo $h;

}
function Footer(){
	$f = '
		</body>
</html>';
echo $f;
}

?>