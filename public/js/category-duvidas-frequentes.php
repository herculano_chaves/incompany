<?php  include("header-interna.php"); ?>

<!-- MENU ------------------------------------------------------------------------- -->

<?php  
$en_US = languageList($requestUri,$urlAtual,"En");
if($en_US==true):
include("menu-en.php"); 
else:
include("menu.php");
endif;
?>
 <style>
    #bg_bela{
        background: url('<?php bloginfo('template_url'); ?>/arquivos/images/bela/ilustra.jpg') no-repeat top left;
        display: block;
        width: 283px;
        height: 759px;
        margin-top: 62px;
    }
 </style>
<!-- CONTENT ------------------------------------------------------------------------ -->

<div class="content left  clear ">
	<div class="form-content centro relative " id="category-institucional">
       
        <div class="caixa-metade left ">
        	
        	<h1 class="title-top" id="title-quem-somos"><?php strTranslate("Dúvidas Frequentes","Common Questions",$en_US); ?></h1>
            <ul class="breadcrumb">
            	<?php
                $categoria = get_the_category();
				$categoria[0]->cat_name;
				$link = get_category_link($categoria[0]->term_id );
				?>
            	<li><a href="<?php bloginfo('url'); ?>"><?php strTranslate("Página Inicial","Home",$en_US); ?></a> »</li>
            	<li><a href="<?php bloginfo('url'); ?>/categoria/empresa"><?php strTranslate("Quem Somos","About Us",$en_US); ?></a>  »</li>
                <li><a><?php echo $categoria[0]->cat_name; ?></a></li>
            </ul><!-- fim breadcrumb -->

            <h2 class="title-itens"><?php wp_title('  ', true, 'right'); ?></h2>
        
            <div class="top"></div>
            <div id="accordion" class="content-interna">

				<?php $i=0; if (have_posts()) : while (have_posts()) : the_post(); $i++;?>
                
                <h3 id="institucional-<?php echo $i ?>" class="<?php if($i == '1'): echo "menos"; else: echo "mais"; endif; ?>"><?php the_title(); ?></h3>
                
                    <span id="descricao-<?php echo $i ?>" class="<?php if($i == '1'): echo "aberto"; else: echo "fechado"; endif; ?> formatacao">
    					<?php the_content(); ?>
                    </span>         

				<?php endwhile; else: ?>
                <?php endif; ?>
                        
                 
            </div><!-- fim content-interna -->
            <div class="footer-form"></div>
           
           
        </div><!-- fim form-left -->
      
        <div class="sidebar" style="background:none;">
			<!-- aqui vem a mascote -->
            <span id="bg_bela"></span>
            <a href=""><img src="<?php bloginfo('template_url'); ?>/arquivos/images/bela/bt-historia.jpg"></a>
			
        </div><!-- fim form-right -->

    </div><!-- fim form-content -->
</div><!-- fim content -->

<!-- FOOTER --------------------------------------------------------------------------- -->

<?php  
if($en_US==true):
include("footer-en.php"); 
else:
include("footer.php");
endif;
?>