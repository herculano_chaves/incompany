//var jQuery = $.noConflict();
$.validator.addMethod("cpf", function(value, element) {
   value = $.trim(value);
	
	value = value.replace('.','');
	value = value.replace('.','');
	cpf = value.replace('-','');
	while(cpf.length < 11) cpf = "0"+ cpf;
	var expReg = /^0+$|^1+$|^2+$|^3+$|^4+$|^5+$|^6+$|^7+$|^8+$|^9+$/;
	var a = [];
	var b = new Number;
	var c = 11;
	for (i=0; i<11; i++){
		a[i] = cpf.charAt(i);
		if (i < 9) b += (a[i] * --c);
	}
	if ((x = b % 11) < 2) { a[9] = 0 } else { a[9] = 11-x }
	b = 0;
	c = 11;
	for (y=0; y<10; y++) b += (a[y] * c--);
	if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11-x; }
	
	var retorno = true;
	if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10]) || cpf.match(expReg)) retorno = false;
	
	return this.optional(element) || retorno;

}, "Informe um CPF válido."); // Mensagem padrão 

	    $.datepicker.regional['pt-BR'] = {
                closeText: 'Fechar',
                prevText: '&#x3c',
                nextText: '&#x3e;',
                currentText: 'Hoje',
                monthNames: ['Janeiro de','Fevereiro de','Mar&ccedil;o de','Abril de','Maio de','Junho de',
                'Julho de','Agosto de','Setembro de','Outubro de','Novembro de','Dezembro de'],
                monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
                'Jul','Ago','Set','Out','Nov','Dez'],
                dayNames: ['Domingo','Segunda-feira','Ter&ccedil;a-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sabado'],
                dayNamesShort: ['D','S','T','Q','Q','S','S'],
                dayNamesMin: ['D','S','T','Q','Q','S','S'],
                weekHeader: 'Sm',
                dateFormat: 'dd/mm/yy',
                firstDay: 0,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''};
        $.datepicker.setDefaults($.datepicker.regional['pt-BR']);



if(location.hostname == 'incompany.dev'){
	var LOCAL = 'http://incompany.dev/';
}
else if(location.hostname == 'incompany.dizain.com.br'){
	var LOCAL = 'http://incompany.dizain.com.br/';
}else{
	var LOCAL = 'http://www.akademia-knauf.com.br/';
}
$(document).ready(function(){
	

	$.validator.addMethod('validateCheckbox', function(value, element) {
  		return ($('input[name="checkIn"]:checked').length > 0);
	});
	jQuery.fn.brTelMask = function() {
 
    return this.each(function(){
        var el = this;
        $(el).focus(function(){
            $(el).mask("(99) 9999-9999?9");
        });
 
        $(el).focusout(function(){
           var phone, element;
           element = $(el);
           element.unmask();
           phone = element.val().replace(/\D/g, '');
           if(phone.length > 10){
               element.mask("(99) 99999-999?9");
           }else{
               element.mask("(99) 9999-9999?9");
           }
        });
    });
}
	
	$.colorbox.resize(function(innerHeight){
		var myHeight = $('#terms').height;
		innerHeight = myHeight;
	});

	//**verifica cancelamento do curso
	$('.bt_cancela_curso').each(function(){
		var actual_date = new Date();
		var date_ini = new Date($(this).data('ano'),($(this).data('mes')-1),$(this).data('dia'));
		
		var limit = new Date(date_ini - (3 * 1000 * 60 * 60 * 24));
		
		if(actual_date>limit == true){
			$(this).css({background:'#93999c', cursor:'default', 'text-decoration':'line-through'}).prop('href','');
		}
		
		
	});

	$('select#estado_id').change(function(){
		var state = $(this).val();
		
		$.ajax({
			url:LOCAL+'city/search',
			type:'post',
			data:{state:state}
		}).done(function(data){
			
			$('#cidade_id').html(data.response).prop('disabled',false);
		});
	});

	$('select.estado_id').change(function(){
		
		var select = $(this);
		var state = $(this).val();
		
		$.ajax({
			url:LOCAL+'city/search',
			type:'post',
			data:{state:state}
		}).done(function(data){
			
			select.parent().parent().siblings().children().children('.cidade_id').html(data.response).prop('disabled',false);
		});
	});
	
	$('.alter_address').click(function(){
		console.log($(this).parent().parent().siblings().children().children('input[name="cep"]').val());
		$(this).parent().parent().siblings().children().children('input').prop("readonly", false);
		//$(this).parent().parent().siblings().children().children('input[name="endereco"]').prop("readonly", false);
	});
	

	$('#lookTerm').colorbox({inline:true,scrolling:false});

	$('.bt_certificado').colorbox({inline:true, scrolling:false});

	var selectAtividade = $('.search select[name="atividade"]');
	var selectEstado = $('.search select[name="estado"]');
	var selectCidade = $('.search select[name="cidade"]');
	var selectTipo = $('.search select[name="tipo"]');
	var selectInstrutor = $('.search select[name="instrutor"]');
	var inputNameDiv = $('#sugestName');
	var inputName = $('.search input[name="name"]');
	
	$('.search form input[name="name"]').focus(function(){
		if($(this).val() == ''){
			$('#sugestName').fadeIn();
		}else{
			$('#sugestName').fadeOut();
		}
		
	});
	
	selectEstado.change(function(){

		selectAtividade.attr('disabled','disabled').children('option:eq(0)').attr('selected','selected');
		selectCidade.attr('disabled','disabled').children('option:eq(0)').attr('selected','selected');
		selectTipo.attr('disabled','disabled').children('option:eq(0)').attr('selected','selected');
		selectInstrutor.attr('disabled','disabled').children('option:eq(0)').attr('selected','selected');
		inputName.attr('disabled','disabled').val('');
		inputNameDiv.empty();

		

		var valor = $(this).val();
		if(valor=='0'){
			alert('Precisa selecionar o critério de busca.');
			
		}else{
			$('.loader').fadeIn();
			$.ajax({
				type:'post',
				url:LOCAL+'cursos/busca',
				data: {estado:valor}
			}).done(function(data){	
				
					selectCidade.empty();
					selectCidade.append("<option value='0'>Selecione Cidade</option>");
					
					for(var i in data){
						selectCidade.append("<option value="+data[i].id+">"+data[i].name+"</option>");
					}
					selectCidade.prop('disabled',false);
				
				$('.loader').fadeOut();
			}).fail(function(){
				alert('Erro ao buscar, tente novamente');
				$('.loader').fadeOut();
			});
		}
	});
	selectCidade.change(function(){

		selectAtividade.attr('disabled','disabled').children('option:eq(0)').attr('selected','selected');
		selectTipo.attr('disabled','disabled').children('option:eq(0)').attr('selected','selected');
		selectInstrutor.attr('disabled','disabled').children('option:eq(0)').attr('selected','selected');
		inputName.attr('disabled','disabled').val('');
		inputNameDiv.empty();

		var valor = $(this).val();

		
		
		if(valor=='0'){
			alert('Precisa selecionar o critério de busca.');	
		}else{
			$('.loader').fadeIn();
			$.ajax({
				type:'post',
				url:LOCAL+'cursos/busca',
				data: {cidade:valor}
			}).done(function(data){	
					// Nova regra
					selectTipo.empty();
					selectTipo.append("<option value='0'>Tipos</option>");

					for(var i in data){
						selectTipo.append("<option value="+data[i].id+">"+data[i].name+"</option>");	
					}

					selectTipo.prop('disabled',false);
				
				$('.loader').fadeOut();
					/*selectAtividade.empty();
					selectAtividade.append("<option value='0'>Área de atuação</option>");
					
					for(var i in data){
						selectAtividade.append("<option value="+data[i].id+">"+data[i].name+"</option>");
					}
					selectAtividade.prop('disabled',false);
				
				$('.loader').fadeOut();*/
			}).fail(function(){
				alert('Erro ao buscar, tente novamente');
				$('.loader').fadeOut();
			});
		}
	});

	selectAtividade.change(function(){

		selectTipo.attr('disabled','disabled').children('option:eq(0)').attr('selected','selected');
		selectInstrutor.attr('disabled','disabled').children('option:eq(0)').attr('selected','selected');
		inputName.attr('disabled','disabled').val('');
		inputNameDiv.empty();
		

		var valor = $(this).val();
		var cidade = selectCidade.val();

		if(valor=='0'){
			alert('Precisa selecionar o critério de busca.');
			
		}else{
			$('.loader').fadeIn();
			$.ajax({
				type:'post',
				dataType: "json",
				url: LOCAL+'cursos/busca',
				data: {atividade:valor,cidade_atividade:cidade}
			}).done(function(data){	
					
					selectTipo.empty();
					selectTipo.append("<option value='0'>Tipos</option>");

					for(var i in data){
						selectTipo.append("<option value="+data[i].id+">"+data[i].name+"</option>");	
					}

					selectTipo.prop('disabled',false);
				
				$('.loader').fadeOut();
			}).fail(function(erro){
				
				alert('Erro ao buscar, tente novamente');
				$('.loader').fadeOut();
			});
		}
	});

	selectTipo.change(function(){

		selectInstrutor.attr('disabled','disabled').children('option:eq(0)').attr('selected','selected');
		inputName.attr('disabled','disabled').val('');
		inputNameDiv.empty();

		$('.loader').fadeIn();
		var valor = $(this).val();
		var cidade = selectCidade.val();
		//var atividade = selectAtividade.val();
		$.ajax({
			type:'post',
			url:LOCAL+'cursos/busca',
			data: {tipo:valor, cidade_tipo:cidade}
		}).done(function(data){	
			
				selectInstrutor.empty();
				selectInstrutor.append("<option value='0'>Instrutores</option>");
				
				for(var i in data){
					selectInstrutor.append("<option value="+data[i].id+">"+data[i].name+"</option>");
				}
				selectInstrutor.prop('disabled',false);
			
			$('.loader').fadeOut();
		}).fail(function(){
			alert('Erro ao buscar, tente novamente');
			$('.loader').fadeOut();
		});
	});
	/*selectInstrutor.change(function(){

		inputName.attr('disabled','disabled').val('');
		inputNameDiv.empty();

		var valor = $(this).val();
		var cidade = selectCidade.val();
		var atividade = selectAtividade.val();
		var tipo = selectTipo.val();
		$('.loader').fadeIn();
		$.ajax({
			type:'post',
			url:LOCAL+'cursos/busca',
			data: {instrutor:valor, atividade:atividade, cidade:cidade, tipo:tipo}
		}).done(function(data){	
			
				inputNameDiv.empty();
				for(var i in data){
					inputNameDiv.append("<li>"+data[i].name+"</li>");
				}
				inputName.prop('disabled',false);
			
			$('.loader').fadeOut();
		}).fail(function(){
			alert('Erro ao buscar, tente novamente');
			$('.loader').fadeOut();
		});
	});
	$('#sugestName').on("click","li",function(){
		var texto = $(this).text();
		
		$('.search form input[name="name"]').val(texto);
		$('#sugestName').fadeOut();
	});*/
	$('#pass').focus(function(){
		

			var pass = $('<input id="passt" name="password" type="password">');
	        $(this).replaceWith(pass);
	        pass.focus();
		
	});
	
	$('.bt_agendar').click(function(){
		var pos = $('.actived').parent().parent().parent().parent('li').index();
		var atualMidHeight = $('.actived').parent().parent().parent().siblings('.mid').height();

		$(this).addClass('actived');
		$(this).parent().parent().parent().parent().siblings('li').children().children().children().children('td').removeClass('actived');

		var nextPos = $(this).parent().parent().parent().parent('li').index();
		
		$(this).parent().parent().parent().siblings('.mid').fadeIn();
		
		$(this).parent().parent().parent().parent().siblings('li').children('.mid').fadeOut();
		
		var offset = $(this).offset().top;
		
		if(pos<nextPos && pos != '-1'){
	
			offsetR = offset-418-atualMidHeight;
		}else if(pos<nextPos && pos == '-1'){
			
			offsetR = offset-403;
		}else if(pos==nextPos){
			offsetR = offset-403;
		}else{
			offsetR = offset-403;
		}

		var container = $('#datewrap');
		
		container.animate({
    		marginTop: offsetR
		},500);
		
		//funçao ajax
		var curso_id = $(this).attr('data-curso');
		var instructor_id = $(this).attr('data-instructor');

		$.ajax({
			url:LOCAL+'curso/data',
			type:'post',
			data:{curso_id:curso_id}
		}).done(function(data){
			var newDate = null;
			var arr = [];
			var mydate = "";

			console.log(data.agendas);

			for(var i in data.agendas){

				mydate = data.agendas[i].date_ini.split(" ");
				mydate = mydate[0].split("-");
				var ano = mydate[0];
				var mes = parseFloat(mydate[1])
				var dia = parseFloat(mydate[2]);
				newDate = ano+"-"+mes+"-"+dia;
				
				arr.push(newDate);
			}
			
		    loadDatePicker(arr, data.curso_days);
	
		});
	});

	//DATEPICKER
	$('#datepicker').multiDatesPicker();
	
	function loadDatePicker(availableDates, avaliableInstructorDates) {

	    $("#datepicker").multiDatesPicker("destroy");
	    var  instructorDates =null;
	    if(avaliableInstructorDates!=false){
	    	instructorDates = avaliableInstructorDates.split(',');
	    }
	   
	
	    function available(date) {
	    	 var day = date.getDay();
	    	
	     

		  dmy = date.getFullYear() + "-" + (date.getMonth()+1) + "-" + date.getDate();
		  newdmy = date.getFullYear() + "-" + ('0' + (date.getMonth()+1)).slice(-2)+ "-" + ('0' + date.getDate()).slice(-2);
		 
		 if(availableDates==null || instructorDates==null){
				availableDates = dmy;
		  }
		  if ( ($.inArray(dmy, availableDates) != -1) || ($.inArray(newdmy, instructorDates) == -1) ) {
		    return [false, "","Não está disponível neste dia."];
		  } else {
		    return [true,"","Disponível"];
		  }
		}

		$('#datepicker').removeClass('hasDatepicker');
		
		$('#datepicker').multiDatesPicker({
		 	
		 	minDate: "+7D", 
		 	maxDate: "+6M",
		 	
		 	beforeShowDay: available, 
		 	onSelect: function (dateText, inst){
		 		
		 		$('#datepicker').multiDatesPicker('removeDates', new Date());
		 		var dates = $('#datepicker').multiDatesPicker('getDates');
		 		
				$('.passDate').val(dates);
				
			} 
		});

	   $( "#datepicker" ).multiDatesPicker('resetDates', 'disabled');
  	}


	$('#frmCadastro').validate({
		rules:{
			name:{required:true},
			cpf:{required:true, cpf:true},
			profissao:{required:true},
			informe:{required:true},
			empresa:{required:true},
			cargo:{required:true},
			profissao:{required:true},
			endereco:{required:true},
			tel:{required:true},
			cel:{required:true},
			cep:{required:true},
			cidade_id:{required:true},
			estado_id:{required:true},
			email:{required:true, email:true}
		},
		errorPlacement: function(error, element) {
			$(element).parent().siblings('.error').append(error);
		},
		messages:{
			name:{required:"Favor, preencher campo Nome Completo."},
			cpf:{required:"Favor, preencher campo CPF", cpf:"Favor inserir CPF v&aacute;lido."},
			profissao:{required:"Favor, preencher campo Profiss&atilde;o."},
			informe:{required:"Favor, preencher campo"},
			empresa:{required:"Favor, preencher campo Empresa."},
			cargo:{required:"Favor, preencher campo Cargo."},
			profissao:{required:"Favor, preencher campo Profiss&atilde;o."},
			endereco:{required:"Favor, preencher campo Endere&ccedil;o."},
			tel:{required:"Favor, preencher campo Telefone."},
			cel:{required:"Favor, preencher campo Celular."},
			cep:{required:"Favor, preencher campo CEP."},
			cidade_id:{required:"Favor, preencher campo Cidade."},
			estado_id:{required:"Favor, preencher campo Estado."},
			email:{required:"Favor, preencher campo Email.", email:"Email inv&aacute;lido."}
		},
		submitHandler: function (form) {
			$('.wrap_loader').append('<img style="vertical-align:-11px;" src="'+LOCAL+'images/load.gif" /> Carregando...');
			form.submit();
		}
	});

	$('#frmCreateCurso').validate({
		rules:{
			endereco:{required:true},
			tel:{required:true},
			cel:{required:true},
			cep:{required:true},
			email:{required:true, email:true},
			//profissao:{required:true, min:1},
			name:{required:true},
			checkIn:{validateCheckbox:true},
		},
		errorPlacement: function(error, element) {
			if (element.attr("name") == "checkIn"){
				$('.checkError').append(error);
			}else{
				$(element).parent().siblings('.error').append(error);
			}
			
		},
		messages:{
			endereco:{required:"Favor, preencher campo Endere&ccedil;o."},
			tel:{required:"Favor, preencher campo Telefone."},
			cel:{required:"Favor, preencher campo Celular."},
			cep:{required:"Favor, preencher campo CEP."},
			email:{required:"Favor, preencher campo Email.", email:"Email inv&aacute;lido."},
			//profissao:{required:"Favor, preencher campo Profiss&atilde;o", min:"Favor, preencher campo Profiss&atilde;o"},
			name:{required:"Favor, preencher campo Nome."},
			checkIn:{validateCheckbox:"Concorde com os termos"},
		},
		submitHandler: function (form) {
			/*var convidados = $('input.convidado');

			var email = new Array();
						
			$.each(convidados, function(i,obj){
				var valor = $(this).val();
				email.push(valor);
	
			});
						
			var sorted_arr = email.sort();  
			                            
			var results = [];
			for (var i = 0; i < email.length - 1; i++) {
			    if (sorted_arr[i + 1] == sorted_arr[i]) {
			        results.push(sorted_arr[i]);
			    }
			}

			if(results.length > 0){
				alert('Existem emails duplicados em seus dados dos participantes, favor verificar!');
				return false;
			}
			*/		
			var confirmation = confirm("O endereço que será realizado o evento está correto? Preencha os dados dos participantes corretamente porque serão utilizados na impressão do certificados .");
			if(confirmation==true){
				$('.wrap_loader:last').append('<img style="vertical-align:-11px;" src="../../images/load.gif" /> Carregando...');
				form.submit();
			}
			
		}
	});

	$('.bt_confirm_curso').click(function(){
		var mdat = $(this).parent().siblings('form').children('.passDate').val();
		if(mdat!=''){
			var aid = $(this).siblings('select[name="atividade_id"]').val();
			$(this).parent().siblings('form').children('input[name="atividade_id"]').val(aid);
			$(this).parent().siblings('form').submit();
		}else{
			alert('Favor selecione o dia');
		}
		
	});


  	/*Apagar usuario convidado via ajax**/
  	$('.deleteEmployerBt').click(function(){

  		var c = confirm("Você deseja apagar este participante?");
  		if(c == true){

  		var uid = $(this).attr('name');
  		var element = $(this);
  		element.siblings('.wrap_loader').append('<img style="vertical-align:-11px;" src="../../images/load.gif" /> Carregando...');
  		window.location.href= LOCAL + 'curso/delete/employer/'+uid;

  		}else{
  			return false;
  		}
  		/*$.ajax({
  			url: LOCAL + 'curso/delete/employer',
  			type: 'post',
  			data:{uid:uid}
  		}).done(function(data){
  			
  			element.parent().parent('.item').remove();
  			var t = parseFloat($('.numEmp').text());
  			$('.numEmp').html(t-1);
  			alert("Convidado "+data.name+" apagado com sucesso!");
  			$('.wrap_loader').empty();
  		});*/
  	});

  	/**Mudar profissao outros campo informe**/
	$('#profissao').change(function(){
		var valor = $(this).val();
		if(valor == 'outros'){
			$('#informe').fadeIn();
		}else{
			$('#informe').fadeOut();
		}
	});
	
	$('#contato form').validate({
		rules:{
			name:{required:true},
			tel:{required:true},
			email:{required:true, email:true},
			assunto:{required:true},
			mensagem:{required:true}
		}, 
		errorPlacement: function(error, element) {
			$(element).parent().siblings('.error').append(error);
		},
		messages:{
			name:{required:"Favor, preencher campo Nome Completo."},
			tel:{required:"Favor, preencher campo Telefone."},
			email:{required:"Favor, preencher campo Email.", email:"Email inv&aacute;lido."},
			assunto:{required:"Favor, preencher campo Assunto."},
			mensagem:{required:"Favor, preencher campo Mensagem."}
		}

	});
	$('#frmCertificado').validate({

		rules:{
			curso_1:{required:true},
			curso_2:{required:true},
			curso_3:{required:true},
			instrutor_1:{required:true},
			instrutor_2:{required:true},
			instrutor_3:{required:true},
			instrutor_4:{required:true},
			instrutor_5:{required:true},
			curso_msg:{required:true}
			
		}, 
		errorPlacement: function(error, element) {
			$(element).siblings('.error').append(error);
		},
		messages:{
			curso_1:{required:"Este campo &eacute; obrigat&oacute;rio."},
			curso_2:{required:"Este campo &eacute; obrigat&oacute;rio."},
			curso_3:{required:"Este campo &eacute; obrigat&oacute;rio."},
			instrutor_1:{required:"Este campo &eacute; obrigat&oacute;rio."},
			instrutor_2:{required:"Este campo &eacute; obrigat&oacute;rio."},
			instrutor_3:{required:"Este campo &eacute; obrigat&oacute;rio."},
			instrutor_4:{required:"Este campo &eacute; obrigat&oacute;rio."},
			instrutor_5:{required:"Este campo &eacute; obrigat&oacute;rio."},
			curso_msg:{required:"Este campo &eacute; obrigat&oacute;rio."}
			
		}
	});
	 $('#tel, #cel').brTelMask();
	 $('#cep').mask('99999-999');
	 $('#cpf').mask('999.999.999-99');
});