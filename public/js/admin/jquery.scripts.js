
$.datepicker.regional['pt-BR'] = {
    closeText: 'Fechar',
    prevText: '&#x3c',
    nextText: '&#x3e;',
    currentText: 'Hoje',
    monthNames: ['Janeiro de','Fevereiro de','Mar&ccedil;o de','Abril de','Maio de','Junho de',
    'Julho de','Agosto de','Setembro de','Outubro de','Novembro de','Dezembro de'],
    monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
    'Jul','Ago','Set','Out','Nov','Dez'],
    dayNames: ['Domingo','Segunda-feira','Ter&ccedil;a-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sabado'],
    dayNamesShort: ['D','S','T','Q','Q','S','S'],
    dayNamesMin: ['D','S','T','Q','Q','S','S'],
    weekHeader: 'Sm',
    dateFormat: 'dd/mm/yy',
    firstDay: 0,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
$.datepicker.setDefaults($.datepicker.regional['pt-BR']);

if(location.hostname == 'incompany.dev'){
	var LOCAL = 'http://incompany.dev/';
}
else if(location.hostname == 'incompany.dizain.com.br'){
	var LOCAL = 'http://incompany.dizain.com.br/';
}else{
	var LOCAL = 'http://www.akademia-knauf.com.br/';
}

$(document).ready(function(){

		$('.clearCurso').click(function(){

		var myurl = $(this).attr('data-link');



		if(confirm("Deseja cancela este curso?")==true){

			window.location.href = myurl;

		}

		return false;

	});

	$('.select_all_employers').click(function(){
		
		if($(this).prop('checked') == false){
			 $('.select_employer').prop('checked',false);
		}else{
			$('.select_employer').prop('checked',true);
		}
		

		return;
	});

$('#trigger_edit_all_employers').click(function(){
	if($('select#action_employers').val() == '1'){
		$('#loaderD').fadeIn();
		$('#edit_all_employers').submit();
	}
});
	

    $( "#to" ).datepicker({

       minDate: new Date(), 

       showOtherMonths: true, 

       dateFormat: "dd/mm/yy"

    });
    $('select#estado_id').change(function(){
		var state = $(this).val();
		
		$.ajax({
			url:LOCAL+'city/search',
			type:'post',
			data:{state:state}
		}).done(function(data){
			console.log(data);
			$('#cidade_id').html(data.response).prop('disabled',false);
		});
	});

     $('.confirm_agenda').click(function(){
        var id = $(this).data('agenda');
       $.pgwModal({
            url: LOCAL+'instrutores/agenda/'+id+'/confirm',
            loadingContent: '<span style="text-align:center">Carregando dados...</span>',
            titleBar: false
        });
    });


     $('input.selec_all').click(function(){
		console.log($(this).prop('checked'));
		if($(this).prop('checked')==false){
			$('.selec_cursos').prop('checked',false);
			//$('input[name="cursos"]').val('');
		}else{
			$('.selec_cursos').prop('checked',true);

		}
		
	});

/*function available(date) {
	              var day = date.getDay();

	              dmy = ('0' + date.getDate()).slice(-2) + "/" + ('0' + (date.getMonth()+1)).slice(-2)+ "/" + date.getFullYear();
	             var availableDates = null;
	             if(availableDates==null){
	                    availableDates = dmy;
	              }
	             // console.log(dmy);
	              //console.log(availableDates);
	              if ( ($.inArray(dmy, availableDates) != -1)  ) {
	                return [true, 'custom-highlight', 'Esta ativo este dia'];
	              } else {
	                return [true,"","Disponível"];
	              }
	            };
	            $('#datepicker').multiDatesPicker({
	                minDate:  "+15D", 
	                maxDate: "+6M",
	                beforeShowDay: available,
	                onSelect: function (dateText, inst){

	                 

	                    //$('.passDate').val('');
	                    $('#datepicker').multiDatesPicker('resetDates', 'disabled');

	                    var dates = $('#datepicker').multiDatesPicker('getDates');
	                    //var pastDates = $('.passDate').val();

	                    
	                },
	                //beforeShowDay: function(inst) {clearToday(inst)},
     				//onChangeMonthYear: function(year, month, inst) {clearToday(inst)}
	            });
*/
	             
			        setTimeout(function() {
			            
			           $('td.ui-datepicker-today').removeClass('ui-state-highlight');
			        }, 10);
			  
  $('input[name="selec_all"]').click(function(){
      if($(this).prop('checked') == true){
        $('.curso_ids').prop('checked',true);
      }else{
        $('.curso_ids').prop('checked',false);
      }
   });
  
  $('input[name="selec_all_cities"]').click(function(){
      if($(this).prop('checked') == true){
        $('.check_cities').prop('checked',true);
      }else{
        $('.check_cities').prop('checked',false);
      }
   });

  $('select[name="action_select"]').change(function(){
  	$('#frmEditActions input[name="action"]').val($(this).val());
  });
  $('#btnEditActions').click(function(){
  	$('#frmEditActions').submit();
  });

    $(".nav-tabs a").click(function(){
        $(this).tab('show');
    });

    $('.data-selecionada .fa-close').click(function(){
    	var id = $(this).parent().parent('p').attr('id');
    	//alert(id);
    	$(this).parent().parent().next('table').remove();
    	//alert($('#pot_'+id).val());
    	$('#pot_'+id).remove();
    	$(this).parents('.data-selecionada').remove();

    	
    	
    	if($('.data-selecionada').size() == 0){
    		$('.no-select-date').show();
    	}
    });

});



