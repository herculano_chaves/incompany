<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
		<style>
			td { padding: 0; }
		</style>
	</head>
	<body>
		<table width="595" align="center" cellspadding="0" cellspacing="0" style="border: 1px solid #ccc;">
			<tr>
				<td colspan="2"><img src="{{URL::to('/')}}/images/emails/top.png" width="595" height="100" style="display:block; border:none;"></td>
			</tr>
			<tr>
				<td colspan="2"><p style="font:18px Arial; color:#313131; margin: 40px 0 0 30px;"><b>Dados do contato</b></p></td>
			</tr>
			<tr>
				<td><p style="font:18px Arial; color:#313131; margin-left: 30px;"><b>Nome: </b></p></td>
				<td><p style="font:18px Arial; color:#313131;">{{ $name }}</p></td>
			</tr>
			<tr>
				<td><p style="font:18px Arial; color:#313131; margin-left: 30px;"><b>Email: </b></p></td>
				<td><p style="font:18px Arial; color:#313131;">{{ $email }}</p></td>
			</tr>
			<tr>
				<td><p style="font:18px Arial; color:#313131; margin-left: 30px;"><b>Telefone: </b></p></td>
				<td><p style="font:18px Arial; color:#313131;">{{ $tel }}</p></td>
			</tr>
			<tr>
				<td><p style="font:18px Arial; color:#313131; margin-left: 30px;"><b>Assunto: </b></p></td>
				<td><p style="font:18px Arial; color:#313131;">{{ $assunto }}</p></td>
			</tr>
			<tr>
				<td><p style="font:18px Arial; color:#313131; margin-left: 30px;"><b>Mensagem: </b></p></td>
				<td><p style="font:18px Arial; color:#313131;">{{ $mensagem }}</p></td>
			</tr>
			<tr>
				<td colspan="2"><img src="{{URL::to('/')}}/images/emails/bottom1.png" width="595" height="82" style="display:block; border:none;"></td>
			</tr>
		</table>
	</body>
</html>