<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
		<style>
			td { padding: 0; }
		</style>
	</head>
	<body>
		<table width="595" align="center" cellspadding="0" cellspacing="0" style="border: 1px solid #ccc;">
			<tr>
				<td><img src="{{URL::to('/')}}/images/emails/top.png" width="595" height="100" style="display:block; border:none;"></td>
			</tr>
			<tr>
				<td><p style="font:18px Arial; color:#313131; margin: 40px 0 0 30px;"><b>Olá, {{ $user }}</b></p></td>
			</tr>
			<tr>
				<td><p style="font:16px Arial; color:#313131; margin: 10px 0 0 30px;">O curso {{ $agenda }} acaba de ser confirmado pelo Instrutor.</p></td>
			</tr>
			<tr>
				<td><p style="font:13px Arial; color:#313131; margin: 10px 0 10px 30px;"><b>Data:</b> {{ $data_agenda }}</p></td>
			</tr>
			<tr>
				<td><p style="font:13px Arial; color:#313131; margin: 10px 0 10px 30px;"><b>Horário:</b> {{ $hora_agenda }}h00 @if($periodo!='') - {{$periodo}} @endif</p></td>
			</tr>
			
			
			</tr>
			<tr>
				<td>
					<p style="margin: 10px 0 10px 30px;">Obrigado, e contamos com sua participação em mais cursos da Akademia Knauf!</p>
				</td>
			</tr>
			
			<tr>
				<td><img src="{{URL::to('/')}}/images/emails/bottom1.png" width="595" height="82" style="display:block; border:none;"></td>
			</tr>
		</table>
	</body>
</html>