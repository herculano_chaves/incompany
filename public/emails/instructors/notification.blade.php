<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
		<style>
			td { padding: 0; }
		</style>
	</head>
	<body>
		<table width="595" align="center" cellspadding="0" cellspacing="0" style="border: 1px solid #ccc;">
			<tr>
				<td><img src="{{URL::to('/')}}/images/emails/top.png" width="595" height="100" style="display:block; border:none;"></td>
			</tr>
			<tr>
				<td><p style="font:18px Arial; color:#313131; margin: 40px 0 0 30px;"><b>Olá, {{ $user }}</b></p></td>
			</tr>
			<tr>
				<td><p style="font:16px Arial; color:#313131; margin: 10px 0 0 30px;">O curso {{ $agenda->curso->consumer_name }} acaba de ser alterado seus dados de endereço.</p></td>
			</tr>
			
			<tr><td><p style="font:13px Arial; color:#313131; margin: 10px 0 10px 30px;"><b>Endereço:</b> {{ $agenda->endereco }}</p></td></tr>
			
			<tr><td><p style="font:13px Arial; color:#313131; margin: 10px 0 10px 30px;"><b>Cep:</b> {{ $agenda->cep }}</p></td></tr>
			<tr><td><p style="font:13px Arial; color:#313131; margin: 10px 0 10px 30px;"><b>Telefone:</b> {{ $agenda->tel }}</p></td></tr>
			
			</tr>
			<tr><td><p style="font:13px Arial; color:#313131; margin: 10px 0 10px 30px;"><b>Telefone:</b> {{ $agenda->cel }}</p></td></tr>
			
			</tr>
		
			<tr>
				<td><img src="{{URL::to('/')}}/images/emails/bottom1.png" width="595" height="82" style="display:block; border:none;"></td>
			</tr>
		</table>
	</body>
</html>