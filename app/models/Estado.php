<?php

class Estado extends Eloquent {
	protected $fillable = [];

	public function cidades(){
		return $this->hasMany('Cidade');
	}

	public function users(){
		return $this->hasMany('User');
	}

	public function cursos(){
		return $this->hasManyThrough('Curso', 'Cidade');
	}

	public function scopeGetStateByCities($query, $c){
		return $query->join('cidades', 'cidades.estado_id','=', 'estados.id')
		->whereIn('cidades.id', $c)
		->groupBy('estados.id')
		->select('estados.*');
	}
}