<?php
class Available extends Eloquent {
	
	protected $fillable = ['date','hours'];
	
	public $timestamps = false;
	
	public function curso(){
		return $this->belongsTo('Curso');
	}

	public function cidades()
	{
		return $this->belongsToMany('Cidade','availables_cidades');
	}

	public function unHorario()
	{
		
		return $this->hours != null ? unserialize(base64_decode($this->hours)) : array();
	}

	static function getHourText($h)
	{
		$p = Periodo::find($h);

		return $p->hour_ini;
	}
	public function getNextDate()
	{
		if($this->date_at >= date('Y-m-d')){
			return $this->date_at;
		}

		return false;
	}

	public function scopeNextDate($query){
		$query->where('date_at', '>=', date('Y-m-d'));
	}
	static function getByDate($d, $t, $i)
	{
		return $cursos = Curso::whereHas('availables', function($q) use($d){
			$q->whereDate_at($d);
		})->whereInstructor_id($i)->tchar($t)->get();
	}

	public static function boot()
	{
		parent::boot();

		static::deleted(function($a){
			
			$a->cidades()->detach($a->cidades()->lists('id'));
		
			
		});
	}
}