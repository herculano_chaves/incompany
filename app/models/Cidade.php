<?php

class Cidade extends Eloquent {
	protected $fillable = ['estado_id,name'];
	public static $rules = ['estado_id'=>'required','name'=>'required'];
	protected $with = array('estado');

	public function estado(){
		return $this->belongsTo('Estado');
	}

	public function instructors()
	{
		return $this->belongsToMany('Instructor', 'instructor_cidades');
	}

	public function users(){
		return $this->hasMany('User');
	}

	public function agendas(){
		return $this->hasMany('Agenda');
	}

	public function availables()
	{
		return $this->belongsToMany('Available','availables_cidades');
	}

	public function scopeFindByInstructor($query, $instructor_id, $caracter_id){
		/*return $query->join('instructor_cidades', 'cursos.instructor_id', '=', 'instructors.id')
		->join('tipos', 'cursos.tipo_id', '=', 'tipos.id')
        ->where('tipos.caracter_id','=',$caracter_id)
		->where('instructors.id', '=', $instructor_id)
		->where('cursos.status','1')
		->select('cidades.*');*/

		//return $query->whereHas('instructors', function())
	}
}