<?php

class Employer extends Eloquent {
	
	protected $fillable = [];

	public $timestamps = false;

	public function agenda(){
		return $this->belongsTo('Agenda');
	}
	public function survey(){
		return $this->hasOne('Survey');
	}
	public function scopePresents($query){
		return $query->where('presence','=','1');
	}
}