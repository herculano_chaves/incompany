<?php
class Curso extends Eloquent {

	// Add your validation rules here
	public static $rules = [
		 'consumer_id' => 'required',
		 'tipo_id' => 'required',
		 'instructor_id' => 'required',
		 'min_quorum' => 'required',
		 'max_quorum' => 'required',
		 //'disponibilidade' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = array('name','excerpt','description', 'consumer_id','tipo_id','instructor_id','min_quorum','max_quorum','disponibilidade');
	protected $with = array('consumer');
	public function consumer(){
		return $this->belongsTo('Consumer');
	}

	public function availables()
	{
		return $this->hasMany('Available')->where('availables.date_at','>=', date('Y-m-d'));
	}
	
	public function atividades(){
		return $this->belongsToMany('Atividade', 'curso_atividades');
	}
	public function agendas(){
		return $this->hasMany('Agenda');
	}
	public function getCidadesThroughAvailablesDate($c,$d){
		$cidades = Curso::join('availables', 'cursos.id','=','availables.curso_id')
		->join('availables_cidades as AC', 'availables.id', '=', 'AC.available_id')
		->join('cidades','AC.cidade_id','=','cidades.id')
		->where('cursos.id', $c)
		->whereIn('availables.date_at', $d)
		->select('cidades.*');
		//print_r(DB::getQueryLog($cidades));
		return $cidades;
	}
	public function tipo(){
		return $this->belongsTo('Tipo');
	}
	public function instructor(){
		return $this->belongsTo('Instructor');
	}
	public function caracter(){
		return $this->belongsTo('Caracter');
	}
	public function user(){
		return $this->belongsTo('User');
	}

	public function getConsumerNameAttribute(){
		if(isset($this->consumer->name)){
			return $this->consumer->name;
		}
		return $this->name;
	}

	public function getDataDispAttribute()
	{
		return $this->disponibilidade != '' ? unserialize(base64_decode($this->disponibilidade)) : '';
	}

	static function getNameById($id)
	{
		$curso = Curso::find($id);

		return $curso->consumer_name;
	}
	static function getPeriodById($id)
	{
		$period = Periodo::find($id);
		$res = $period->caracter->name.' - ';
		$res .= $period->hour_ini.'hrs';

		if($period->name != ''){
			$res .=' - '.$period->name;
		}  

		return $res;
	}

	public function periodo($d){

		$a = Available::whereCurso_id($this->id)->whereDate_at($d)->first();

		$disp = unserialize(base64_decode($a->hours));

		if($disp != null){
		
			$periodos = Periodo::find($disp);
			$output = '';
			foreach ($periodos as $p) {
				$name = null;
				if($p->name): $name = $p->name; endif;
				//$output .= $p;
				$output .= '<option value="'.$p->id.'">De '.$p->hour_ini.'h00 até '.($p->hour_ini+$this->tipo->duracao).'h00 ' .$name. '</option>';
			}
			return $output;
			
		}
		return '<option value="">Ainda não tem horário disponível</option>';
	}

	public function statusPeriodo(){
		//$availables = Available::whereCurso_id($this->id)->get();
		if(unserialize(base64_decode($this->disponibilidade_horarios)) == null){
			return false;
		}
		return '1';
	}
	public function statusPeriodoText(){

		if($this->availables->count() < '1'){
			return '<div class="site_status">* Ainda não tem horário disponível para este curso</div>';
		}
		$diff = array_diff($this->availables->lists('date_at'), $this->agendas->lists('date_ini'));
		/*print_r($this->availables->lists('date_at'));
		print_r($this->agendas->lists('date_ini'));
		print_r($diff);
		count($diff);*/
		if(count($diff) <= 0){
			return '<div class="site_status">* Todas as agendas estão cheias</div>';
		}
		
		$atividades = Atividade::whereHas('cursos', function($c){
			$c->whereIn('cursos.id', array($this->id));
		})->orderBy('name')->lists('name','id');
		
		$output = '<p>';
		$output .= Form::select('atividade_id', [''=>'Selecione atividade']+$atividades, null, ['style'=>'border: 1px solid #ccc;height: 38px;float: left;margin: -1px 15px 0 0']);
		$output .= '<a class="bt blue bt_confirm_curso" href="javaScript:;">Prosseguir com solicitação</a></p>';
		return $output;
	}

	public function scopeOnlyActive($query){
		return $query->whereStatus('1');
	}

	public function scopeEstado($query, $estado_id){
		return $query->join('cidades', 'cidades.id','=','cursos.cidade_id')
				->join('estados','cidades.estado_id','=','estados.id')
				->where('estados.id','=',$estado_id)
				->groupBy('estados.id')
				->select(array('cursos.*'));;
	}

	public function scopeTchar($query, $caracter_id){
		
		return $query->join('tipos', 'cursos.tipo_id', '=', 'tipos.id')
            			->where('tipos.caracter_id','=',$caracter_id)
            			->select('cursos.*');
	}

	/*public function scopeGetCidade($query, $cidade_id){
		return $query->join('cidades', 'cursos.cidade_id','=','cidades.id')
		->where('cidades.id',$cidade_id)
		->select('cursos.*');
	}*/

	public function scopeGetInstructor($query, $instructor_id){
		return $query->join('instructors', 'cursos.instructor_id','=','instructors.id')
		->where('instructors.id',$instructor_id)
		->select('cursos.*');
	}

	public function statusText(){
		return $this->status == '1' ? 'Ativo' : 'Inativo';
	}

	public function getCursoHorario()
	{
		return Periodo::where('caracter_id', '=',$this->tipo->caracter->id)->orderBy('hour_ini','ASC')->get();
	}

	public function unHorario()
	{
		
		return $this->disponibilidade_horarios != null ? unserialize(base64_decode($this->disponibilidade_horarios)) : array();
	}

	public function unDisp()
	{
		return $this->disponibilidade != '' ? unserialize(base64_decode($this->disponibilidade)) : '';
	}

	public function findAvailable($d)
	{
		$a = Available::whereCurso_id($this->id)->whereDate_at($d)->first();

		return $a->id;
	}

	public static function boot()
	{
		parent::boot();

		static::deleted(function($curso){
			
			$curso->atividades()->detach($curso->atividades->lists('id'));
		
			
		});
	}
}