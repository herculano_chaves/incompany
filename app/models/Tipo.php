<?php

class Tipo extends Eloquent {
	protected $fillable = ['name','duracao','caracter_id'];
	public static $rules = ['name'=>'required', 'duracao'=>'required'];

	public function cursos(){
		return $this->hasMany('Curso');
	}
	public function caracter(){
		return $this->belongsTo('Caracter');
	}
	public function periodo(){
		return $this->belongsTo('Periodo');
	}
}