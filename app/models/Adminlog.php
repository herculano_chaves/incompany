<?php

class Adminlog extends \Eloquent {
	protected $fillable = [];

	public function agenda(){
		return $this->belongsTo('Agenda');
	}
	public function instructor(){
		return $this->belongsTo('Instructor');
	}
	public function user(){
		return $this->belongsTo('User');
	}
	public function statusText($link=false){
		if($this->status == '1'):
			return 'Concluído'; 
		elseif($this->status == '2'):
			return 'Cancelado';
		elseif($this->status == '3'):
			return 'Confirmado';
		else:
			return 'Aguardando';
		endif;

	}
}