<?php

class Caracter extends \Eloquent {
	protected $fillable = ['name'];

	public function tipos(){
		return $this->hasMany('Tipo');
	}
	public function periodos(){
		return $this->hasMany('Periodo');
	}
	public function cursos(){
		return $this->hasManyThrough('Curso','Tipo','caracter_id','tipo_id');
	}
}