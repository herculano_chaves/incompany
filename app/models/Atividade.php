<?php
class Atividade extends Eloquent {
	protected $fillable = ['name'];
	public static $rules = ['name'=>'required'];
	
	public function cursos(){
		return $this->belongsToMany('Curso', 'curso_atividades');
	}

	public function agendas()
	{
		return $this->hasMany('Agenda');
	}
}