<?php

class Consumer extends \Eloquent {
	protected $fillable = ['name','excerpt','description'];

	public static $rules = [
		'name'=>'required',
		'excerpt'=>'required',
		'description'=>'required'
	];

	public function cursos(){
		return $this->hasMany('Curso');
	}
}