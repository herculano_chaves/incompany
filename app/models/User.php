<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	protected $table = 'users';
	protected $fillable = ['email','name', 'cpf', 'profissao', 'empresa', 'cargo', 'endereco', 'cep', 'tel', 'cel', 'estado_id', 'cidade_id'];
	protected $guarded = ['id','password'];
	public static $rules = [
		'name' => 'required',
		'email' => 'required|email|unique:users',
		'cpf' => 'required',
		'empresa' => 'required',
		'cargo' => 'required',
		'endereco' => 'required',
		'cep' => 'required',
		'tel' => 'required',
		'cel' => 'required',
		//'estado_id' => 'required',
		//'cidade_id' => 'required',
	 ];
	
	protected $hidden = array('password', 'remember_token');

	public function agendas(){
		return $this->hasMany('Agenda');
	}
	public function adminlog(){
		return $this->hasOne('Adminlog');
	}
	public function instrutor(){
		return $this->hasOne('Instructor');
	}
	public function cursos(){
		return $this->hasManyThrough('Curso','Instructor');
	} 
	public function groups(){
		return $this->belongsToMany('Group','users_groups');
	}
	public function estado(){
		return $this->belongsTo('Estado');
	}
	public function cidade(){
		return $this->belongsTo('Cidade');
	}
	/**
	* Verificando relacionamentos e excluindo 
	*/
   /* protected static function boot(){
    	parent::boot();
    	static::deleting(function($user){
    		$user->agendas()->delete();
    		$user->instrutor->delete();
    		$user->groups()->detach();
    	});
    }*/
}
