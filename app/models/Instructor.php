<?php

class Instructor extends Eloquent {
	protected $fillable = ['user_id','name'];

	public function cursos(){
		return $this->hasMany('Curso');
	}

	public function user(){
		return $this->belongsTo('User');
	}

	public function agendas(){
		return $this->hasManyThrough('Agenda','Curso');
	}
	public function adminlog(){
		return $this->hasOne('Adminlog');
	}

	public function cidades()
	{
		return $this->belongsToMany('Cidade', 'instructor_cidades')->orderBy('name','asc');
	}
}