<?php

class Agenda extends Eloquent {
	protected $fillable = ['curso_id', 'user_id', 'cidade_id', 'atividade_id', 'periodo_id', 'date_ini', 'endereco', 'cep', 'email', 'tel', 'cel', 'share_url', 'status'];

	public $timestamps = false;
	
	public function curso(){
		return $this->belongsTo('Curso');
	}
	public function atividade(){
		return $this->belongsTo('Atividade');
	}
	public function employers(){
		return $this->hasMany('Employer');
	}
	public function adminlog(){
		return $this->hasOne('Adminlog');
	}
	public function user(){
		return $this->belongsTo('User');
	}
	public function periodo(){
		return $this->belongsTo('Periodo');
	}
	public function cidade(){
		return $this->belongsTo('Cidade');
	}

	public function instructor(){
		return $this->belongsTo('Instructor');
	}

	public function getLink(){
		$route_name = 'employers/show';
		//echo Route::current()->getPath();
		if(Route::current()->getPath() == 'admin/instrutor/{id}/cursos'){
			$route_name = 'employers/show/admin';
		}
		return '<a href="'.route($route_name,['id'=>$this->id]).'">Alunos</a>';
	}

	public function scopeAtivos($query){
		return $query->where('status','=','0');
	}
	public function scopeCompletos($query){
		return $query->where('status','=','1');
	}
	public function scopeCancelados($query){
		return $query->where('status','=','2');
	}

	public function scopeConfirmados($query){
		return $query->where('status','=','3');
	}

	public function shareUrl($default = null){
		if($this->status == '3' and $this->share_url != 'null'){
			return route('cadastro/participante', ['agenda_id'=>$this->id, 'share_url'=>$this->share_url]); 

		}

		return $default;
	}

	public function statusText($link=false){
		if($this->status == '1'): 
    		return $link==true ? "Concluído (".link_to_route('agenda/back','Desfazer conclusão',['id'=>$this->id]).")" : "Concluído"; 
    	elseif($this->status == '2'):
    	
    		return "Cancelado"; 

    	elseif($this->status == '3'):

    		return $link==true ? "Confirmado (".link_to_route('agenda/finish','Concluir curso',['id'=>$this->id]).")" : "Confirmado"; 
    	else:
    		return 'Aguardando Confirmação (<a class="confirm_agenda" data-agenda="'. $this->id .'" href="javaScript:;">Confirmar</a>)';  
    	endif;
	} 

	
}