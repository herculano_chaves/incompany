<?php

class Survey extends Eloquent {
	protected $fillable = ['employer_id','curso_1','curso_2','curso_3','instrutor_1','instrutor_2','instrutor_3','instrutor_4','instrutor_5','curso_msg'];
	public $timestamps = false;
	public static $rules = [
		'curso_1'=>'required',
		'curso_2'=>'required',
		'curso_3'=>'required',
		'instrutor_1'=>'required',
		'instrutor_2'=>'required',
		'instrutor_3'=>'required',
		'instrutor_4'=>'required',
		'instrutor_5'=>'required'
	];

	public function employer(){
		return $this->belongsTo('Employer');
	}
	
}