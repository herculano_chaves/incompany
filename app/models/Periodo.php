<?php

class Periodo extends \Eloquent {
	protected $fillable = [];

	public function tipos(){
		return $this->hasMany('Tipo');
	}
	public function caracter(){
		return $this->belongsTo('Caracter');
	}
	public function agendas(){
		return $this->hasMany('Agenda');
	}
}