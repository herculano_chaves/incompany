<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth.admin', function()
{
	// Check if the user is logged in
	if ( ! Sentry::check())
	{

		// Redirect to the login page
		return Redirect::route('backend/login');
	}

	// Check if the user has access to the admin page
	if ( ! Sentry::getUser()->hasAccess('admin'))
	{
		// Show the insufficient permissions page
		Sentry::logout();
		return Redirect::route('backend/login')->with('error','Você não possui permissão de acesso!');
	}
});

Route::filter('auth.instructor', function()
{
	// Check if the user is logged in
	if ( ! Sentry::check())
	{

		// Redirect to the login page
		return Redirect::route('instructor/login');
	}

	// Check if the user has access to the admin page
	if ( ! Sentry::getUser()->hasAccess('instructor'))
	{
		// Show the insufficient permissions page
		Sentry::logout();
		return Redirect::route('instructor/login')->with('error','Você não possui permissão de acesso!');
	}
});

Route::filter('auth', function()
{
	if ( ! Sentry::check())
	{
	
		// Redirect to the login page
		return Redirect::route('user/login');
	}

});

Route::filter('admin.permission', function($route){
	$user = Sentry::getUser();

	if(!$user->hasAccess('admin')){

		return Redirect::back()->with('error','Você não tem esta permissão');

	}

});

Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});
