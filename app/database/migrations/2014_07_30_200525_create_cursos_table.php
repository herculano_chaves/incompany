<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCursosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cursos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('atividade_id');
			$table->integer('tipo_id');
			$table->integer('cidade_id');
			$table->integer('instrutor_id');
			$table->string('name');
			$table->text('description');
			$table->date('date_due_ini');
			$table->date('date_due_fin');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cursos');
	}

}
