<?php

class ConsumersController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /consumers
	 *
	 * @return Response
	 */
	public function index()
	{
		$consumers = Consumer::orderBy('id','DESC')->paginate(15);

		return View::make('consumers.index', compact('consumers'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /consumers/create
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('consumers.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /consumers
	 *
	 * @return Response
	 */
	public function store()
	{
		$data = Input::all();

		$validator = Validator::make($data, Consumer::$rules);

		if($validator->fails()){
			return Redirect::route('admin.eventos.create')->withErrors($validator)->withInput();
		}

		$consumer = Consumer::create($data);

		return Redirect::route('admin.eventos.index')->withSuccess('Criado com sucesso');
	}

	/**
	 * Display the specified resource.
	 * GET /consumers/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /consumers/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$consumer = Consumer::find($id);

		return View::make('consumers.edit', compact('consumer'));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /consumers/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$data = Input::all();

		$validator = Validator::make($data, Consumer::$rules);

		if($validator->fails()){
			return Redirect::route('admin.eventos.edit', $id)->withErrors($validator)->withInput();
		}

		$consumer = Consumer::find($id);
		$consumer->update($data);

		return Redirect::route('admin.eventos.edit', $id)->withSuccess('Editado com sucesso');
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /consumers/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$consumer = Consumer::find($id);
		$consumer->delete();

		return Redirect::route('admin.eventos.index')->withSuccess('Apagado com sucesso');
	}

}