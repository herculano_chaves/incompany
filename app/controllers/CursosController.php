<?php



class CursosController extends BaseController {

	

	protected $layout = 'layouts.base';


	/**

	 * Display a listing of cursos

	 *

	 * @return Response

	 */

	public function index()

	{

		$cursos = Curso::onlyActive()->get();



		$atividades = Atividade::all()->lists('name');

		array_unshift($atividades, 'Atividades');


		// Pegando estados onde instrutores tem disponibilidade
		$estados = Estado::whereHas('cidades', function($c){
			$c->has('availables');
		})
		->orderBy('name','ASC')->lists('uf','id');


		$instrutores = Instructor::all()->lists('name');

		array_unshift($instrutores, 'Instrutores');

		

		$tipos = Tipo::all()->lists('name');

		array_unshift($tipos, 'Tipos');



		return View::make('cursos.index', compact('atividades','estados','instrutores','tipos','cursos'));

		//return View::make('cursos.index');

	}



	public function adminCursos($caracter){

		$caracter_id = $caracter;

		$c = Curso::tchar($caracter_id);

		if(Input::has('cidade_id')){
			$c->getCidade(Input::get('cidade_id'));
		}
		if(Input::has('instructor_id')){
			$c->getInstructor(Input::get('instructor_id'));
		}
		if(Input::has('atividade')){
			$c->whereHas('atividade', function($q){
				$q->whereId(Input::get('atividade'));
			});
		}
		if(Input::has('tipo')){
			$c->whereHas('tipo', function($q){
				$q->whereId(Input::get('tipo'));
			});
		}

		$cidades = array();

		if(Input::has('estado_id')){
			$cidades = Cidade::whereEstado_id(Input::get('estado_id'))->orderBy('name','ASC')->lists('name','id');
		}


		
		$cursos = $c->orderBy('cursos.id','DESC')
		->paginate(10);
		

		$atividades = Atividade::orderBy('name')->lists('name','id');


		$estados = Estado::orderBy('name','ASC')->lists('uf','id');
		

		$instrutores = Instructor::orderBy('name')->lists('name','id');
	

		$tipos = Tipo::where('caracter_id', '=' ,$caracter_id)->get();
		
		return View::make('backend.cursos', compact('atividades','estados','instrutores','tipos','cursos','cidades','caracter_id'));

	}

	public function editActions($caracter_id){
	
			$data = Input::all();
			$c = Input::get('checkpoint_saved');
			$checkpoint = str_replace('{caracter}', $caracter_id, $c);

			if(empty($data['curso_ids'])){
				return Redirect::back()->withError('Selecione ao menos um ou mais cursos, para realizar a ação desejada.');
			}
			if(!Input::has('action')){
				return Redirect::back()->withError('Selecione a ação a ser executada.');
			}

			$cursos = Curso::whereIn('id',$data['curso_ids'])->get();

			$atividades = Atividade::all()->lists('name','id');


			$estados = Estado::orderBy('name','ASC')->lists('uf','id');
			$cidades = array();

			$instrutores = Instructor::all()->lists('name','id');
		

			$tipos = Tipo::where('caracter_id', '=' ,$caracter_id)->get();

			switch ($data['action']) {
				case '1':
					$view = 'backend.cursos.actions.form-local';
					$shared = compact('estados','cidades');
				break;
				case '2':
					$view = 'backend.cursos.actions.form-atividade';
					$shared = compact('atividades');
				break;
				case '3':
					$view = 'backend.cursos.actions.form-tipo';
					$shared = compact('tipos');
				break;
				case '4':
					$view = 'backend.cursos.actions.form-instrutor';
					$shared = compact('instrutores');
				break;
				case '5':
					$view = 'backend.cursos.actions.form-status';
					$shared = array();
				break;
				case '6':
					$view = 'backend.cursos.actions.form-minmax';
					$shared = array();
				break;
					
				default:
					$view = null;
				break;
			}
			return View::make('backend.cursos.edit-actions', compact('cursos','caracter_id','checkpoint'))
			->nest('form_action', $view, $shared);
	}

	public function updateActions(){
		$data = Input::all();
		$cursos = Curso::whereIn('id',$data['curso_ids']);
		switch ($data['action']) {
				case '1':
					$cursos->update(['cidade_id'=>Input::get('cidade_id')]);
				break;
				case '2':
					$cursos->update(['atividade_id'=>Input::get('atividade_id')]);
				break;
				case '3':
					$cursos->update(['tipo_id'=>Input::get('tipo_id')]);
				break;
				case '4':
					$cursos->update(['instrutor_id'=>Input::get('instrutor_id')]);
				break;
				case '5':
					$cursos->update(['status'=>Input::get('status')]);
				break;
				case '6':
					$cursos->update(['min_quorum'=>Input::get('min_quorum'), 'max_quorum'=>Input::get('max_quorum')]);
				break;
				default:
					$view = null;
				break;
			}
			$url = Input::get('checkpoint') != '' ? Input::get('checkpoint') : 'admin' ;
		return Redirect::to($url)->withSuccess('Cursos Editados com sucesso');
	}


	public function adminCursosCreate($caracter){

		$caracter_id = $caracter;

		//echo $caracter_id;

		$atividades = Atividade::all()->lists('name','id');
		

		$cidades = Cidade::orderBy('name','ASC')->lists('name','id');


		$estados = Estado::orderBy('name','ASC')->lists('uf','id');
		

		$instrutores = Instructor::all()->lists('name','id');

		$consumers = Consumer::orderBy('id')->lists('name','id');
	

		$tipos = Tipo::where('caracter_id', '=' ,$caracter_id)->get();

		return View::make('backend.cursos.create', compact('consumers','atividades','estados','instrutores','tipos', 'cidades','caracter_id'));
	}

	public function adminInstrutoresIns($id=null, $caracter_id=null){
		
		$instructor = Instructor::where('user_id','=',$id)->first();

		$c = Curso::where('instructor_id', '=', $instructor->id);
		
		if(Input::has('cidade_id')){
			//$c->whereCidade_id(Input::get('cidade_id'));
		}

		$cursos = $c->tchar($caracter_id)
		->orderBy('id','DESC')
		->paginate(10);

		
		if($cursos->count() < 1 and isset($id)){	
			$tipologia = $caracter_id == '1' ? 'Cursos' : 'Palestras';
			return Redirect::back()->with('error','Até agora não foram cadastrados '.$tipologia.' para este instrutor!');	
		}
		$buttons = null;

		$cidades = Cidade::findByInstructor($instructor->id, $caracter_id)->lists('name', 'id');

		return View::make('backend.cursos.index', compact('cursos','instructor', 'cidades'));
	}

	public function adminInstrutoresCursos($caracter_id=null){
		$u = Sentry::getUser();
		$user = User::find($u->id);

		/*if(!Session::has('dates')){
			return Redirect::back()->withError('Selecione datas');
		}*/
		$dates = Session::get('dates', array());
		
		$instructor = Instructor::where('user_id','=',$user->id)->first();
		
		$c = Curso::where('instructor_id', '=', $instructor->id);

		if(Input::has('cidade_id')){
			//$c->whereCidade_id(Input::get('cidade_id'));
		}
		if(Input::has('course_id')){
			$c->where('cursos.id',Input::get('course_id'));
		}

		if($caracter_id == 'cursos'){
			$c_id = 1;
			$tipologia = 'Meus cursos';
		}else{
			$c_id = 2;
			$tipologia = 'Minhas palestras';
		}

		$cursos = $c->tchar($c_id)
		->onlyActive()
		->orderBy('id','DESC')
		->get();
		
		if($cursos->count() < 1){	
			
			return Redirect::back()->with('error','Até agora não foram endontrados '.$tipologia.' neste critério de busca!');	
		}
		
		$buttons = null;

		//cidades = Cidade::findByInstructor($instr$uctor->id, $c_id)->lists('name', 'id');

		return View::make('backend.instructors.meus-cursos', compact('cursos','instructor', 'tipologia', 'dates','c_id'));

	}

	public function adminInstrutores($status='agendandos'){

		
		$u = Sentry::getUser();
		$user = User::find($u->id);
	

		$instructor = Instructor::where('user_id','=',$user->id)->first();

		$cursos = Curso::where('instructor_id', '=', $instructor->id)

		->paginate(10);

		if($status == 'concluidos'){
			$s=1;
			$a_active = null;
			$i_active = null;
			$c_active = 'active';
			$cc_active = null;
		}elseif($status == 'confirmados'){
			$s=3;
			$a_active = null;
			$i_active = 'active';
			$c_active = null;
			$cc_active = null;

		}elseif($status == 'cancelados'){
			$s=2;
			$a_active = null;
			$i_active = null;
			$c_active = null;
			$cc_active = 'active';
		}else{
			// agendados
			$s=0;
			$a_active = 'active';
			$c_active = null;
			$i_active = null;
			$cc_active = null;
		}
		$buttons = '<a class="btn btn-default '.$a_active.'" href="'.route('admin/instructors',['status'=>'agendados']).'">Agendados</a>
				<a class="btn btn-default '.$i_active.'" href="'.route('admin/instructors',['status'=>'confirmados']).'">Confirmados</a>
				<a class="btn btn-default '.$c_active.'" href="'.route('admin/instructors',['status'=>'concluidos']).'">Concluídos</a>
				<a class="btn btn-default '.$cc_active.'" href="'.route('admin/instructors',['status'=>'cancelados']).'">Cancelados</a>';

		$a = Agenda::join('cursos', 'agendas.curso_id','=','cursos.id')
		->join('instructors', 'cursos.instructor_id','=','instructors.id')
		->where('instructors.id',$instructor->id)
		->where('agendas.status',$s)
		->select('agendas.*');

		// Buscas
		//Estado / cidade / Curso / período

		
		if(Input::has('period_id')){
			$a->where('agendas.periodo_id', Input::get('period_id'));
		}
		if(Input::has('course_id')){
			$a->where('cursos.id', Input::get('course_id'));
		}
		

		$cities = array();
		if(Input::has('state_id')){
			$cities = Cidade::whereEstado_id(Input::get('state_id'))->orderBy('name','ASC')->lists('name','id');

			$a->join('cidades as c1','agendas.cidade_id','=','c1.id')
			->join('estados','c1.estado_id','=','estados.id')->where('estados.id',Input::get('state_id'));
		}

		if(Input::has('city_id')){
			$a->join('cidades as c2','agendas.cidade_id','=','c2.id')
			->where('c2.id',Input::get('city_id'));
		}

		$agendas = $a->get();

		if($agendas->count() > 0 ){
			$states = Estado::whereHas('cidades', function($c) use($agendas){
				$c->whereIn('id',$agendas->lists('cidade_id'));
			})->orderBy('name','ASC')->lists('uf','id');
		}else{
			$states = Estado::orderBy('name','ASC')->lists('uf','id');
		}
		$menu = View::make('layouts.backendmenuinstructors');

		if($cursos->count() < 1 and isset($id)){	
			
			return Redirect::back()->with('error','Até agora não foram cadastrados cursos para este instrutor!');	
		}

		return View::make('backend.instructors.cursos', compact('agendas', 'instructor', 'buttons','menu','states','cities','status'));

	}



	public function search(){

		if(Request::ajax()){

			if(Input::has('estado')){
				$response = Cidade::has('availables')
				->whereEstado_id(Input::get('estado'))
				->get();
			}
			if(Input::has('cidade')){
				$response = Tipo::whereHas('cursos', function($u){
					$u->onlyActive()->whereHas('availables', function($i){
						$i->whereHas('cidades', function($c){
							$c->whereIn('cidades.id',array(Input::get('cidade')));
						});
					});
				})
				->get();
				
			}
		
			if(Input::has('tipo')){

				$response = Instructor::whereHas('cursos', function($u){
					$u->onlyActive()->whereTipo_id(Input::get('tipo'))->whereHas('availables', function($i){
						$i->whereHas('cidades', function($c){
							$c->whereIn('cidades.id',array(Input::get('cidade_tipo')));
						});
					});
				})
				->get();
			}	

			return Response::json($response);
		}

		//return Redirect::route('cursos/busca')->with('success','Não encontramos esta busca');

	}



	public function searchall(){

		$atividades = Atividade::all()->lists('name','id');

		$a_1 = Atividade::find(Input::get('atividade'));

		

		$estados = Estado::whereHas('cidades', function($c){
			$c->has('availables');	
		})
		->orderBy('name','ASC')
		->lists('uf','id');

		$a_2 = Estado::find(Input::get('estado'));



		$cidades = Cidade::has('availables')->where('estado_id','=',Input::get('estado'))->lists('name','id');

		$a_3 = Cidade::find(Input::get('cidade'));


		$tipos = Tipo::whereHas('cursos', function($u){
					$u->onlyActive()->whereHas('availables', function($a){
						$a->whereHas('cidades', function($c){
							$c->whereIn('cidades.id',array(Input::get('cidade')));
						});
					});
				})->lists('name','id');
		
		$a_4 = Tipo::find(Input::get('tipo'));


		$instrutores = Instructor::whereHas('cursos', function($u){
					$u->onlyActive()->whereHas('availables', function($a){
						$a->whereHas('cidades', function($c){
							$c->whereIn('cidades.id',array(Input::get('cidade')));
						});
					})->whereTipo_id(Input::get('tipo'));
				})
				->lists('name','id');

		$a_5 = Instructor::find(Input::get('instrutor'));

		if(Input::has('estado') and (Input::get('cidade') == '0' or Input::get('cidade') == '')){
			
			$search = Curso::onlyActive()->whereHas('availables', function($a){
				$a->whereHas('cidades', function($c){
					$c->whereEstado_id(Input::get('estado'));
				});
			});

		}
		
		elseif(Input::has('cidade') and (Input::get('tipo') == '0' or Input::get('tipo') == '')){
			
			$search = Curso::onlyActive()->whereHas('availables', function($a){
				$a->whereHas('cidades', function($c){
					$c->whereIn('cidades.id', array(Input::get('cidade')));
				});
			});
						
		}
		elseif(Input::has('cidade') and Input::has('tipo') and (Input::get('instrutor') == '0' or Input::get('instrutor') == '')){
			
			$search = Curso::onlyActive()->whereHas('availables', function($a){
				$a->whereHas('cidades', function($c){
					$c->whereIn('cidades.id', array(Input::get('cidade')));
				});
			})->where('tipo_id','=', Input::get('tipo'));
						
		}
		elseif(Input::has('cidade') and Input::has('tipo') and Input::has('instrutor') and (Input::get('instrutor') != '0' and Input::get('instrutor') != '')){
			
			$search = Curso::onlyActive()->whereHas('availables', function($a){
				$a->whereHas('cidades', function($c){
					$c->whereIn('cidades.id', array(Input::get('cidade')));
				});
			})
			->where('tipo_id','=', Input::get('tipo'))
			->where('instructor_id','=', Input::get('instrutor'));
						
		}
		else{
			$search = false;
		}

		if($search === false){
			$search = null;
		}else{
			$search = $search->orderBy('cursos.id','DESC')
			->paginate(10);
		}
	
		$periodos = Periodo::all();

		$user = $this->getUser();

		return View::make('cursos.index',compact('search','user','periodos', 'atividades','estados','cidades','instrutores','tipos', 'a_1', 'a_2', 'a_3', 'a_4', 'a_5'));

	}



	public function data_confirm(){

		if(Request::ajax()){

			

			$agendas = Agenda::where('curso_id', '=', Input::get('curso_id'))

						->where('status','=','0')

						->get();

			$curso = Curso::find(Input::get('curso_id'));
			//unserialize(base64_decode($curso->disponibilidade))
			$availables = Available::whereCurso_id($curso->id)->lists('date_at');
			$availables = implode(',', $availables);
			return Response::json(['agendas'=>$agendas, 'curso'=>$curso, 'curso_days'=>$availables]);

		}

	}



	public function preconfirm(){	

		$data = Input::all();

		$validator = Validator::make($data, ['atividade_id'=>'required', 'curso_id'=>'required', 'passDate'=>'required']);

		if($validator->fails()){
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Session::set('dataCurso', $data);

		if(!isset($data['user_id']) and $data['user_id'] ==''){

			return Redirect::to('login')->with('success','Faça login para continuar a confirmar seu curso');

		}

		return  Redirect::route('cursos/create');

	}



	/**
	 * Show the form for creating a new curso
	 *
	 * @return Response
	 */

	public function create()

	{

		$data = Session::get('dataCurso');
		
		 $m = $data['passDate'];

		$dates = explode(',',$m);


		/*$ext_data = explode('/', $m);

		$mdate = $ext_data[2].'-'.$ext_data[1].'-'.$ext_data[0]; 

		$mdate = $this->Extenso(null,$mdate);

		$realDate =  $ext_data[2].'-'.$ext_data[1].'-'.$ext_data[0];*/

		$curso = Curso::find($data['curso_id']);


		switch($data['atividade_id']){
			case '1':
				$arr_profissoes = [
				''=>'Profissão',
				'Arquitetura'=>'Arquitetura',
				'Estágio de arquitetura'=>'Estágio de arquitetura',
				'Design de Interiores'=>'Design de Interiores',
				'Estágio de Design de Interiores'=>'Estágio de Design de Interiores',
				'Engenheiro Civil'=>'Engenheiro Civil', 
				'Estágio de Engenharia Civil'=>'Estágio de Engenharia Civil', 
				'Técnica em Edificações'=>'Técnica em Edificações',
				'Estágio de Técnica em Edificações'=>'Estágio de Técnica em Edificações'
				];
			break;
			case '2':
				$arr_profissoes = [
				''=>'Profissão',
				'Arquitetura'=>'Arquitetura',
				'Estágio de arquitetura'=>'Estágio de arquitetura',
				'Design de Interiores'=>'Design de Interiores',
				'Estágio de Design de Interiores'=>'Estágio de Design de Interiores',
				'Engenheiro Civil'=>'Engenheiro Civil', 
				'Estágio de Engenharia Civil'=>'Estágio de Engenharia Civil', 
				'Técnica em Edificações'=>'Técnica em Edificações',
				'Estágio de Técnica em Edificações'=>'Estágio de Técnica em Edificações'
				];
			break;
			case '3':
				$arr_profissoes = [
				''=>'Profissão',
				'Arquitetura'=>'Arquitetura',
				'Estágio de arquitetura'=>'Estágio de arquitetura',
				'Design de Interiores'=>'Design de Interiores',
				'Estágio de Design de Interiores'=>'Estágio de Design de Interiores',
				'Engenheiro Civil'=>'Engenheiro Civil', 
				'Estágio de Engenharia Civil'=>'Estágio de Engenharia Civil', 
				'Técnica em Edificações'=>'Técnica em Edificações',
				'Estágio de Técnica em Edificações'=>'Estágio de Técnica em Edificações'
				];
			break;
			case '4':
				$arr_profissoes = [
				''=>'Profissão',
				'Estudante de Arquitetura' => 'Estudante de Arquitetura',
				'Estudante de Engenharia Civil'=>'Estudante de Engenharia Civil',
				'Estudante de Design de Interiores'=>'Estudante de Design de Interiores'
				];
			break;
			case '5':
				$arr_profissoes = [
				''=>'Profissão',
				'Escola Técnica em Edificações'=>'Técnico em Edificações',
				'Escola Técnica Design de Interiores'=>'Técnico em Design de Interiores',
				];
			break;
			case '6':
				$arr_profissoes = [''=>'Profissão','Estudante de Construção Civil'=>'Estudante de Construção Civil'];
			break;
			case '7':
				$arr_profissoes = [
				''=>'Profissão',
				'Propietário'=>'Propietário',
				'Gerente Comercial'=>'Gerente Comercial',
				'Gerente Técnico'=>'Gerente Técnico',
				'Vendedor'=>'Vendedor',
				'Especificador'=>'Especificador',
				'Instalador'=>'Instalador'
				];
			break;
		}
		
		$u = Sentry::getUser();

		$user = User::find($u->id);

		$estados = Estado::orderBy('uf','ASC')->lists('uf', 'id');

		$cidades = Cidade::whereEstado_id($user->estado_id)->orderBy('name','ASC')->lists('name','id');


		$this->layout->content = View::make('cursos.create', compact('curso','dates','data','user', 'estados','cidades'));

	}


	static function Extenso($c, $data = false) {

        $meses = array(1 => "janeiro", 2 => "fevereiro", 3 => "março", 4 => "abril", 5 => "maio", 6 => "junho", 7 => "julho", 8 => "agosto", 9 => "setembro", 10 => "outubro", 11 => "novembro", 12 => "dezembro");

        if (!$data) {

            $data = date("Y-m-d");

        }

        $d = date_parse($data);

        $m = $meses[$d['month']];

        return ($c ? $c . ", " : "") . "$d[day] de $m de $d[year]";

    }

  



	/**

	 * Store a newly created curso in storage.

	 *

	 * @return Response

	 */

	public function store()

	{

		$data = Input::all();

		//$validator = Validator::make($data, ['estado_id.0'=>'required']);

		//$validator->each('cidade_id', ['required']);
		//$validator->each('estado_id', ['required']);

		/*if($validator->fails()){
			return Redirect::back()->withErrors($validator)->withInput();
		}*/

		$curso = Curso::find($data['curso_id']);

		$diff = array_diff($data['estado_id'], $curso->instructor->cidades->lists('estado_id'));
		
		if(count($diff) > '0'){
			return Redirect::route('cursos/create')->withError('Este curso não pode ser ministrado em Estado diferente de onde o instrutor se encontra');
		}


		for ($i=0; $i < sizeof($data['date_ini']); $i++) { 
			
			$agenda = new Agenda;

			$agenda->curso_id = $data['curso_id'];

			$agenda->user_id = $data['user_id'];
			
			$agenda->atividade_id = $data['atividade_id'];

			$agenda->cidade_id = $data['cidade_id'][$i];

			$agenda->date_ini = Helper::ConverterUS($data['date_ini'][$i]);

			$agenda->periodo_id = $data['periodo_id'][$i];

			$agenda->email = $data['email'];

			$agenda->cep = $data['cep'];

			$agenda->endereco = $data['endereco'];

			$agenda->tel = $data['tel'];

			$agenda->cel = $data['cel'];

			$agenda->save();

			//pegando e enviando email ao instrutor
			Mail::send('emails.instructors.newagenda', ['user'=>$agenda->curso->instructor->user->name,'responsavel'=>$agenda->user->name, 'agenda'=>$agenda->curso->consumer->name, 'data_agenda'=> Helper::ConverterBR($agenda->date_ini, true), 'hora_agenda'=>$agenda->periodo->hour_ini, 'periodo'=>$agenda->periodo->name, 'endereco'=>$agenda->endereco, 'cep'=>$agenda->cep, 'tel'=>$agenda->tel,'cel'=>$agenda->cel], function($message) use($agenda){
				$message->to($agenda->curso->instructor->user->email, $agenda->curso->instructor->user->name)->subject('Pedido de solicitação para '.$agenda->curso->consumer->name);
			});

		}

		Mail::send('emails.users.newagenda', ['user'=>$agenda->user->name,'responsavel'=>$agenda->user->name, 'agenda'=>$agenda->curso->consumer->name, 'data_agenda'=> Helper::ConverterBR($agenda->date_ini, true), 'hora_agenda'=>$agenda->periodo->hour_ini, 'periodo'=>$agenda->periodo->name, 'endereco'=>$agenda->endereco, 'cep'=>$agenda->cep, 'tel'=>$agenda->tel,'cel'=>$agenda->cel], function($message) use($agenda){
				$message->to($agenda->user->email, $agenda->user->name)->subject('Pedido de solicitação para '.$agenda->curso->consumer->name);
		});


		Session::forget('dataCurso');



		return Redirect::route('user/account/agenda').with('success','Curso solicitado com sucesso! Agora aguarde que em breve o Instrutor aprovando, você receberá as instruções de divulgação de seu agendamento');	

		

	}



	

	/**

	 * Display the specified curso.

	 *

	 * @param  int  $id

	 * @return Response

	 */

	public function generate(){

		$data = Input::except('caracter');



		$validate = Validator::make($data, Curso::$rules);



		if($validate->fails()){

			return Redirect::route('admin/cursos',['caracter'=>Input::get('caracter')])->withErrors($validate)->withInput();

		}


		$curso = new Curso;

		$curso->tipo_id = $data['tipo_id'];

		$curso->instructor_id = $data['instructor_id'];


		$curso->consumer_id = $data['consumer_id'];

		$curso->min_quorum = $data['min_quorum'];

		$curso->max_quorum = $data['max_quorum'];
			
			//'disponibilidade'=>'a:5:{i:0;s:1:"1";i:1;s:1:"2";i:2;s:1:"3";i:3;s:1:"4";i:4;s:1:"5";}',
		$curso->status = '1';

		$curso->save();

		$curso->atividades()->attach($data['atividade_id']);
		

		return Redirect::route('admin/cursos',['caracter'=>Input::get('caracter')])->with('success','Curso criado com sucesso!');

	}





	/**

	 * Show the form for editing the specified curso.

	 *

	 * @param  int  $id

	 * @return Response

	 */

	public function edit($id)

	{

		$curso = Curso::find($id);

		

		$atividades = Atividade::orderBy('name')->lists('name','id');

		

		$tipos = Tipo::orderBy('name')->lists('name','id');

	
		$instrutores = Instructor::orderBy('name')->lists('name', 'id');


		$consumers = Consumer::orderBy('name')->lists('name','id');

		


		return View::make('cursos.edit', compact('curso','consumers','atividades','instrutores','tipos'));

	}



	/**

	 * Update the specified curso in storage.

	 *

	 * @param  int  $id

	 * @return Response

	 */

	public function update()

	{



		$curso = Curso::findOrFail(Input::get('id'));



		$validator = Validator::make($data = Input::all(), Curso::$rules);



		if ($validator->fails())

		{

			return Redirect::back()->withErrors($validator)->withInput();

		}

		$curso->update($data);

		$curso->atividades()->detach($curso->atividades->lists('id'));

		$curso->atividades()->attach(Input::get('atividade_id'));

		$curso->save();

		

		return Redirect::back()->with('success','Curso salvo com sucesso!');

	}



	/**

	 * Remove the specified curso from storage.

	 *

	 * @param  int  $id

	 * @return Response

	 */

	public function destroy($caracter, $id)

	{

		$curso = Curso::find($id);

		//$curso->employers()->delete();

		$curso->agendas()->delete();

		$curso->destroy($id);

		return Redirect::route('admin/cursos',['caracter'=>$caracter])->with('success','Curso Apagado com sucesso');

	}

	public function show($id){

		$curso = Curso::find($id);
		///$mdate = $this->Extenso(null,$curso);
		return View::make('cursos.show',compact('curso'));

	}

	public function agenda(){
		$a = Agenda::where('user_id','=', Sentry::getUser()->id);
		
		$agendas = $a->confirmados()->orderBy('id','DESC')
		->get();

		$agendas_conc = $a->cancelados()->orderBy('id','DESC')
		->get();
		
		$this->layout->content = View::make('users.account.agenda',compact('agendas','agendas_conc'));

	}

	public function indexAtividades(){
		$atividades = Atividade::all();
		return View::make('backend.atividades.index', compact('atividades'));
	}
	public function createAtividade(){
		$data = Input::all();
		$validate = Validator::make($data, Atividade::$rules);
		if($validate->fails()){
			return Redirect::route('atividades/index')->withErrors($validate);
		}
		$atividade = Atividade::create($data);
		return Redirect::route('atividades/index')->with('success','Atividade criada com sucesso!');
	}
	public function editAtividade($id){
	
		$atividade = Atividade::find($id);

		return View::make('backend.atividades.edit', compact('atividade'));
	}
	public function updateAtividade(){
		$data = Input::all();
		$atividade = Atividade::find($data['id']);
		if($atividade->update($data)){
			return Redirect::route('atividade/edit',['id'=>$atividade->id])->with('success','Atividade editada com sucesso!');
		}
		return Redirect::route('atividade/edit',['id'=>$atividade->id])->with('success','Erro ao apagar!');
	}
	public function deleteAtividade($id){

		$atividade = Atividade::find($id);

		$atividade->delete();

		return Redirect::route('atividades/index')->with('success','Atividade apagada com sucesso!');
	}

	public function indexTipos(){
		$tipos = Tipo::all();
		$caracters = Caracter::orderBy('name')->lists('name','id');
		return View::make('backend.tipos.index', compact('tipos','caracters'));
	}
	public function createTipo(){
		$data = Input::all();
		$validate = Validator::make($data, Tipo::$rules);
		if($validate->fails()){
			return Redirect::route('tipos/index')->withErrors($validate);
		}
		$tipo = Tipo::create($data);
		return Redirect::route('tipos/index')->with('success','Tipo criado com sucesso!');
	}
	public function editTipo($id){
	
		$tipo = Tipo::find($id);
		$caracters = Caracter::orderBy('name')->lists('name','id');

		return View::make('backend.tipos.edit', compact('tipo','caracters'));
	}
	public function updateTipo(){
		$data = Input::all();
		$tipo = Tipo::find($data['id']);
		if($tipo->update($data)){
			return Redirect::route('tipo/edit',['id'=>$tipo->id])->with('success','Tipo editado com sucesso!');
		}
		return Redirect::route('tipo/edit',['id'=>$tipo->id])->with('success','Erro ao apagar!');
	}
	public function deleteTipo($id){

		$tipo = Tipo::find($id);

		$tipo->delete();

		return Redirect::route('tipos/index')->with('success','Tipo apagado com sucesso!');
	}

	public function indexCidades(){
		$cidades = Cidade::all();
		$estados = Estado::all();
		return View::make('backend.cidades.index', compact('cidades','estados'));
	}
	public function createCidade(){
		$data = Input::all();
		$validate = Validator::make($data, Cidade::$rules);
		if($validate->fails()){
			return Redirect::route('cidades/index')->withErrors($validate);
		}
		
		$estado = Estado::find($data['estado_id']);
		$cidade = new Cidade;
		$cidade->name = $data['name'];
		$cidade->estado()->associate($estado);
		$cidade->save();
		return Redirect::route('cidades/index')->with('success','Cidade criada com sucesso!');
	}
	public function editCidade($id){
		$estados = Estado::all();
		$cidade = Cidade::find($id);

		return View::make('backend.cidades.edit', compact('cidade','estados'));
	}
	public function updateCidade(){
		$data = Input::all();
		$estado = Estado::find($data['estado_id']);
		$cidade = Cidade::find($data['id']);
		$cidade->name = $data['name'];
		$cidade->estado()->associate($estado);
		
		if($cidade->save()){
			return Redirect::route('cidade/edit',['id'=>$cidade->id])->with('success','Cidade editada com sucesso!');
		}
		return Redirect::route('cidade/edit',['id'=>$cidade->id])->with('success','Erro ao apagar!');
	}
	public function deleteCidade($id){

		$cidade = Cidade::find($id);

		$cidade->delete();

		return Redirect::route('cidades/index')->with('success','cidade apagada com sucesso!');
	}

	public function avaliableCurso($id){
		
		$curso = Curso::find($id);

		$horarios = Periodo::where('caracter_id', '=',$curso->tipo->caracter->id)->orderBy('hour_ini','ASC')->get();

		$un_horario = $curso->disponibilidade_horarios != null ? unserialize(base64_decode($curso->disponibilidade_horarios)) : null;
		

		return View::make('backend.cursos.disponibilidade', compact('curso', 'horarios', 'un_horario'));
	}
	public function avaliableCursoEdit(){
		$data = Input::all();

		$curso = Curso::find($data['id']);

		$curso->disponibilidade = isset($data['disponibilidade']) ? base64_encode(serialize($data['disponibilidade'])) : null;
		
		$curso->disponibilidade_horarios = isset($data['disponibilidade_horarios']) ? base64_encode(serialize($data['disponibilidade_horarios'])) : null;
		//$curso->periodo_fim = Helper::ConverterUS($data['periodo_fim']); 

		if($curso->save()){
			return Redirect::route('instructor/disponibilidade',['id'=>$curso->id])->with('success','Disponibilidade alterada com sucesso!');
		}

		return Redirect::route('instructor/disponibilidade',['id'=>$curso->id])->with('error','Erro ao alterar disponibilidade.');
	}

	public function InstructorAvaliableCurso($id){
		
		$curso = Curso::find($id);

		$horarios = Periodo::where('caracter_id', '=',$curso->tipo->caracter->id)->orderBy('hour_ini','ASC')->get();

		$un_horario = $curso->disponibilidade_horarios != null ? unserialize(base64_decode($curso->disponibilidade_horarios)) : null;
		

		return View::make('backend.instructors.curso-disponibilidade', compact('curso', 'horarios', 'un_horario'));
	}
	public function InstructorAvaliableCursoEdit(){
		$data = Input::all();

		$curso = Curso::find($data['id']);

		$curso->disponibilidade = isset($data['disponibilidade']) ? base64_encode(serialize($data['disponibilidade'])) : null;
		
		$curso->disponibilidade_horarios = isset($data['disponibilidade_horarios']) ? base64_encode(serialize($data['disponibilidade_horarios'])) : null;
		//$curso->periodo_fim = Helper::ConverterUS($data['periodo_fim']); 

		if($curso->save()){
			return Redirect::route('instructor/disponibilidade/ins',['id'=>$curso->id])->with('success','Disponibilidade alterada com sucesso!');
		}

		return Redirect::route('instructor/disponibilidade/ins',['id'=>$curso->id])->with('error','Erro ao alterar disponibilidade.');
	}

	public function alterDispAll(){
		$data = Input::all();

	
		foreach ($data['cursos'] as $c) {
			$curso = Curso::find($c);
			$curso->disponibilidade = isset($data['disponibilidade']) ? base64_encode(serialize($data['disponibilidade'])) : null;
			$curso->disponibilidade_horarios = isset($data['disponibilidade_horarios']) ? base64_encode(serialize($data['disponibilidade_horarios'])) : null;
			$curso->save();
		}

		//echo $curso->instructor->id;


		return Redirect::route('instructor/cursos', ['id'=>$curso->instructor->user->id,'caracter_id'=>$curso->tipo->caracter->id])->withSuccess('Disponibilidade alterada com sucesso para os cursos');
	}

	public function actions(){
		$data = Input::all();

		$str = serialize($data['cursos']);
    	$strenc = urlencode($str);
		
		return Redirect::route('alter/disp', ['c'=>$strenc]);
	}
	public function InstructorActions(){
		$data = Input::all();

		if(Input::get('cursos') == ''){
			return Redirect::back()->withError('Selecione ao menos um curso');
		}

		$str = serialize($data['cursos']);
    	$strenc = urlencode($str);
		
		return Redirect::route('instructor/alter/disp', ['c'=>$strenc]);
	}
	

	public function alterDisp($c){
		
		$arr = unserialize(urldecode($c));
		$cursos = Curso::whereIn('id',$arr)->get();

		$curso  = Curso::find($arr[0]);

		$horarios = Periodo::where('caracter_id', '=',$curso->tipo->caracter->id)->orderBy('hour_ini','ASC')->get();

		$un_horario = $curso->disponibilidade_horarios != null ? unserialize(base64_decode($curso->disponibilidade_horarios)) : null;
		
		return View::make('backend.cursos.disponibilidadeall', compact('cursos', 'horarios', 'un_horario'));
	}
	public function InstructorAlterDisp($c){
		
		$arr = unserialize(urldecode($c));

		$cursos = Curso::whereIn('id',$arr)->get();

		$curso  = Curso::find($arr[0]);

		$horarios = Periodo::where('caracter_id', '=',$curso->tipo->caracter->id)->orderBy('hour_ini','ASC')->get();

		$un_horario = $curso->disponibilidade_horarios != null ? unserialize(base64_decode($curso->disponibilidade_horarios)) : null;
		
		return View::make('backend.instructors.disponibilidadeall', compact('cursos', 'horarios', 'un_horario'));
	}
	public function InstructorAlterDispAll(){
		
		$data = Input::all();
		print_r($data);
		if(!isset($data['cursos']) or !isset($data['dates'])){
			return Redirect::back()->withError('Selecione ao menos 1 curso e uma data, para poder editar sua disponibilidade');
		}

		// Definindo horas, quando for todas
		$hours = isset($data['disponibilidade_horarios']) ? base64_encode(serialize($data['disponibilidade_horarios'])) : null;
		

		foreach ($data['cursos'] as $c) {

			$curso = Curso::find($c);

			if($curso->availables->count() > 0){
				
				// Verifica a diferença das disponibilidades ja existentes
				$diff = array_diff($data['dates'],$curso->availables->lists('date_at'));
				
				foreach ($diff as $d) {
			
					$a = new Available;
					$a->date_at = $d;
					$a->curso_id = $curso->id;
					$a->hours = $hours;
					$a->save();
					$a->cidades()->attach($data['city_ids'][$curso->id]);
					$a->save();
				}
			
			}else{
				
				// Se não houver disponibilidade para o curso
				foreach ($data['dates'] as $d) {
				
					$a = new Available;
					$a->date_at = $d;
					$a->curso_id = $curso->id;
					$a->hours = $hours;
					$a->save();
					$a->cidades()->attach($data['city_ids'][$curso->id]);
					$a->save();
				}
				
			}

		}
		
		if(Session::has('dates')){
			Session::forget('dates');
		}

		return Redirect::route('admin.instructors.calendar')->withSuccess('Disponibilidade alterada com sucesso para os cursos');
	}

	public function InstructorAlterDispHour()
	{
		$data = Input::all();
		
		for ($i=0; $i < sizeof($data['availables']); $i++) { 
			$available = Available::find($data['availables'][$i]);
			$available->hours = isset($data['disponibilidade_horarios_exc']) ? base64_encode(serialize($data['disponibilidade_horarios_exc'])) : null;
			$available->save();
		}

		return Redirect::route('admin/cursos/instrutor', $data['caracter_id'])->withSuccess('Horários alterados com sucesso.');

	}

	/**
	* Calendário
	*
	***/
	public function calendar()
	{
		$u = Sentry::getUser();
		$user = User::find($u->id);

		/*if(!Session::has('dates')){
			return Redirect::back()->withError('Selecione datas');
		}*/
	
		$instructor = Instructor::where('user_id','=',$user->id)->first();
		
		$c = Curso::where('instructor_id', '=', $instructor->id);
		$c_p = Curso::where('instructor_id', '=', $instructor->id);

	

		$cursos = $c->tchar(1)
		->onlyActive()
		->has('availables')
		->orderBy('id','DESC')
		->get();

		$palestras = $c_p->tchar(2)
		->onlyActive()
		->has('availables')
		->orderBy('id','DESC')
		->get();

		//$arr_cursos = implode(',', $cursos->availables->lists('date'));

		$availables_cursos = Available::whereIn('curso_id', $cursos->lists('id'));
		$availables_palestras = Available::whereIn('curso_id', $palestras->lists('id'));

		if(Session::has('dates')){
			Session::forget('dates');
		}

		$arr_cursos = implode(',', $availables_cursos->lists('date_at'));
		$arr_palestras = implode(',', $availables_palestras->lists('date_at'));

		return View::make('backend.instructors.calendar', compact( 'cursos','palestras','arr_cursos', 'arr_palestras'));
	}
	public function calendarSaveDays()
	{
		$data = Input::all();
		
		if($data['savedDays'] == ''){
			return Redirect::route('admin.instructors.calendar')->withError('Selecione ao menos 1 dia para editar');
		}

		$dates = explode(',', $data['savedDays']);

		Session::set('dates',$dates);

		return Redirect::route('admin/cursos/instrutor', ['caracter_id'=>$data['caracter_id']]);

	}
	public function searchInByDate()
	{
		$data = Input::all();
		$u = Sentry::getUser();
		$user = User::find($u->id);

		$instructor = Instructor::where('user_id','=',$user->id)->first();
		
		$c = Curso::where('instructor_id', '=', $instructor->id);

		$cs = $c->tchar($data['t'])
		->onlyActive()
		->where('disponibilidade', '!=', '')
		->orderBy('id','DESC')
		->get();

		$arr = array();
		foreach ($cs as $cu) {
			
			if(strpos($cu->unDisp(),$data['date'])){
				array_push($arr,$cu->id);
			}
		}

		//print_r($arr);

		$cursos = Curso::whereIn('id',$arr)->get()->toArray();

		return Response::json(['cursos'=>$cursos]);
	}

	public function delDisp($id){
		$available = Available::find($id);
		$available->delete();
		return Redirect::back()->withSuccess('Retirado com sucesso!');
	}

	public function delDispAll($date){
		$available = Available::whereDate_at($date);
		$available->delete();
		return Redirect::back()->withSuccess('Retirado com sucesso!');
	}

	public function cancelDisp()
	{
		if(Session::has('dates')){
			Session::forget('dates');
		}

		return Redirect::route('admin.instructors.calendar')->withSuccess('Edição cancelada');
	}
}

