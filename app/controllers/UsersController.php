<?php



class UsersController extends BaseController {



	/*

	|--------------------------------------------------------------------------

	| Default Home Controller

	|--------------------------------------------------------------------------

	|

	| You may wish to use controllers instead of, or in addition to, Closure

	| based routes. That's great! Here is an example controller method to

	| get you started. To route to this controller, just add the route:

	|

	|	Route::get('/', 'HomeController@showWelcome');

	|

	*/

	protected $layout = 'layouts.base';



	public function index()

	{

		if(Sentry::check()){

			

				return Redirect::route('cursos/index');

			

			

		}

		$this->layout->content = View::make('users.login');

	}

	public function login()

	{

		$credentials = ['email'=>Input::get('email'), 'password'=>Input::get('password')];



		$validate = Validator::make($credentials, ['email'=>'required|email','password'=>'required']);



		if($validate->passes()){



			try{

				// Try to log the user in

				Sentry::authenticate(Input::only('email', 'password'), Input::get('remember-me', 0));

				if(Session::get('dataCurso')){

					return Redirect::route('cursos/create');

				}

				return Redirect::route('cursos/index');

			}

			catch (Cartalyst\Sentry\Users\UserNotFoundException $e)

			{

				return Redirect::route('user/login')->with('login_error','Login ou senha incorretos!')->withInput(Input::except('password'));

			}

			catch (Cartalyst\Sentry\Users\UserNotActivatedException $e)

			{

				return Redirect::route('user/login')->with('login_error','Não ativado')->withInput(Input::except('password'));

			}
			catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e)
			{
			   return Redirect::route('user/login')->with('login_error','Usuário suspenso por mais de 5 tentativas de login')->withInput(Input::except('password'));
			}
			catch (Cartalyst\Sentry\Throttling\UserBannedException $e)
			{
			  return Redirect::route('user/login')->with('login_error','Usuário banido por tentativa de invasão')->withInput(Input::except('password'));
			}
	

			return Redirect::route('user/login')->with('login_error','Login ou senha incorretos')->withInput(Input::except('password'));

		
		}else{

			return Redirect::route('user/login')->withInput()->withErrors($validate);

		}

	}



	public function getAdminLogin(){

		if((Sentry::check()) and (Sentry::getUser()->hasAccess('admin')))

		{

			return Redirect::route('admin/cursos',['caracter'=>'1']);

		}

		elseif ((Sentry::check()) and (!Sentry::getUser()->hasAccess('admin')))

		{

			Sentry::logout();

			return View::make('backend.login')->with('error','Você não possui permissão de acesso!');

		}else{

			return View::make('backend.login');

		}	

		

	}

	public function getInstructorLogin(){

		if((Sentry::check()) and (Sentry::getUser()->hasAccess('instructor')))

		{

			return Redirect::route('admin/cursos/instrutor', ['caracter_id'=>'cursos']);

		}

		elseif ((Sentry::check()) and (!Sentry::getUser()->hasAccess('instructor')))

		{

			Sentry::logout();

			return View::make('backend.login')->with('error','Você não possui permissão de acesso!');

		}else{

			return View::make('backend.login');

		}	

		

	}

	public function postAdminLogin()

	{

		$credentials = ['email'=>Input::get('email'), 'password'=>Input::get('password')];



		$validate = Validator::make($credentials, ['email'=>'required|email','password'=>'required']);



		if($validate->passes()){



			try{

				// Try to log the user in

				Sentry::authenticate(Input::only('email', 'password'), Input::get('remember-me', 0));

				

				if(Input::get('instructor')){

					return Redirect::route('admin.instructors.calendar');

				}

				return Redirect::route('admin/cursos',['caracter'=>'cursos']);

			}

			catch (Cartalyst\Sentry\Users\UserNotFoundException $e)

			{

				if(Input::get('instructor')){return Redirect::route('instructor/login')->with('error','Login ou senha incorretos!')->withInput();}

				return Redirect::route('backend/login')->with('error','Login ou senha incorretos!')->withInput();

			}

			catch (Cartalyst\Sentry\Users\UserNotActivatedException $e)

			{

				if(Input::get('instructor')){return Redirect::route('instructor/login')->with('error','Não ativado')->withInput();}

				return Redirect::route('backend/login')->with('error','Não ativado')->withInput();

			}

			if(Input::get('instructor')){return Redirect::route('instructor/login')->with('error','Login ou senha incorretos')->withInput();}

			return Redirect::route('backend/login')->with('error','Login ou senha incorretos')->withInput();

			

		}else{

			if(Input::get('instructor')){

				return Redirect::route('instructor/login')->withInput()->withErrors($validate);

			}

			return Redirect::route('backend/login')->withInput()->withErrors($validate);

		}

	}

	

	public function register()

	{

		if(Sentry::check()){

			return Redirect::route('cursos/index');

		}

		$estados = Estado::orderBy('name','ASC')->lists('uf','id');

		$this->layout->content = View::make('users.register', compact('estados'));

	}

	public function post_register()

	{

		$userdata = Input::all();



		$rules = array(

			'name' => 'required|min:2',

			'cpf' => 'required|unique:users',

			'profissao' => 'required',

			'empresa' => 'required',

			'cargo' => 'required',

			'endereco' => 'required',

			'cep' => 'required',

			'email' => 'required|email|unique:users',

			'tel' => 'required',

			'cel' => 'required'

		);

		$messages = array(

			'name.required'=>'Favor preencher campo nome',

			'cpf.required'=>'Prencher CPF'

		);

		$validate = Validator::make($userdata, $rules, $messages);



		if($validate->fails()){
			return Redirect::back()

			->withErrors($validate)

			->withInput();
		}

			

			try

			{

			    // Let's register a user.

			    $user = Sentry::register(array(

			      'name' => Input::get('name'),

			      	'password' => 'testing',

					'email' => Input::get('email'),

					'cpf' => Input::get('cpf'),

					'profissao' => Input::get('profissao'),

					'empresa' => Input::get('empresa'),

					'cargo' => Input::get('cargo'),

					'endereco' => Input::get('endereco'),

					'cep' => Input::get('cep'),

					'tel' => Input::get('tel'),

					'cel' => Input::get('cel'),

			    ));

				//Adicionando ao grupo de usuarios
			    $user_group = Sentry::findGroupById(3);
			    $user->addGroup($user_group);
			    
			    // Let's get the activation code

			    $token = $user->getActivationCode();



			Mail::send('emails.users.register', array('token'=>$token, 'id'=>$user->id), function($message){

				$message->to(Input::get('email'),Input::get('name'))->subject('Recebemos o seu pedido de cadastro');

			});

			    // Send activation code to the user so he can activate the account

			return Redirect::back()->with('success','Cadastrado com sucesso! Confira agora seu e-mail e finalize seu cadastro.');

			}

			catch (Cartalyst\Sentry\Users\LoginRequiredException $e)

			{

			    return Redirect::back()->with('error','Email requerido');

			}

			catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)

			{

			    return Redirect::back()->with('error','Senha requerida');

			}

			catch (Cartalyst\Sentry\Users\UserExistsException $e)

			{

			   return Redirect::back()->with('error','Ja existente');

			}
			catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
			{

				 return Redirect::back()->with('error','Grupo Ja existente');
			}	


			

	

	}



	public function updatePass(){

		$code = $_GET['code'];

		$id = $_GET['id'];



		$user = Sentry::findUserById($id);



		if($user->activation_code == $code){

			 $this->layout->content = View::make('users.confirm', compact('code','id'));

		}else{

			return Redirect::to('/')->with('success','Usuário já foi verificado, favor efetuar seu login.');

		}



		

		

	}



	public function postUpdatePass(){

		$data = Input::all();

		$validate = Validator::make($data, ['password'=>'required|min:4','password_confirmation'=>'required|same:password'], ['password.required'=>'Favor preencher senha','password.min'=>'Senha deve conter mais de 4 caracteres','password_confirmation.required'=>'Favor preencher confirmação de senha','password_confirmation.same'=>'Sua confirmação deve ser igual ao campo senha']);



		if($validate->fails()){

			return Redirect::back()->withInput()->withErrors($validate);

		}

		try

		{

			$user = Sentry::getUserProvider()->findById(Input::get('user_id'));

			



			if ($user->attemptActivation(Input::get('code')))

		    {

		    	$user->password = Input::get('password');



				if($user->save()){

					return Redirect::route('user/login')->with('success','Senha salva com sucesso! Favor faça seu login.');

				}



		    	 

		    }

		   

		        return Redirect::to('/')->with('success','Erro ao autenticar seu usuario');

		   

			

		}

		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)

		{

		   return Redirect::back()->with('success','Usuário não encontrado');

		}

		catch (Cartalyst\Sentry\Users\UserAlreadyActivatedException $e)

		{

		   return Redirect::back()->with('success','Usuário ja foi ativado');

		}

	}

	public function admin()

	{

		return View::make('admin.index');

	}





	public function show(){

		$user = Sentry::getUser();

		$this->layout->content = View::make('users.account.index', compact('user'));

	}

	public function update(){

		$user = User::find(Input::get('user_id'));



		$data = Input::except('informe', 'user_id', 'is_admin');

		try{
			$user->update($data);

			if($user->has('instrutor')){
				$instrutor = Instructor::whereUser_id($user->id)->first();
				$instrutor->name = Input::get('name');
				$instrutor->save();
			}
			if(Input::has('is_admin')){
				if(Input::get('password') != ''){
					$user->password = Hash::make(Input::get('password'));
					$user->save();
				}
				$u = Sentry::getUser();
				if($u->hasAccess('instructor')){
					return Redirect::route('instructor/edit/instructor', ['id'=>$user->id, 'default'=>'1'])->with('success','Editado com sucesso!');
				}
				return Redirect::route('instructor/edit', ['id'=>$user->id])->with('success','Editado com sucesso!');
			}
			return Redirect::route('user/edit')->with('success','Editado com sucesso!');

		}
		catch(Illuminate\Database\QueryException $e){
			return Redirect::back()->with('error','Erro ao editar usuário já existe em nossa base');
		}

		return Redirect::back()->with('error','Erro ao editar');

	}

	public function listagem(){
		$group = Sentry::findGroupByName('User');

		$users = Sentry::findAllUsersInGroup($group);

		return View::make('users.index', compact('users'));
	}

	public function instructorsIndex(){
		$group = Sentry::findGroupByName('Instructor');

		$users = Sentry::findAllUsersInGroup($group);

	/*	$users = User::join('users_groups', 'users_groups.user_id','=','users.id')
		->where('users_groups.group_id','=','2')
		->get();*/

		$tipos = Tipo::all();

		return View::make('users.instructors.index', compact('users','tipos'));
	}

	public function createInstructor(){
		$data  =Input::all();

		$validate = Validator::make($data, User::$rules);

		if($validate->fails()){
			return Redirect::route('instructors/index')->withErrors($validate)->withInput();
		}
		try{
			//incluindo permissoes
			$permissions = ['permissions'=>array(
				'admin'=>-1,
				'user'=>1,
				'instructor'=>1
			)];
			$nested = ['activated'   => true];
			$data = array_merge($data, $permissions, $nested);
			
			$user = Sentry::createUser($data);
			$group = Sentry::findGroupByName('Instructor');
			$user->addGroup($group);

			//criando intrutor
			$instrutor = Instructor::create(['user_id'=>$user->id,'name'=>$user->name]);

			return Redirect::route('instructors/index')->with('success','Usuário cadastrado com sucesso!');
		}
		catch(Cartalyst\Sentry\Users\UserExistsException $e){
			return Redirect::route('instructors/index')->with('error','Usuário já existente!')->withInput();
		}
		catch(Cartalyst\Sentry\Groups\GroupNotFoundException $e){
			return Redirect::route('instructors/index')->with('error','Grupo inexistente!')->withInput();
		}
		
	}
	public function editInstructor($id){
		$user = User::find($id);

		$states = Estado::orderBy('name')->lists('uf','id');

		$menu = View::make('layouts.backendmenu');

		$ed = unserialize( base64_decode( $user->instrutor->estados_disp ) );

		$estados_disp = Estado::whereIn('id',$ed)->get();

		if(Input::has('default')){
			$menu = View::make('layouts.backendmenuinstructors');
		}

		return View::make('users.instructors.edit', compact('user', 'menu','states','estados_disp'));
	}
	public function deleteInstrutor($id){
		try{
			$user = User::find($id);
			
			$user->agendas()->delete();
			$user->cursos()->delete();
    		$user->instrutor()->delete();
    		$user->groups()->detach();
			$user->delete();

			return Redirect::route('instructors/index')->with('success','Apagado com sucesso!');
		}
		catch(Cartalyst\Sentry\Users\UserNotFoundException $e){
			return Redirect::route('instructors/index')->with('error','Usuário não encontrado.');
		}
	
	}


	public function searchByState(){
		if(Request::ajax()){

			$cities = Cidade::whereEstado_id(Input::get('state'))->get();


			$output = '<option value="">Selecione sua cidade</option>';

			foreach ($cities as $c) {
				$output .= '<option value="'.$c->id.'">'.$c->name.'</option>';
			}

			return Response::json(['response' => $output]);
		}
	}


	public function getLogout()

	{

		// Log the user out

		Sentry::logout();



		// Redirect to the users page

		return Redirect::to('/')->with('success', 'Você foi deslogado com sucesso, volte sempre!');

	}



}

