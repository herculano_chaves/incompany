<?php

class AdminlogsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /adminlogs
	 *
	 * @return Response
	 */
	protected $layout = 'layouts.backend';

	public function index()
	{
		//Logs

		$logs = Adminlog::paginate(25);

		$this->layout->content = View::make('logs.index', compact('logs'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /adminlogs/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /adminlogs
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /adminlogs/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /adminlogs/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /adminlogs/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /adminlogs/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}