<?php

class SiteController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function index()
	{
		return View::make('site.home');
	}
	public function contact(){
		return View::make('site.contato');
	}
	public function postContact()
	{
		$data = Input::all();

		$rules = [
			'name'=>'required',
			'email'=>'required|email',
			'assunto'=>'required',
			'tel'=>'required',
			'mensagem'=>'required'
		];
		$validator = Validator::make($data, $rules);

		if($validator->passes()){
			
			Mail::send('emails.site.contato', $data, function($message)
			{
			  $message->to('sak@knauf.com.br', 'Contato Knauf Incompany')
			          ->subject('Contato Knauf Incompany');
			         
			});
			return Redirect::route('site/contact')->with('success','Mensagem enviada com sucesso! Aguarde, pois retornaremos em breve.');
		}else{
			return Redirect::back('contato')
				->withInput()
				->withErrors($validator);
		}
	}
}
