<?php

class InstructorsController extends BaseController {

	public function events()
	{
		$instructor = Instructor::where('user_id','=',$this->getUser()->id)->first();
		
		$cursos = Curso::where('instructor_id', '=', $instructor->id)
		->tchar(1)
		->onlyActive()
		->has('availables')
		->orderBy('id','DESC')
		->paginate(10);

		$palestras = Curso::where('instructor_id', '=', $instructor->id)
		->tchar(2)
		->onlyActive()
		->has('availables')
		->orderBy('id','DESC')
		->paginate(10);
		
		return View::make('backend.instructors.events', compact('cursos','palestras'));

	}
	
	public function localeIndex()
	{
		$instructor = Instructor::where('user_id','=',$this->getUser()->id)->first();

		$states_disp = unserialize(base64_decode($instructor->estados_disp));

		$states = Estado::whereIn('id',$states_disp)->lists('uf','id');

		$cities = array();
		if(Input::old('state_id')){
			$cities = Cidade::whereEstado_id(Input::old('state_id'))->orderBy('name','ASC')->lists('name','id');
		}
		$instructor = Instructor::where('user_id','=',$this->getUser()->id)->first();

		return View::make('backend.instructors.locale', compact('states','cities', 'instructor'));
	}

	public function localeStore($id)
	{
		$data = Input::all();

		$validator = Validator::make($data, ['state_id'=>'required','city_id'=>'required']);

		if($validator->fails()){
			return Redirect::route('instructor.locale.index')->withInput()->withErrors($validator);
		}

		$instructor = Instructor::find($id);

		$instructor->cidades()->attach($data['city_id']);

		return Redirect::route('instructor.locale.index')->withSuccess('Cidade incluída');

	}
	
	public function localeStateStore($id){
		$data = Input::all();
		
		$instructor = Instructor::find($id);
		
		$validator = Validator::make($data, ['state_id'=>'required']);

		if($validator->fails()){
			return Redirect::route('instructor/edit', $instructor->user->id)->withInput()->withErrors($validator);
		}

		$states_disp = $instructor->estados_disp != '' ? unserialize(base64_decode($instructor->estados_disp)) : array();

		if( !in_array($data['state_id'], $states_disp) ){
			array_push($states_disp, $data['state_id']);
			$instructor->estados_disp = base64_encode(serialize($states_disp));
			$instructor->save();
		}else{
			return Redirect::route('instructor/edit', $instructor->user->id)->withSuccess('Você ja adicionou...');
		}
		
		return Redirect::route('instructor/edit', $instructor->user->id)->withSuccess('Alterado com sucesso');
	}

	public function localeStateDelete($id)
	{
		$data = Input::all();
		
		$instructor = Instructor::find($id);	

		$states_disp = $instructor->estados_disp != '' ? unserialize(base64_decode($instructor->estados_disp)) : array();

		$arr = array_diff($states_disp, array(Input::get('state_id')));

		$instructor->estados_disp = base64_encode(serialize($arr));

		$instructor->save();

		return Redirect::route('instructor/edit', $instructor->user->id)->withSuccess('Apagado com sucesso');
	}

	public function localeDelete($id)
	{
		$instructor = Instructor::find($id);

		$cidade = Cidade::find(Input::get('city_id'));

		$instructor->cidades()->detach([$cidade->id]);

		return Redirect::route('instructor.locale.index')->withSuccess('Cidade apagada');

	}
}