<?php



class DatasController extends BaseController {



	protected $layout = 'layouts.backend';



	public function index(){

		$this->layout->content = View::make('datas.index');

	}

	public function employers()
	{
		$employers = Employer::has('Agenda')
		->with('agenda')
		->get(); 

		Excel::create('Knauf_InCompany_participantes_'.date('d_m_Y'), function($excel) use($employers){

			$excel->sheet('Inscritos', function($sheet) use($employers) {

       			 $sheet->loadView('excels.employers',['employers'=>$employers]);
       		});
    	})->export('xls');
	}

}