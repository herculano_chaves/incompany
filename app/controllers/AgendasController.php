<?php

class AgendasController extends \BaseController {

	protected $layout = 'layouts.base';

	/**
	 * Remove the specified resource from storage.
	 * DELETE /agendas/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function cancelar($id){
		$user = Sentry::getUser();
		
		
		
		if(!$user->hasAccess('admin')){

			
			$agenda = Agenda::where('id','=',$id)
			->where('user_id','=',$user->id)->first();
			
			if(!$agenda){
				return Redirect::route('user/account/agenda')->with('error','Você não tem esta permissão de acesso!');
			}

		}else{
			$agenda = Agenda::find($id);
		}
		//print_r($agenda);
		$agenda->status = '2';

		if($agenda->save()){
			if($user->hasAccess('admin')){
				return Redirect::back()->with('success','Agenda cancelada com sucesso <a href="'.URL::to('usuario/agenda/desfazer/'.$agenda->id).'">Desfazer</a>');
				
			}
			return Redirect::route('user/account/agenda')->with('success','Agenda cancelada com sucesso <a href="'.URL::to('usuario/agenda/desfazer/'.$agenda->id).'">Desfazer</a>');
		}

		return Redirect::route('user/account/agenda')->with('success','Erro ao cancelar agenda');
	}
	public function desfazer($id){
		$user = Sentry::getUser();
		$agenda = Agenda::where('id','=',$id)
		->where('user_id','=',$user->id)->first();
		
		if(!$agenda){
			return Redirect::route('user/account/agenda')->with('error','Você não tem esta permissão de acesso!');
		}
		
		$agenda->status = '0';
		if($agenda->save()){
			return Redirect::route('user/account/agenda')->with('success','Desfeito!');
		}
		return Redirect::route('user/account/agenda')->with('success','Erro ao desfazer ação agenda');
	}
	public function show($id){
		$curso = Curso::find($id);

		return View::make('backend.instructors.agendas', compact('curso'));
	} 
	public function showDetalhes($id){
		$user = Sentry::getUser();
		$agenda = Agenda::where('id','=',$id)
		->where('user_id','=',$user->id)->first();
		
		if(!$agenda){
			return Redirect::route('user/account/agenda')->with('error','Você não tem esta permissão de acesso!');
		}
		
		$arr_profissoes = $this->professions($agenda->atividade->id);
		
		return View::make('agendas.show', compact('agenda','arr_profissoes'));
	}

	private function professions($atividade){
		switch($atividade){
			case '1':
				$arr_profissoes = [
				''=>'Profissão',
				'Arquitetura'=>'Arquitetura',
				'Estágio de arquitetura'=>'Estágio de arquitetura',
				'Design de Interiores'=>'Design de Interiores',
				'Estágio de Design de Interiores'=>'Estágio de Design de Interiores',
				'Engenheiro Civil'=>'Engenheiro Civil', 
				'Estágio de Engenharia Civil'=>'Estágio de Engenharia Civil', 
				'Técnica em Edificações'=>'Técnica em Edificações',
				'Estágio de Técnica em Edificações'=>'Estágio de Técnica em Edificações'
				];
			break;
			case '2':
				$arr_profissoes = [
				''=>'Profissão',
				'Arquitetura'=>'Arquitetura',
				'Estágio de arquitetura'=>'Estágio de arquitetura',
				'Design de Interiores'=>'Design de Interiores',
				'Estágio de Design de Interiores'=>'Estágio de Design de Interiores',
				'Engenheiro Civil'=>'Engenheiro Civil', 
				'Estágio de Engenharia Civil'=>'Estágio de Engenharia Civil', 
				'Técnica em Edificações'=>'Técnica em Edificações',
				'Estágio de Técnica em Edificações'=>'Estágio de Técnica em Edificações'
				];
			break;
			case '3':
				$arr_profissoes = [
				''=>'Profissão',
				'Arquitetura'=>'Arquitetura',
				'Estágio de arquitetura'=>'Estágio de arquitetura',
				'Design de Interiores'=>'Design de Interiores',
				'Estágio de Design de Interiores'=>'Estágio de Design de Interiores',
				'Engenheiro Civil'=>'Engenheiro Civil', 
				'Estágio de Engenharia Civil'=>'Estágio de Engenharia Civil', 
				'Técnica em Edificações'=>'Técnica em Edificações',
				'Estágio de Técnica em Edificações'=>'Estágio de Técnica em Edificações'
				];
			break;
			case '4':
				$arr_profissoes = [
				''=>'Profissão',
				'Estudante de Arquitetura' => 'Estudante de Arquitetura',
				'Estudante de Engenharia Civil'=>'Estudante de Engenharia Civil',
				'Estudante de Design de Interiores'=>'Estudante de Design de Interiores'
				];
			break;
			case '5':
				$arr_profissoes = [
				''=>'Profissão',
				'Escola Técnica em Edificações'=>'Técnico em Edificações',
				'Escola Técnica Design de Interiores'=>'Técnico em Design de Interiores',
				];
			break;
			case '6':
				$arr_profissoes = [''=>'Profissão','Estudante de Construção Civil'=>'Estudante de Construção Civil'];
			break;
			case '7':
				$arr_profissoes = [
				''=>'Profissão',
				'Propietário'=>'Propietário',
				'Gerente Comercial'=>'Gerente Comercial',
				'Gerente Técnico'=>'Gerente Técnico',
				'Vendedor'=>'Vendedor',
				'Especificador'=>'Especificador',
				'Instalador'=>'Instalador'
				];
			break;

			case '8':
				$arr_profissoes = [
				''=>'Profissão',
				'Arquitetura'=>'Arquitetura',
				'Estágio de arquitetura'=>'Estágio de arquitetura',
				'Design de Interiores'=>'Design de Interiores',
				'Estágio de Design de Interiores'=>'Estágio de Design de Interiores',
				'Engenheiro Civil'=>'Engenheiro Civil', 
				'Estágio de Engenharia Civil'=>'Estágio de Engenharia Civil', 
				'Técnica em Edificações'=>'Técnica em Edificações',
				'Estágio de Técnica em Edificações'=>'Estágio de Técnica em Edificações'
				];
			break;
			default:
			$arr_profissoes = [
				''=>'Profissão',
				'Arquitetura'=>'Arquitetura',
				'Estágio de arquitetura'=>'Estágio de arquitetura',
				'Design de Interiores'=>'Design de Interiores',
				'Estágio de Design de Interiores'=>'Estágio de Design de Interiores',
				'Engenheiro Civil'=>'Engenheiro Civil', 
				'Estágio de Engenharia Civil'=>'Estágio de Engenharia Civil', 
				'Técnica em Edificações'=>'Técnica em Edificações',
				'Estágio de Técnica em Edificações'=>'Estágio de Técnica em Edificações'
				];
				break;
		}

		return $arr_profissoes;
	}
	public function getEmployers($id){
		$agenda = Agenda::find($id);

		

		return View::make('backend.instructors.employers', compact('agenda'));
	}
	public function getUserEmployers($id){
		$agenda = Agenda::find($id);

		return View::make('users.employers', compact('agenda'));
	}

	public function getRegisterEmployer($agenda_id, $share_url){
		$agenda = Agenda::find($agenda_id);
		
		if($agenda->share_url != $share_url){
			return Redirect::home()->withError('Link de cadastro inválido');
		}

		$arr_profissoes = $this->professions($agenda->atividade->id);

		if($agenda->employers()->count() >= $agenda->curso->max_quorum){
			$this->layout->content = View::make('agendas.employers.limit', compact('agenda'));
		}


		$this->layout->content = View::make('agendas.employers.create', compact('agenda', 'arr_profissoes'));

	}

	public function postRegisterEmployer($agenda_id, $share_url){
		
		$data = Input::all();

		$validator = Validator::make($data, ['name'=>'required', 'email'=>'required|email', 'profissao'=>'required']);
		
		if($validator->fails()){

			return Redirect::route('cadastro/participante',[$agenda_id, $share_url])->withErrors($validator)->withInput();
		}

		$agenda = Agenda::find($agenda_id);

		if($agenda->employers()->count() >= $agenda->curso->max_quorum){
			return Redirect::route('cadastro/participante',[$agenda_id, $share_url])->withError('Agenda já alcançou sua capacidape máxima de participantes');
		}

		if($agenda->employers()->whereEmail($data['email'])->count() > 0){
			return Redirect::route('cadastro/participante',[$agenda_id, $share_url])->withError('Você já se inscreveu para este curso!');
		}

		$employer = new Employer;

		$employer->name = $data['name'];
		$employer->email = $data['email'];
		$employer->profissao = $data['profissao'];
		$employer->agenda()->associate($agenda);

		$employer->save();

		Mail::send('emails.users.invite', ['user'=>$employer, 'responsavel'=>$agenda->user->name, 'responsavel_email'=>$agenda->user->email, 'agenda'=>$agenda->curso->consumer->name, 'data_agenda'=> Helper::ConverterBR($agenda->date_ini, true), 'hora_agenda'=>$agenda->periodo->hour_ini, 'periodo'=>$agenda->periodo->name, 'endereco'=>$agenda->endereco, 'cep'=>$agenda->cep, 'tel'=>$agenda->tel,'cel'=>$agenda->cel], function($message) use($employer, $agenda){

				$message->to($employer->email, $employer->name)->subject('Bem vindo a '.$agenda->curso->consumer->name);

		});

		return Redirect::home()->withSuccess('Você confirmou sua participação em '.$agenda->curso->consumer->name.' com sucesso! Confira as instruções que lhe enviamos por email.');
	}

	public function updateEmployerPresence($a, $id){
		$employer = Employer::find($id);

		$employer->presence = 1;
		$employer->save();

		return Redirect::route('employers/show',['id'=>$a])->with('success','Verificado com sucesso!');
	}
	public function updateEmployerCertified($a, $id){
		$employer = Employer::find($id);
		$code = uniqid(rand());
		$employer->certified = 1;
		$employer->code_certified = $code;
		$employer->save();

		//Mandando email de certificado

		Mail::queue('emails.users.certify', ['user'=>$employer, 'agenda'=>$employer->agenda->curso->name], function($message) use($employer){
			$message->to($employer->email, $employer->name)->subject('Certificado por '.$employer->agenda->curso->name);
		});

		if(Input::has('byUser')){
			return Redirect::route('user/account/agenda')->with('success','Certificado gerado com sucesso!');
		}
		return Redirect::route('employers/show',['id'=>$a])->with('success','Certificado gerado com sucesso!');
	}

	public function updateAllEmployers($a){
		if(!Input::has('employers')){
			return Redirect::route('employers/show',['id'=>$a])->with('error','Selecione ao menos um aluno participante!');
		}
		$employers = Employer::find(Input::get('employers'));
		foreach ($employers as $e) {
			# code...
			$employer = Employer::find($e->id);
			$employer->presence = 1;
			$code = uniqid(rand());
			$employer->certified = 1;
			$employer->code_certified = $code;
			$employer->save();
			
			Mail::queue('emails.users.certify', ['user'=>$employer, 'agenda'=>$employer->agenda->curso->name], function($message) use($employer){
				$message->to($employer->email, $employer->name)->subject('Certificado por '.$employer->agenda->curso->name);
			});
		}
		return Redirect::route('employers/show',['id'=>$a])->with('success','Gerado presença e certificado com sucesso!');
	}

	public function employerCertifyGenerate($id, $code){
		$employer = Employer::where('id','=',$id)
		->where('code_certified','=',$code)->first();

		if(!$employer){
			return Redirect::home()->with('success','Participante não encontrado.');
		}
		if($employer->survey_check == '1' or isset($employer->survey->id)){
			return View::make('agendas.generateCertify', compact('employer'));
		}
		return View::make('agendas.certify', compact('employer'));
	}

	public function employerCertifyAction(){

		$data = Input::except('_token');
		$validator = Validator::make($data, Survey::$rules);
		if($validator->passes()){
			if($survey = Survey::firstOrCreate($data)){
				$employer = Employer::findOrFail($data['employer_id']);
				$employer->survey_check = 1;
				$employer->save();
				return View::make('agendas.generateCertify', compact('employer'));
			}
		}
		return Redirect::back()->with('error','Erro ao salvar sua pesquisa, por favor, refaça novamente!');

	}
	public function getConfirm($id){
		$agenda = Agenda::findOrFail($id);

		return View::make('agendas.confirm', compact('agenda'));
	}
	public function postConfirm($id, $action){
		
		$agenda = Agenda::findOrFail($id);

		$log = new Adminlog;
		$log->user_id = $agenda->user->id;
		$log->instructor_id = $agenda->curso->instructor->id;
		$log->agenda()->associate($agenda);

		$arr_data = [
			'agenda'=>$agenda->curso->consumer->name,
			'user'=>$agenda->user->name,
			'data_agenda'=> Helper::ConverterBR($agenda->date_ini, true), 
			'hora_agenda'=>$agenda->periodo->hour_ini, 
			'periodo'=>$agenda->periodo->name, 
			'endereco'=>$agenda->endereco, 
			'cep'=>$agenda->cep, 
			'tel'=>$agenda->tel,
			'cel'=>$agenda->cel,
			'justify'=>Input::get('justify')
		];

		if($action == 'Y'):

			// Confirmando agenda
			$agenda->status = '3';
			$log->status = '3';

			// Gerando o link para cadastro de participantes
			$agenda->share_url = uniqid(rand());

			Mail::send('emails.instructors.confirm', $arr_data, function($message) use($agenda){
				$message->to($agenda->email, $agenda->user->name)->subject('Confirmação de agenda em '.$agenda->curso->consumer->name);
			});

		else:

			$data = Input::all();

			$validator = Validator::make($data, ['justify'=>'required|min:3']);
			if($validator->fails()){
				return Redirect::route('admin/instructors')->withError('Faltou gerar sua justificativa, para efetivar o cancelamento');
			}
			// Cancelando agendamento
			$agenda->status = '2';
			
			$log->status = '2';
			$log->justify = $data['justify'];
		
			Mail::send('emails.instructors.cancel', $arr_data, function($message) use($agenda){
				$message->to($agenda->email, $agenda->user->name)->subject('Aviso de cancelamento de agenda em '.$agenda->curso->consumer->name);
			});
		endif;

		// Saving Models
		$agenda->save();
		$log->save();

		return Redirect::route('admin/instructors', ['status'=>'concluidos'])->with('success','Agenda confirmada com sucesso');
	}

	public function finish($id){

		$agenda = Agenda::findOrFail($id);

		if($agenda->date_ini > date('Y-m-d')){
			return Redirect::back()->withError('Você não poderá concluir um curso antes da data da realização do mesmo.');
		}

		$agenda->update(['status' => '1']);

		return Redirect::route('admin/instructors', ['status'=>'concluidos'])->with('success','Gerado conlusão com sucesso');
	}
	public function backFin($id){
		$agenda = Agenda::findOrFail($id);

		$agenda->update(['status' => '0']);

		return Redirect::route('admin/instructors', ['status'=>'agendados'])->with('success','Desfez conlusão com sucesso');
	}
	public function agendaEmpresa($id){
		$user = User::find($id);
		return View::make('users.agendas', compact('user'));
	}
	public function update(){
		$data = Input::all();

		$agenda = Agenda::find($data['id']);

		if($data['endereco'] != $agenda->endereco 
			|| $data['cep'] != $agenda->cep 
			|| $data['tel'] != $agenda->tel 
			|| $data['cel'] != $agenda->cel 
			){
			
			Mail::queue('emails.instructors.notification', ['agenda'=>$agenda, 'user'=>$agenda->curso->instructor->user->name], function($message) use($agenda){

				$message->to($agenda->curso->instructor->user->email, $agenda->curso->instructor->user->name)->subject('Notificação de alteração de agenda');

			});
		}

		$agenda->update($data);

		if(isset($data['name'])){
			for ($i=1; $i <= sizeof($data['name']); $i++) { 
				$employer = Employer::find($data['cid'][$i]);

				if(isset($employer->id)){
					$employer->name = $data['name'][$i];
					if($data['email_convidado'][$i]!=$employer->email){
						$employer->email = $data['email_convidado'][$i];
						Mail::queue('emails.users.invite', ['user'=>$employer, 'agenda'=>$agenda->curso->consumer->name, 'data_agenda'=> $agenda->date_ini, 'hora_agenda'=>$agenda->periodo->hour_ini, 'periodo'=>$agenda->periodo->name, 'endereco'=>$agenda->endereco, 'cep'=>$agenda->cep, 'tel'=>$agenda->tel,'cel'=>$agenda->cel], function($message) use($employer, $agenda){

						$message->to($employer->email, $employer->name)->subject('Bem vindo a '.$agenda->curso->consumer->name);

						});
					}
					
					$employer->profissao = $data['profissao'][$i];
					$employer->agenda()->associate($agenda);
					$employer->save();
				}else{
					$new = new Employer;
					$new->name = $data['name'][$i];
					$new->email = $data['email_convidado'][$i];
					$new->profissao = $data['profissao'][$i];
					$new->agenda()->associate($agenda);
					$new->save();

					Mail::queue('emails.users.invite', ['user'=>$new, 'agenda'=>$agenda->curso->consumer->name, 'data_agenda'=> $agenda->date_ini, 'hora_agenda'=>$agenda->periodo->hour_ini, 'periodo'=>$agenda->periodo->name, 'endereco'=>$agenda->endereco, 'cep'=>$agenda->cep, 'tel'=>$agenda->tel,'cel'=>$agenda->cel], function($message) use($new, $agenda){

						$message->to($new->email, $new->name)->subject('Bem vindo a '.$agenda->curso->consumer->name);

					});

				}
			}
		}

		return Redirect::route('agenda/show/detalhes', ['id'=>$agenda->id])->with('success','Agenda editada com sucesso!');
	}
	public function destroy($id)
	{
		//
	}

	public function destroyEmployer($id){
		$user = Sentry::getUser();

		$employer = Employer::find($id);

		
		$agenda = Agenda::where('id','=',$employer->agenda->id)
		->where('user_id','=',$user->id)->first();
		
		if(!$agenda){
			return Redirect::route('user/account/agenda')->with('error','Você não tem esta permissão de acesso!');
		}

		if($agenda->employers->count() <= $employer->agenda->curso->min_quorum){
			return Redirect::back()->with('error','Você não pode excluir mais participantes.');
		}

		$employer->delete();

		return Redirect::back()->with('success','Convidado <b>'.$employer->name.'</b> apagado com sucesso!');
	}

}