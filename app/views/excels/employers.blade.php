<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
		<style>
			td { padding: 0; }
		</style>
	</head>
	<body>
		<table width="595" align="center" cellspadding="0" cellspacing="0" style="border: 1px solid #ccc;">
			<tr>
				<th>Nome</th>
				<th>Email</th>
				<th>Profiss&atilde;o</th>
				<th>Curso</th>
				<th>Instrutor</th>
				 <th>Data</th>
				 <th>Hora</th>
				<th>Local</th>
				<th>Respons&aacute;vel Email</th>
				<th>Respons&aacute;vel Telefone/Celular</th>
				<th>Programa - Relev&acirc;ncia do conte&uacute;do</th> 
				<th>Programa - Aplicabilidade no trabalho</th>
				<th>Programa - Carga hor&aacute;ria</th>
				<th>Intrutor - Dom&iacute;nio do assunto abordado</th>
				<th>Intrutor - Clareza e objetividade</th>
				<th>Intrutor - Habilidade em estimular a aten&ccedil;&atilde;o do grupo</th>
				<th>Intrutor - Metodologia aplicada</th>
				<th>Intrutor - Pontualidade</th>
				<th>Programa - Mensagem</th>
			</tr>
			@foreach($employers as $e)
				<tr>
					<td>{{utf8_decode($e['name'])}}</td>
					<td>{{$e->email}}</td>
					<td>{{utf8_decode($e->profissao)}}{{$e->agenda->email}}</td>
					<td>{{utf8_decode($e->agenda->curso->name)}}</td>
					<td>{{utf8_decode($e->agenda->curso->instructor->name)}}</td>
					 <td>{{Helper::ConverterBR($e->agenda->date_ini, true)}}</td>
					 <td>{{ $e->agenda->periodo->hour_ini }}h at&eacute; {{$e->agenda->periodo->hour_ini+$e->agenda->curso->tipo->duracao}}</td>
					<td>{{utf8_decode($e->agenda->endereco)}}, {{utf8_decode($e->agenda->cidade->name)}} - {{$e->agenda->cidade->estado->uf}}</td>
					<td>{{$e->agenda->email}}</td>
					<td>{{$e->agenda->tel}}/{{$e->agenda->cel}}</td>
					
					<td>{{Helper::respostaPesquisa($e->survey['curso_1'])}}</td>
					<td>{{Helper::respostaPesquisa($e->survey['curso_2'])}}</td>
					<td>{{Helper::respostaPesquisa($e->survey['curso_3'])}}</td>
					<td>{{Helper::respostaPesquisa($e->survey['instrutor_1'])}}</td>
					<td>{{Helper::respostaPesquisa($e->survey['instrutor_2'])}}</td>
					<td>{{Helper::respostaPesquisa($e->survey['instrutor_3'])}}</td>
					<td>{{Helper::respostaPesquisa($e->survey['instrutor_4'])}}</td>
					<td>{{Helper::respostaPesquisa($e->survey['instrutor_5'])}}</td>
					<td>{{$e->survey['curso_msg']}}</td>


				</tr>
			@endforeach
		</table>
	</body>
</html>