@section('content')
 <!-- confirmação de curso -->
 	<div id="cadastro">
		
		@include('layouts.notifications')
	{{ Form::open(['route'=>'cursos/store', 'method'=>'post', 'id'=>'frmCreateCurso']) }}
		<h2>{{$curso->consumer->name}}</h2>
		<ul>
			{{--<li>
				<span>Localização:</span>
				<span>{{ $curso->cidade->name }} - {{ $curso->cidade->estado->uf }}
					
				</span>
			</li>
			<li>
				<span>Especialidade</span>
				<span>{{ $curso->atividade->name }}</span>
			</li>--}}
			<li>
				<span class="last">Instrutor(a)</span>
				<span>{{ $curso->instructor->name }}</span>
			</li>
			<li>
				<span class="last">Instrutor(a) Email</span>
				<span>{{ $curso->instructor->user->email }}</span>
			</li>
				<li>
				<span class="last">Instrutor(a) Telefone(s)</span>
				<span>{{ $curso->instructor->user->tel }} | {{ $curso->instructor->user->cel }}</span>
			</li>
		</ul>

		<div class="blc">
			<em>* Não confirme este curso caso não esteja nesta localidade.</em>
		</div>
		
		<div class="blc">
		<p class="sub"></p> 
		<!--<p class="sub"><strong>Importante:</strong> O solicitante deverá ter um espaço para realização do curso contando com o número de participantes confirmados.</p>-->

		</div>
		
		<div class="row">

		
		{{ Form::input('hidden', 'curso_id', $data['curso_id']) }}
		{{-- Form::input('hidden', 'date_ini[]', $realDate) --}}
		{{ Form::input('hidden', 'user_id', Sentry::getUser()->id) }}
		{{ Form::input('hidden', 'atividade_id', $data['atividade_id']) }}

		{{-- Form::input('hidden', 'periodo_id', $periodo->id) --}}
		<h4>Favor, prossiga confirmando o endereço desejado para solicitar o agendamento.</h4>

		@foreach($dates as $k => $d)
			{{ Form::hidden('date_ini[]',$d) }}
			<div class="well">
				<table>
					<tr>
						<td><strong>Nº</strong></td>
						<td>#{{$k+1}}</td>
					</tr>
					<tr>
						<td><strong>Data:</strong></td>
						<td>{{ $d }}</td>
					</tr>
					<tr>
						<td><strong>Horário:</strong></td>
						<td>
							<select class="periodo_id" name="periodo_id[]">
								{{$curso->periodo(Helper::ConverterUS($d, true))}}
							</select>
						</td>
					</tr>
					<tr>
						<td><strong>Email:</strong></td>
						<td>{{ Form::text('email', $user->email, ['placeholder'=>'Email','id'=>'email', 'readonly']) }}</td>
					</tr>
					<tr>
						<td><strong>Telefone:</strong></td>
						<td>{{ Form::text('tel', $user->tel, ['placeholder'=>'Telefone','id'=>'tel', 'readonly']) }}</td>
					</tr>
					<tr>
						<td><strong>Celular:</strong></td>
						<td>{{ Form::text('cel', $user->cel, ['placeholder'=>'Celular','id'=>'cel', 'readonly']) }}</td>
					</tr>

					<tr>
						<td><strong>CEP:</strong></td>
						<td>{{ Form::text('cep', $user->cep, ['placeholder'=>'CEP','id'=>'cep', 'readonly']) }}</td>
					</tr>

					<tr>
						<td><strong>Endereço:</strong></td>
						<td>{{ Form::text('endereco', $user->endereco, ['placeholder'=>'Endereço do local onde o curso será realizado', 'class'=>'large','readonly']) }}</td>
					</tr>
					<tr>
						<td><strong>Estado:</strong></td>
						<td>{{ Form::select('estado_id[]', [''=>'UF']+$estados, $user->cidade_id, ['class'=>'estado_id required']) }}</td>
					</tr>
					<tr>
						<td><strong>Cidade:</strong></td>
						<td>{{ Form::select('cidade_id[]', [''=>'Selecione sua cidade']+$cidades, $user->cidade_id, [ 'disabled', 'class'=>'cidade_id required']) }}</td>
					</tr>
					<tr>
						<td colspan="2"><a href="javaScript:;" class="ft12 orange2 u alter_address">Alterar dados de endereço para esta agenda</a></td>
						
					</tr>
				</table>
				 
			</div>

		@endforeach
		
		</div>
		
		{{--
		<p class="sub mrg-btt-10">Dados dos participantes. Mínimo de {{ $curso->min_quorum }} e máximo de {{ $curso->max_quorum }}.</p>
		
		<div id="addinput">
			@for($i=1; $i<=$curso->min_quorum; $i++)
			<div class="item">
			<div class="blc">
				<span>
					<p>{{ Form::text('name['.$i.']', '',['placeholder'=>'Nome completo', 'class'=>'required']) }}</p>
					<p class="error">{{ $errors->first('name') }}</p>
				</span>
				<span>
					<p>{{ Form::text('email_convidado['.$i.']','',['placeholder'=>'E-mail', 'class'=>'required convidado']) }}</p>
					<p class="error">{{ $errors->first('cpf') }}</p>
				</span>	
				<!-- <a class="bt bt_x"></a> -->
			</div>
			<div class="blc">
				<span>
					<p>{{Form::select('profissao['.$i.']', $arr_profissoes,null,['class'=>'required'])}}</p>
					<p class="error">{{ $errors->first('profissao') }}</p>
				</span>	
				<span style="display:none;" id="informe">
					<p>{{ Form::text('informe','',['placeholder'=>'Caso não tenha sua profissão listada ao lado']) }}</p>
					<p class="error">{{ $errors->first('informe') }}</p>
				</span>	
			</div>
			<hr class="left">
			</div>
			@endfor
		</div>

		
		<div class="blc"> <a class="bt orange add-part">v</a> Adicionar participantes</div>
		<div class="blc"> <p class="ft12">*Todos os campos são obrigatórios</p> </div>--}}
		<div style="display:none;" class="blc alteraction"> <p class="ft16 ft_orange"><strong>Atenção: você alterou o endereço do local onde será realizado o curso.</strong></p> </div>
		<div class="blc ft14"> <input type="checkbox" name="checkIn" value="1"> <em>Aceitar os <a href="#terms" id="lookTerm" class="txt_underline">termos de uso</a></em> 
			<p style="margin-top:6px;color: red;font-size: 11px;"class="checkError error"></p>
		</div>
		<div class="blc">
			{{ Form::submit('Confirmar solicitação',['class'=>'bt blue bt_large']) }}
			<span style="float:none; margin-left:20px;" class="wrap_loader"></span>
		</div>
		{{ Form::close() }}

		{{link_to_route('cursos/index','< Voltar',null,['class'=>'ft11 u gray','style'=>'float:right;'])}}
	</div>
	@include('layouts.modals.terms')
	{{--<script type="text/javascript">
		$(document).ready(function(){
			var addDiv = $('#addinput');
			
			var i = $('#addinput .item').size() + 1;

			$('.add-part').click(function() {
				if(i <= {{ $curso->max_quorum  }}){
				$('#addinput').append('<div class="item"><div class="blc"><span><p><input placeholder="Nome completo" class="required" name="name['+i+']" type="text" value=""></p><p class="error"></p></span><span><p><input placeholder="E-mail" class="required email convidado" name="email_convidado['+i+']" type="text" value=""></p><p class="error"></p> </span> <a href="javaScript:;" class="bt bt_x delete"></a></div><div class="blc"><span><p>{{Form::select('profissao['.$i.']', $arr_profissoes, null,['class'=>'required'])}}</p><p class="error"></p></span><span style="display:none;" id="informe"><p><input placeholder="Caso não tenha sua profissão listada ao lado" name="informe" type="text" value=""></p><p class="error"></p></span></div><hr class="left"></div>');
			
				i++;
				}
				return false;
				});

			$('#addinput').on('click','.delete', function() {
	
				if( i > {{ $curso->min_quorum }}) {
				$(this).parent().parent('.item').remove();
				i--;
			}
			return false;
			});
		});
	</script>--}}
@stop