@extends('layouts.base')

@section('content')
	<div id="grid_cursos">
		<h2 class="ft24" style="margin-bottom: 10px;">Marque a sua participação nos cursos in company</h2>
  		<p style="margin-bottom: 20px; color: #009fe3;"><i><b>Atenção!</b> Os cursos ou palestras só poderão ser realizados em local da mesma cidade onde o instrutor escolhido tem disponibilidade.</i></p>
		<div class="search">
			<p class="ft15 white">Filtre a sua busca completando qualquer ou todos os campos abaixo para encontrar os resultados:</p>
			{{ Form::open(['route'=>'cursos/store', 'method'=>'get']) }}
				{{ Form::select('atividade', [''=>'Selecione']) }}
				{{ Form::select('estado', [''=>'Selecione'],null,['class'=>'sm']) }}
				{{ Form::select('cidade', [''=>'Selecione'],null,['class'=>'md']) }}
				{{ Form::select('tipo', [''=>'Selecione']) }}
				{{ Form::select('instrutor', [''=>'Selecione']) }}
				{{ Form::text('name',null,['placeholder'=>'Nome do curso']) }}
				{{ Form::submit('', ['class'=>'bt_search']) }}
			{{ Form::close() }}
		</div>
		<div id="datewrap">
			<p class="sub">Resultados encontrados no calendário:</p>
			<div id="datepicker"></div>
			<img src="{{URL::to('/')}}/images/legendas.png">
		</div>
		<div id="results">
			<p class="sub">Total de cursos encontrados: 1</p>
			<ul>
				<li>
					<table cellspadding="0" cellspacing="0" class="wraptit">
						<tr>
							<td style="padding:13px;" valign="center">
								<p class="ft16 b" style="margin-bottom:7px;">Curso de Produtos Premium Knauf</p>
								<p class="ft12">Breve descrição do curso. Breve descrição do curso. Breve descrição do curso. Breve descrição do curso.</p>
							</td>
							<td class="pullit bt_agendar" style="padding:25px; cursor:pointer;" valign="center" ><a>Agendar</a></td>
						</tr>
						
					</table>
					<div class="mid">
						<p class="ft12">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam tellus quam, venenatis et malesuada sollicitudin, porta vitae eros. Nunc nec pulvinar lectus, at sagittis felis. Aliquam erat volutpat. Suspendisse potenti. Integer eu justo porttitor sem tristique congue et in quam. Fusce vel turpis at turpis iaculis semper ac aliquet tellus. Curabitur vitae sapien elit. Aliquam in libero ipsum. Donec sed sollicitudin libero, quis auctor ligula. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Duis ante enim, ultrices vitae porta et, tempus a nunc. </p>
						<p class="b ft12">Veja as disponibilidades na agenda ao lado, selecione uma data e confirme.</p>
						<p><a class="bt blue" href="">Confirmar</a></p>
					</div>

				</li>
				<li>
					<table cellspadding="0" cellspacing="0" class="wraptit">
						<tr>
							<td style="padding:13px;" valign="center">
								<p class="ft16 b" style="margin-bottom:7px;">Curso de Produtos Premium Knauf</p>
								<p class="ft12">Breve descrição do curso. Breve descrição do curso. Breve descrição do curso. Breve descrição do curso.</p>
							</td>
							<td class="pullit bt_agendar" style="padding:25px; cursor:pointer;" valign="center" ><a>Agendar</a></td>
						</tr>
						
					</table>
					<div class="mid">
						<p class="ft12">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam tellus quam, venenatis et malesuada sollicitudin, porta vitae eros. Nunc nec pulvinar lectus, at sagittis felis. Aliquam erat volutpat. Suspendisse potenti. Integer eu justo porttitor sem tristique congue et in quam. Fusce vel turpis at turpis iaculis semper ac aliquet tellus. Curabitur vitae sapien elit. Aliquam in libero ipsum. Donec sed sollicitudin libero, quis auctor ligula. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Duis ante enim, ultrices vitae porta et, tempus a nunc. </p>
						<p class="b ft12">Veja as disponibilidades na agenda ao lado, selecione uma data e confirme.</p>
						<p><a class="bt blue" href="">Confirmar</a></p>
					</div>

				</li>
				<li>
					<table cellspadding="0" cellspacing="0" class="wraptit">
						<tr>
							<td style="padding:13px;" valign="center">
								<p class="ft16 b" style="margin-bottom:7px;">Curso de Produtos Premium Knauf</p>
								<p class="ft12">Breve descrição do curso. Breve descrição do curso. Breve descrição do curso. Breve descrição do curso.</p>
							</td>
							<td class="pullit bt_agendar" style="padding:25px; cursor:pointer;" valign="center" ><a>Agendar</a></td>
						</tr>
						
					</table>
					<div class="mid">
						<p class="ft12">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam tellus quam, venenatis et malesuada sollicitudin, porta vitae eros. Nunc nec pulvinar lectus, at sagittis felis. Aliquam erat volutpat. Suspendisse potenti. Integer eu justo porttitor sem tristique congue et in quam. Fusce vel turpis at turpis iaculis semper ac aliquet tellus. Curabitur vitae sapien elit. Aliquam in libero ipsum. Donec sed sollicitudin libero, quis auctor ligula. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Duis ante enim, ultrices vitae porta et, tempus a nunc. </p>
						<p class="ft12">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam tellus quam, venenatis et malesuada sollicitudin, porta vitae eros. Nunc nec pulvinar lectus, at sagittis felis. Aliquam erat volutpat. Suspendisse potenti. Integer eu justo porttitor sem tristique congue et in quam. Fusce vel turpis at turpis iaculis semper ac aliquet tellus. Curabitur vitae sapien elit. Aliquam in libero ipsum. Donec sed sollicitudin libero, quis auctor ligula. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Duis ante enim, ultrices vitae porta et, tempus a nunc. </p>
						<p class="ft12">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam tellus quam, venenatis et malesuada sollicitudin, porta vitae eros. Nunc nec pulvinar lectus, at sagittis felis. Aliquam erat volutpat. Suspendisse potenti. Integer eu justo porttitor sem tristique congue et in quam. Fusce vel turpis at turpis iaculis semper ac aliquet tellus. Curabitur vitae sapien elit. Aliquam in libero ipsum. Donec sed sollicitudin libero, quis auctor ligula. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Duis ante enim, ultrices vitae porta et, tempus a nunc. </p>
						<p class="b ft12">Veja as disponibilidades na agenda ao lado, selecione uma data e confirme.</p>
						<p><a class="bt blue" href="">Confirmar</a></p>
					</div>

				</li>
				<li>
					<table cellspadding="0" cellspacing="0" class="wraptit">
						<tr>
							<td style="padding:13px;" valign="center">
								<p class="ft16 b" style="margin-bottom:7px;">Curso de Produtos Premium Knauf</p>
								<p class="ft12">Breve descrição do curso. Breve descrição do curso. Breve descrição do curso. Breve descrição do curso.</p>
							</td>
							<td class="pullit bt_agendar" style="padding:25px; cursor:pointer;" valign="center" ><a>Agendar</a></td>
						</tr>
						
					</table>
					<div class="mid">
						<p class="ft12">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam tellus quam, venenatis et malesuada sollicitudin, porta vitae eros. Nunc nec pulvinar lectus, at sagittis felis. Aliquam erat volutpat. Suspendisse potenti. Integer eu justo porttitor sem tristique congue et in quam. Fusce vel turpis at turpis iaculis semper ac aliquet tellus. Curabitur vitae sapien elit. Aliquam in libero ipsum. Donec sed sollicitudin libero, quis auctor ligula. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Duis ante enim, ultrices vitae porta et, tempus a nunc. </p>
						<p class="b ft12">Veja as disponibilidades na agenda ao lado, selecione uma data e confirme.</p>
						<p><a class="bt blue" href="">Confirmar</a></p>
					</div>

				</li>
			</ul>
		</div>
	</div>
	
@stop