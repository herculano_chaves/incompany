@extends('layouts.backend')



@section('content')

	@include('layouts.backendmenu')

	<div class="content">

		<div class="panel">

			<div class="panel-heading">

				<strong>Cursos</strong>

			</div>

			

				<div class="panel-body">	

					<h2>Editar curso</h2>

					@include('layouts.notifications-list')

					{{ Form::open(['method'=>'post', 'action'=>'curso/update', 'id'=>'frmAddCurso']) }}

					{{ Form::input('hidden','id',$curso->id) }}

					<table class="table table-striped table-bordered">

	                    <thead>

	                        <tr>

	                            <th colspan="2">Editar</th>

	                				

	                        </tr>

	                    </thead>

	                    <tbody>

	          

	                    		<tr>
	                    			<td>
	                    				{{ Form::label('consumer_id', 'Nome do curso:') }}
	                    				{{ Form::select('consumer_id', [''=>'Selecione nome de curso']+$consumers, $curso->consumer->id) }}
	                    			</td>
	                    		</tr>

	                    		<tr>

	                    			<td>{{ Form::label('min_quorum', 'Mínimo participantes:') }} {{Form::text('min_quorum',$curso->min_quorum,['placeholder'=>'Mínimo participantes'])}}</td>

	                    			<td>{{ Form::label('max_quorum', 'Máximo participantes:') }} {{Form::text('max_quorum',$curso->max_quorum,['placeholder'=>'Máximo participantes'])}}</td>

	                    		</tr>

	                    		<tr>
									<td>
										{{ Form::label('tipo_id', 'Tipo:') }}   
										{{ Form::select('tipo_id', [''=>'Selecione']+$tipos, $curso->tipo_id) }} 
									</td>
	                    			<td>
	                    				{{ Form::label('atividade_id', 'Atividades:') }}  
	                    				{{ Form::select('atividade_id[]', [''=>'Selecione Atividades']+$atividades, $curso->atividades->lists('id'), ['multiple']) }}
	                    			</td>

	                    		</tr>

	                    		<tr>

	                    			<td>{{ Form::label('instructor_id', 'Instrutor:') }}  
	                    				{{ Form::select('instructor_id', [''=>'Selecione instrutores']+$instrutores, $curso->instructor->id) }} 
	                    			</td>

	                    		</tr>

	                    		<tr>

	                    			<td colspan="2">{{ Form::submit('Atualizar') }}</td>

	                    		</tr>

	                    	</tr>

	                    </tbody>

	                </table>

	                {{ Form::close() }}

				</div>

		</div>

@stop