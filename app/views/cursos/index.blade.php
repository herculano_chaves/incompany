@extends('layouts.base')

@section('content')
	<div id="grid_cursos">
		@include('layouts.notifications-list')
		<h2 class="ft24" style="margin-bottom: 10px;">Marque a sua participação nos cursos in company</h2>
  		<p style="margin-bottom: 20px; color: #009fe3;"><i><b>Atenção!</b> Os cursos ou palestras só poderão ser realizados em local da mesma cidade onde o instrutor escolhido tem disponibilidade.</i></p>
		<div class="search">
			<p class="ft15 white">Filtre a sua busca completando qualquer ou todos os campos abaixo para encontrar os resultados:</p>
			{{ Form::open(['route'=>'cursos/search', 'method'=>'get']) }}

				@if(isset($a_2)) {{ Form::select('estado', [null=>"UF"]+$estados, $a_2->id ,['class'=>'sm']) }} @else {{ Form::select('estado', [null=>"UF"]+$estados,null,['class'=>'sm']) }} @endif

				@if(isset($a_3)) 
					{{ Form::select('cidade', [''=>'Selecione Cidade']+$cidades, $a_3->id , ['class'=>'md']) }} 
				@else 
					{{ Form::select('cidade', [''=>'Selecione'],null,['class'=>'md', 'disabled'=>'disabled']) }} 
				@endif
				
				{{--
				@if(isset($a_1)) 
					{{ Form::select('atividade', [''=>'Área de atuação']+$atividades, $a_1->id) }} 
				@else 
					{{ Form::select('atividade', [''=>'Área de atuação']+$atividades, null, ['disabled']) }} 
				@endif
				--}}

				@if(isset($a_4)) 
					{{ Form::select('tipo', [''=>'Tipos']+$tipos, $a_4->id) }} 
				@else 
					{{ Form::select('tipo', [''=>'Tipos']+$tipos,null, ['disabled'=>'disabled']) }} 
				@endif

				@if(isset($a_5)) 
					{{ Form::select('instrutor', [''=>'Instrutores']+$instrutores, $a_5->id) }} 
				@else 
					{{ Form::select('instrutor', [''=>'Instrutores']+$instrutores,null, ['disabled'=>'disabled']) }} 
				@endif
				
				{{ Form::button('Buscar <i class="fa fa-search"></i>', ['class'=>'','type'=>'submit']) }}
			{{ Form::close() }}
			<div class="loader"></div>
		</div>
		<div id="datewrap">
			<p class="sub">Resultados encontrados no calendário:</p>
			<div id="datepicker"></div>
			<div class="legenda"></div>
		</div>
 
		{{-- Se houver busca --}}
		@if(isset($search))
			<div id="results">
				<p class="sub">Total de cursos encontrados: {{ $search->getTotal() }}</p>
				<ul>
					@foreach($search as $c)
						<li>
							<table cellspadding="0" cellspacing="0" class="wraptit">
								<tr>
									<td style="padding:13px;" valign="center">
										<p class="ft16 b" style="margin-bottom:7px;">{{ $c->consumer->name }}</p>
										<p class="ft12"><span class="b">Localidade:</span> <span class="b">Tipo:</span> {{ $c->tipo->name }} | <span class="b">Instrutor(a):</span> {{ $c->instructor->name }}</p><br/>
										<p class="ft12">{{ $c->excerpt }}</p>
									</td>
									<td class="pullit bt_agendar" data-instructor="{{$c->instructor->id}}" data-curso="{{$c->id}}" style="padding:25px; cursor:pointer;" valign="center" ><a>Agendar</a></td>
								</tr>
							</table>
							<div class="mid">
								<p class="ft12">{{ $c->consumer->description }}</p>
								{{--<p class="b ft12">Veja as disponibilidades na agenda ao lado, selecione uma data, um período e escreva seu participante.</p>--}}
									{{ Form::open(['route'=>'curso/preconfirma', 'method'=>'post']) }}
									<input type="hidden" name="curso_id" value="{{ $c->id }}">
									<input type="hidden" name="user_id" value="@if(Sentry::check()) {{ $user->id }} @endif">
									{{ Form::hidden('atividade_id') }}	
										{{ Form::hidden('status_periodo', $c->statusPeriodo()) }}
										
										<input type="hidden" name="passDate" class="passDate">
									{{ Form::close() }}
								{{ $c->statusPeriodoText() }}
								
							</div>
						</li>
					@endforeach
				</ul>
				{{ $search->appends($_GET)->links() }}
			</div>
		@endif
	</div>
	
@stop