@extends('layouts.base')

@section('content')
	@include('layouts.notifications')
	<div id="presentation">
		<h2>Agende agora mesmo cursos <span>In Company!</span></h2>
		<p>A Knauf leva até a sua empresa a oportunidade de atualização profissional</p>
		<div class="bt_group">
			{{ HTML::link('cursos', 'Cursos Oferecidos', ['class'=>'bt blue']) }}
			{{ HTML::link('cadastro', 'Cadastre-se', ['class'=>'bt orange']) }}
		</div>
	</div>
	<ul id="banner">
		<li><img src="images/banner_home.jpg" alt="Knauf In Company"></li>
	</ul>
@stop