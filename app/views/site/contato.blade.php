@extends('layouts.base')

@section('content')
	<div id="contato">
	<h2>Envie sua mensagem que entraremos em contato em breve.</h2>
		@include('layouts.notifications')
		{{ Form::open(['route'=>'site/contact', 'method'=>'post']) }}
		 {{-- Form::honeypot('my_name', 'my_time') --}}
		<div class="blc">
			<span>
				<p>{{ Form::text('name', '',['placeholder'=>'Nome completo (Responsável pela empresa)']) }}</p>
				<p class="error">{{ $errors->first('name') }}</p>
			</span>
			<span>
				<p>{{ Form::email('email','',['placeholder'=>'E-mail']) }}</p>
				<p class="error">{{ $errors->first('email') }}</p>
			</span>	
		</div>
		<div class="blc">
			<span>
				<p>{{ Form::text('tel', '',['placeholder'=>'Telefone','id'=>'tel']) }}</p>
				<p class="error">{{ $errors->first('tel') }}</p>
			</span>
			<span>
				<p>{{ Form::text('assunto','',['placeholder'=>'Assunto']) }}</p>
				<p class="error">{{ $errors->first('assunto') }}</p>
			</span>	
		</div>
		<div class="blc">
			<span>
				<p>{{ Form::textarea('mensagem','',['placeholder'=>'Mensagem']) }}</p>
				<p class="error">{{ $errors->first('mensagem') }}</p>
			</span>
		</div>
		<div class="blc"><p class="ft12">*Todos os campos são obrigatórios</p></div>
		<div class="blc">
			{{ Form::submit('Enviar',['class'=>'bt blue bt_large']) }}
		</div>
		{{ Form::close() }}
	</div>
@stop