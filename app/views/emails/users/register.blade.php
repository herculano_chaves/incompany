<!DOCTYPE html>

<html lang="en-US">

	<head>

		<meta charset="utf-8">

		<style>

			td { padding: 0; }

		</style>

	</head>

	<body>

		<table width="595" align="center" cellspadding="0" cellspacing="0" style="border: 1px solid #ccc;">

			<tr>

				<td><img src="{{URL::to('/')}}/images/emails/top.png" width="595" height="100" style="display:block; border:none;"></td>

			</tr>

			<tr>

				<td><p style="font:18px Arial; color:#313131; margin: 40px 0 0 30px;"><b>Recebemos o seu pedido de cadastro.</b></p></td>

			</tr>

			<!-- <tr>

				<td><p style="font:18px Arial; color:#313131; margin: 10px 0 0 30px;"></p></td>

			</tr>
 -->
			<tr>

				<td><p style="font:18px Arial; color:#313131; margin: 40px 0 0 30px;">Clique no link abaixo para criar uma senha e terminar o processo de cadastro:</p></td>

			</tr>

			<tr>

				<td><p style="margin: 10px 0 40px 30px;">{{ HTML::linkRoute('user/set-password', null, array('code'=>$token,'id'=>$id)) }}

				<!-- 	<a href="http://knauf.com.br/akademiaknauf/curso-incompany/facilisisnislsagittiseuismod" style="font:18px Arial; color:#353535;">http://knauf.com.br/akademiaknauf/curso-incompany/facilisisnislsagittiseuismod</a> -->

				</p></td>

			</tr>

			<tr>
				<td>
					<table width="100%" align="center" cellspadding="0" cellspacing="0">
						<tr>
							<td>
								<a href="http://knauf.com.br/" target="_blank">
									<img src="{{URL::to('/')}}/emails/bottom1_01.jpg" width="161" height="82" style="display:block; border:none;">
								</a>
							</td>
							<td>
								<img src="{{URL::to('/')}}/emails/bottom1_02.jpg" width="314" height="82" style="display:block; border:none;">
							</td>
							<td>	
								<a href="https://www.facebook.com/KnaufDoBrasil/?fref=ts" target="_blank">
									<img src="{{URL::to('/')}}/emails/bottom1_03.jpg" width="57" height="82" style="display:block; border:none;">
								</a>
							</td>
							<td>
								<a href="https://www.youtube.com/user/KnaufBrasil" target="_blank">
									<img src="{{URL::to('/')}}/emails/bottom1_04.jpg" width="63" height="82" style="display:block; border:none;">
								</a>
							</td>
						</tr>
					</table>
				</td>
			</tr>

		</table>

	</body>

</html>

