@section('content')
	<div id="contato">
	<h2>Confirme sua participação</h2>
		@include('layouts.notifications')
		<p>{{ $agenda->employers()->count() }} de {{ $agenda->curso->max_quorum }} vagas preenchidas.</p><br>
		<table class="employer_create">
			<tr>
				
				<td colspan="2">{{ $agenda->curso->name }}</td>
			</tr>
			<tr>
				<td>Localização:</td>
				<td>{{ $agenda->cidade->name }} - {{ $agenda->cidade->estado->uf }}
					
				</td>
			</tr>
			{{--<tr>
				<td>Especialidade</td>
				<td>{{ $agenda->curso->atividade->name }}</td>
			</tr>--}}
			<tr>
				<td class="last">Instrutor(a)</td>
				<td>{{ $agenda->curso->instructor->name }}</td>
			</tr>
			<tr>
				<td class="last">Instrutor(a) Email</td>
				<td>{{ $agenda->curso->instructor->user->email }}</td>
			</tr>
				<tr>
				<td class="last">Instrutor(a) Telefone(s)</td>
				<td>{{ $agenda->curso->instructor->user->tel }} | {{ $agenda->curso->instructor->user->cel }}</td>
			</tr>
		</table>

		<h3>Dados básicos</h3><br/>
		{{ Form::open(['route'=> array('post/cadastro/participante', $agenda->id, $agenda->share_url) ]) }}
		 {{-- Form::honeypot('my_name', 'my_time') --}}
		<div class="blc">
			<span>
				<p>{{ Form::text('name', '',['placeholder'=>'Nome completo', 'class'=>'required']) }}</p>
				<p class="error">{{ $errors->first('name') }}</p>
			</span>
			<span>
				<p>{{ Form::email('email','',['placeholder'=>'E-mail']) }}</p>
				<p class="error">{{ $errors->first('email') }}</p>
			</span>	
		</div>
		<div class="blc">
			<span>
				<p>{{ Form::select('profissao', [''=>'Selecione Profissão']+$arr_profissoes, null, ['class'=>'required']) }}</p>
				<p class="error">{{ $errors->first('profissao') }}</p>
			</span>
			
		</div>
		<div class="blc"><p class="ft12">*Todos os campos são obrigatórios</p></div>
		<div class="blc">
			{{ Form::submit('Enviar',['class'=>'bt blue bt_large']) }}
		</div>
		{{ Form::close() }}
	</div>
@stop