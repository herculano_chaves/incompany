@section('content')
	<div class="alert alert-warning">
		<h2>{{ $agenda->employers()->count() }} de {{ $agenda->curso->max_quorum }} vagas preenchidas.</h2>
		<p>Agenda para {{ $agenda->curso->consumer->name }} se encontra completa, todos os cadastros foram preenchidos. Favor entre em contato com o instrutor deste curso e sua organização</p>
	</div>
@stop