@extends('layouts.base')
@section('content')
<div id="cadastro">
		
		@include('layouts.notifications')
	<h2>A Knauf Brasil agradece mais uma vez pela sua participação!</h2>

	<p class="ft18 blue">Sua presença foi muito importante para nós e para melhorar cada vez mais, gostaríamos de contar com a sua colaboração para responder o breve questionário. Em seguida será gerado o seu certificado de participação!</p>
	{{ Form::open(['action'=>'certify/generate', 'method'=>'post', 'id'=>'frmCertificado']) }}
	{{ Form::input('hidden', 'employer_id', $employer->id) }}
	<div class="rollUp">
		<h3>Avaliação do Programa:</h3>
		<div>
			<p>1 - Relevância do conteúdo</p>
			{{ Form::radio('curso_1', '1') }} <span>suficiente</span>
			{{ Form::radio('curso_1', '2') }} <span>bom</span>
			{{ Form::radio('curso_1', '3') }} <span>excelente</span>
			<p class="error"></p>
		</div>
		<div>
			<p>2 - Aplicabilidade no trabalho</p>
			{{ Form::radio('curso_2', '1') }} <span>suficiente</span>
			{{ Form::radio('curso_2', '2') }} <span>bom</span>
			{{ Form::radio('curso_2', '3') }} <span>excelente</span>
			<p class="error"></p>
		</div>
		<div>
			<p>3 - Carga horária</p>
			{{ Form::radio('curso_3', '1') }} <span>suficiente</span>
			{{ Form::radio('curso_3', '2') }} <span>bom</span>
			{{ Form::radio('curso_3', '3') }} <span>excelente</span>
			<p class="error"></p>
		</div>
	</div>
	<div class="rollUp">
		<h3>Avaliação do Instrutor:</h3>
		<div>
			<p>1 - Domínio do assunto abordado</p>
			{{ Form::radio('instrutor_1', '1') }} <span>suficiente</span>
			{{ Form::radio('instrutor_1', '2') }} <span>bom</span>
			{{ Form::radio('instrutor_1', '3') }} <span>excelente</span>
			<p class="error"></p>
		</div>
		<div>
			<p>2 - Clareza e objetividade</p>
			{{ Form::radio('instrutor_2', '1') }} <span>suficiente</span>
			{{ Form::radio('instrutor_2', '2') }} <span>bom</span>
			{{ Form::radio('instrutor_2', '3') }} <span>excelente</span>
			<p class="error"></p>
		</div>
		<div>
			<p>3 - Habilidade em estimular a atenção do grupo</p>
			{{ Form::radio('instrutor_3', '1') }} <span>suficiente</span>
			{{ Form::radio('instrutor_3', '2') }} <span>bom</span>
			{{ Form::radio('instrutor_3', '3') }} <span>excelente</span>
			<p class="error"></p>
		</div>
		<div>
			<p>4 - Metodologia aplicada</p>
			{{ Form::radio('instrutor_4', '1') }} <span>suficiente</span>
			{{ Form::radio('instrutor_4', '2') }} <span>bom</span>
			{{ Form::radio('instrutor_4', '3') }} <span>excelente</span>
			<p class="error"></p>
		</div>
		<div>
			<p>5 - Pontualidade</p>
			{{ Form::radio('instrutor_5', '1') }} <span>suficiente</span>
			{{ Form::radio('instrutor_5', '2') }} <span>bom</span>
			{{ Form::radio('instrutor_5', '3') }} <span>excelente</span>
			<p class="error"></p>
		</div>
	</div>
	<div class="rollUp">
		<h3>Críticas e Sugestões:</h3>
		{{ Form::textarea('curso_msg') }}
		<p class="error"></p>
		{{ Form::submit('Enviar opinião e gerar certificado', ['class'=>'bt orange bt_large']) }}
	</div>
	{{ Form::close() }}
</div>
@stop