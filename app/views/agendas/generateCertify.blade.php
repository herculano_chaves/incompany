<html>
<head>
<title>Knauf Akademia</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script>
	window.onload=function(){
		window.print();
	};
</script>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<table width="842" height="595" border="0" align="center" cellpadding="0" cellspacing="0" background="{{ asset('images/certificado/background.jpg') }}">
	<tr>
		<td colspan="3">
			<img src="{{ asset('images/certificado/Cabecalho.jpg') }}" width="842" height="119" alt="Certificado Knauf Akademia"></td>
	</tr>
	<tr>
		<td width="84">&nbsp;</td>
        
	  <td width="674" height="344" align="center" style="font-family:arial; font-size:26px; color:#616161;">
        
        <p>A Knauf do Brasil certifica que<br>
         
          <strong>{{ $employer->name }}</strong>, concluiu o curso<br> 
     
     <strong>{{ $employer->agenda->curso->consumer_name }}</strong>.</p>
        <p><strong>Carga Horária:</strong> {{ $employer->agenda->curso->tipo->duracao }} hora(s)<br>
        <strong>Data:</strong> {{ Helper::ConverterBR($employer->agenda->date_ini, true) }}
        </p>
        </td>
		<td width="84">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">
			<img src="{{ asset('images/certificado/Rodape.png') }}" width="842" height="132" alt="Knauf do Brasil"></td>
	</tr>
</table>

</body>
</html>