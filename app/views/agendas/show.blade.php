@extends('layouts.base')

@section('content')

 <!-- confirmação de curso -->

 	<div id="cadastro">

		

		@include('layouts.notifications')

	

		<h2>{{$agenda->curso->consumer->name}}</h2>

		<ul>

			 <li>

				<span>Data</span>

				<span>{{ Helper::ConverterBR($agenda->date_ini, true) }}</span>

			</li> 

			<li>

				<span>Horário</span>

				<span>Das {{$agenda->periodo->hour_ini}}h às {{$agenda->periodo->hour_ini+$agenda->curso->tipo->duracao}}h</span>

			</li>

			{{--<li>

				<span>Especialidades</span>

				<span>{{ $agenda->curso->atividade->name }}</span>

			</li>--}}

			<li>

				<span class="last">Instrutor(a)</span>

				<span>{{ $agenda->curso->instructor->name }}</span>

			</li>
					<li>
				<span class="last">Instrutor(a) Email</span>
				<span>{{ $agenda->curso->instructor->user->email }}</span>
			</li>
				<li>
				<span class="last">Instrutor(a) Telefone(s)</span>
				<span>{{ $agenda->curso->instructor->user->tel }} | {{ $agenda->curso->instructor->user->cel }}</span>
			</li>

		</ul>

		

		<div class="blc">

			<h3>Local onde será realizado o curso</h3>

		</div>

		

		{{ Form::open(['route'=>'agenda/update', 'method'=>'post', 'id'=>'frmCreateCurso']) }}

		{{ Form::input('hidden', 'id', $agenda->id) }}

		<div class="blc">

			

			<span>

				<p>{{ Form::text('endereco',$agenda->endereco,['placeholder'=>'Endereço do local onde o curso será realizado', 'class'=>'large']) }}</p>

				<p class="error">{{ $errors->first('endereco') }}</p>

			</span>	

		</div>

		<div class="blc">

			

			<span>

				<p>{{ Form::text('cep',$agenda->cep,['placeholder'=>'CEP','id'=>'cep']) }}</p>

				<p class="error">{{ $errors->first('cep') }}</p>

			</span>	

		

			

			<span>

				<p>{{ Form::email('email',$agenda->email,['placeholder'=>'E-mail']) }}</p>

				<p class="error">{{ $errors->first('email') }}</p>

			</span>	

		</div>

		<div class="blc">

			

			<span>

				<p>{{ Form::text('tel',$agenda->tel,['placeholder'=>'Telefone fixo (ddd+número)','id'=>'tel']) }}</p>

				<p class="error">{{ $errors->first('tel') }}</p>

			</span>	

		

			<span>

				<p>{{ Form::text('cel',$agenda->cel,['placeholder'=>'Celular (ddd+número)','id'=>'cel']) }}</p>

				<p class="error">{{ $errors->first('cel') }}</p>

			</span>	

		</div>



		

		<p class="sub mrg-btt-10">Dados dos participantes. Neste curso estão participando <em class="numEmp">{{$agenda->employers->count()}}</em> inscritos.</p>

		

		<div id="addinput">

			

			<?php $i=0; ?>

				@forelse($agenda->employers as $e) <?php $i++; ?>

					<div class="item">

						<input type="hidden" name="cid[{{$i}}]" value="{{$e->id}}">

					<div class="blc">



						<span>

							<p>{{ Form::text('name['.$i.']', $e->name,['placeholder'=>'Nome', 'class'=>'required']) }}</p>

							<p class="error">{{ $errors->first('name') }}</p>

						</span>

						<span>

							<p>{{ Form::text('email_convidado['.$i.']', $e->email, ['placeholder'=>'E-mail', 'class'=>'required']) }}</p>

							<p class="error">{{ $errors->first('cpf') }}</p>

						</span>	

						<a class="bt bt_x deleteEmployerBt" name="{{$e->id}}"></a>

						<div class="wrap_loader"></div>

					</div>

					<div class="blc">

						<span>

							<p>{{Form::select('profissao['.$i.']', $arr_profissoes, $e->profissao,['class'=>'required','readonly'=>'readonly'])}}</p>

							<p class="error">{{ $errors->first('profissao') }}</p>

						</span>	

						<span style="display:none;" id="informe">

							<p>{{ Form::text('informe','',['placeholder'=>'Caso não tenha sua profissão listada ao lado']) }}</p>

							<p class="error">{{ $errors->first('informe') }}</p>

						</span>	

					</div>

					<hr class="left">

					</div>
				@empty
					<p class="ft12">Ainda não tem participantes confirmados até o momento</p>
				@endforelse

			

		</div>

		

		<div style=" background:#fff; line-height:25px; border:1px dashed #bbbbbb; padding:20px; box-sizing:border-box; margin-top:20px; float:left;" class="blc"> <!--<a class="bt orange add-part">v</a>-->
			<p>Para adicionar participantes, basta copiar a url abaixo e publicar para seus participantes.</p>
			<span style="color:#bbbbbb; font-size:14px;">{{ $agenda->shareUrl('Ainda não temos link, aguarde confirmação do instrutor') }}</span>
		</div>

		<div class="alert alert-warning">
			<p></p>
		</div>

		<div class="blc"> <p class="ft12">*Todos os campos são obrigatórios</p> </div>

		<div style="display:none;" class="blc alteraction"> <p class="ft16 ft_orange"><strong>Atenção: você alterou o endereço do local onde será realizado o curso.</strong></p> </div>

		<div class="blc ft14"> <input type="checkbox" checked="checked" name="checkIn" value=""> <em>Aceitar os <a href="#terms" id="lookTerm" class="txt_underline">termos de uso</a></em> 

			<p style="margin-top:6px;color: red;font-size: 11px;"class="checkError error"></p>

		</div>

		<div class="blc">

			{{ Form::submit('Editar participação',['class'=>'bt blue bt_large']) }}

			<span style="float:none; margin-left:20px;" class="wrap_loader"></span>

		</div>

		{{ Form::close() }}

	</div>

	@include('layouts.modals.terms')

	{{--<script type="text/javascript">

		$(document).ready(function(){

			var addDiv = $('#addinput');

			

			var i = $('#addinput .item').size() + 1;



			$('.add-part').click(function() {

				if(i <= {{ $agenda->curso->max_quorum  }}){



				$('#addinput').append('<div class="item"><input type="hidden" name="cid['+i+']" value="0"><div class="blc"><span><p><input placeholder="Nome" class="required" name="name['+i+']" type="text" value=""></p><p class="error"></p></span><span><p><input placeholder="E-mail" class="required email" name="email_convidado['+i+']" type="text" value=""></p><p class="error"></p> </span> <a href="javaScript:;" class="bt bt_x delete"></a></div><div class="blc"><span><p><select class="required" name="profissao['+i+']">@foreach($arr_profissoes as $k => $v) <option value="{{$k}}">{{$v}}</option> @endforeach</select></p><p class="error"></p></span><span style="display:none;" id="informe"><p><input placeholder="Caso não tenha sua profissão listada ao lado" name="informe" type="text" value=""></p><p class="error"></p></span></div><hr class="left"></div>');

			

				i++;

				}

				return false;

				});



			$('#addinput').on('click','.delete', function() {

	

				if( i > {{ $agenda->curso->min_quorum }}) {

				$(this).parent().parent('.item').remove();

				i--;

			}

			return false;

			});

		});

	</script>--}}

@stop