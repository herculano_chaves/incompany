{{ HTML::style('css/style.css') }}

	<div class="row">
		<div class="col-md-12 text-center">
			<h2 style="font-size: 21px; margin-bottom: 25px;">Deseja confirmar este agendamento?</h2>
			<a data-action="y" class="bt blue bt_confirmation" href="{{ route('agenda/confirmar/post',['id'=>$agenda->id, 'action'=>'Y']) }}">Sim</a>

			<a data-action="n" class="bt orange bt_confirmation" href="javaScript:;">Não</a>
			{{ Form::open([ 'route'=>array('agenda/confirmar/post', 'id'=>$agenda->id, 'action'=>'N'), 'method'=>'get', 'id'=>'frmNegativeConfirm' ]) }}
				<div class="row">
					<div class="col-md-12">
						{{ Form::textarea('justify', null, ['placeholder'=>'Insira aqui sua justificativa','style'=>'border:1px solid #bebebe;']) }}
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						{{ Form::submit('Enviar', ['class'=>'bt blue', 'style'=>'float: right;  margin-right: 12%;']) }}
					</div>
				</div>
			{{ Form::close() }}
		</div>
	</div>

<style type="text/css">
	#frmNegativeConfirm{display: none;}
</style>
{{-- HTML::script('js/jquery.js') --}}
{{ HTML::script('js/jquery.validate.js') }}
<script>
	$(document).ready(function(){
		$('.bt_confirmation').click(function(){
			var act = $(this).data('action');

			if(act == 'n'){
				$('#frmNegativeConfirm').fadeIn();
			}

			return;
		});

		$('#frmNegativeConfirm').validate({
			rules:{
				justify:{required:true}
			}
		});

	});
</script>