@extends('layouts.backend')
@section('content')
	@include('layouts.backendmenu')
	<div class="content">
		<div class="panel">
			<div class="panel-heading">
				<strong>Nomes de cursos e eventos ({{ $consumer->name }})</strong>

			</div>
			<div class="panel-body">
				@include('layouts.notifications-list')
				{{ Form::open(['route'=>array('admin.eventos.update', $consumer->id), 'method'=>'put']) }}
				
				<table class="table table-striped table-bordered">
		          <thead>
	                  <tr>
	                    <th colspan="4">Editar</th>
	                   
	                   </tr>
	               </thead>
	               <tbody>
	               	<tr>
	               		<td>Nome</td>
	               		<td>{{ Form::text('name', $consumer->name, ['class'=>'form-control']) }}</td>
	               	</tr>
	               	 <tr>
	               		<td>Resumo</td>
	               		<td>{{ Form::textarea('excerpt', $consumer->excerpt, ['class'=>'form-control']) }}</td>
	               	</tr>

	               	 <tr>
	               		<td>Descrição</td>
	               		<td>{{ Form::textarea('description', $consumer->description, ['class'=>'form-control']) }}</td>
	               	</tr>
	               	<tr>
	               		<td colspan="2" style="text-align:right;">{{ Form::submit('Enviar', ['class'=>'btn btn-primary']) }}</td>
	               	</tr>
	               </tbody>
	            </table>
	            {{ Form::close() }}
	            {{ link_to_route('admin.eventos.index','< Voltar') }}
			</div>	

@stop