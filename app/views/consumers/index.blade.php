 
 @extends('layouts.backend')

@section('content')
	@include('layouts.backendmenu')
	<div class="content">
		@include('layouts.notifications')
		<div class="panel">
			<div style="overflow:hidden;" class="panel-heading">
				<strong>Nome de cursos e eventos</strong>
				<a class="btn btn-primary pull-right" href="{{ route('admin.eventos.create') }}"><i class="fa fa-plus"></i> Novo</a>
			</div>
			
			<div class="panel-body">
		
				<table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Nome do curso</th>
                            <th>Resumo</th>
                            <th>Descrição</th>

	                         <th>Editar</th>
	                         <th>Apagar</th>
                          	
                            
                        </tr>
                    </thead>
                    <tbody>
						@forelse($consumers as $c)
							  <tr>
								<td valign="middle">{{$c->name}}</td>
								<td valign="middle">{{$c->excerpt}}</td>
								<td valign="middle">{{str_limit($c->description,100,'...')}}</td>
							   <td valign="middle"><a href="{{ route('admin.eventos.edit', $c->id) }}">Editar</a></td>
							   <td valign="middle">
							   		{{ Form::open(['route'=>array('admin.eventos.destroy',$c->id), 'method'=>'delete']) }}
										<button type="submit">Apagar</button>
							   		{{ Form::close() }}
							   </td>
							  </tr>
                    	
						@empty
						<tr><td colspan="8">Ainda não tem conteúdo cadastrado</td></tr>
  						@endforelse
                    </tbody>
                </table>
                {{ Form::close() }}
                 {{ $consumers->appends($_GET)->links() }}
                 {{link_to_route('admin.eventos.index', '< Voltar')}}
                
			</div>
		
	</div>
<div id="datepic" style="display:none;">
	<div id="datepicker"></div>
</div>
@stop
