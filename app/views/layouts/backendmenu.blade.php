<div class="app-aside">

	<nav>

		<ul class="app-aside-nav-list">
			<li><a href="{{URL::route('admin.eventos.index')}}">Nomes de cursos e eventos</a></li>
			<li><a href="{{URL::route('admin/cursos',['caracter'=>'1'])}}">Cursos</a></li>
			

			<li><a href="{{URL::route('admin/cursos',['caracter'=>'2'])}}">Palestras</a></li>
			<li><a href="{{URL::route('users/index')}}">Empresas</a></li>
			<li><a href="{{URL::route('instructors/index')}}">Instrutores</a></li>
			<li><a href="{{URL::route('atividades/index')}}">Atividades</a></li>
			<li><a href="{{URL::route('tipos/index')}}">Tipos</a></li>
			{{--<li><a href="{{URL::route('cidades/index')}}">Cidades</a></li>--}}
			<li><a href="{{URL::route('data/index')}}">Exportação de dados</a></li>
			<li><a href="{{URL::route('logs/index')}}">Logs</a></li>

		</ul>

	</nav>

</div>