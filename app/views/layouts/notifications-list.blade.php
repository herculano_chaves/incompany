@if($message = Session::get('success'))
	<div class="site_status">
		<p>{{ $message }}</p>
	</div>
@endif
@if($message = Session::get('error'))
	<div class="site_status">
		<p>{{ $message }}</p>
	</div>
@endif
@if($errors->all())
	<div class="site_status">
		<ul>
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif