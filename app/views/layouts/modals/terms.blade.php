<div style="display:none;">
<div id="terms" style="width:800px;">
	
	<h2>SITE KNAUF AKADEMIA – TERMOS DE USO</h2><br/>

		<p><strong>Condições gerais</strong></p><br/>

		<p><strong>1. Cadastro</strong></p>

		<p><strong>2. Conduta</strong></p>

		<p><strong>3. Licença dos seus conteúdos e propriedade intelectual</strong></p>

		<p><strong>4. Configuração e atualização do site</strong></p>

		<p><strong>5. Emissão de certificados</strong></p>

		<p><strong>6. Mais informações</strong></p><br/>

		<p><strong>Condições gerais</strong></p>

		<p>Ao visitar o website akademia-knauf.com.br (doravante denominado simplesmente site), que pertence à Knauf do Brasil Ltda., e utilizar seus recursos e funcionalidades, você expressa sua concordância com os termos e as condições da Política de Privacidade da Knauf do Brasil Ltda., publicados no endereço http://www.knauf.com.br e incorporados neste documento por referência. Se você não concordar com algum desses termos, não utilize o site academia-knauf.com.br. Os termos de uso constituem um acordo legal vinculativo entre você, como usuário, e a Knauf do Brasil Ltda.</p><br/>

		<p><strong>1. Cadastro</strong></p>

		<p>Para ter acesso às funcionalidades e aos recursos do site, você deve cadastrar-se, por meio do fornecimento de dados pessoais como nome, idade e endereço de e-mail, dentre outros solicitados no formulário específico para essa finalidade.É obrigatório ter 18 anos ou mais (ou autorização dos pais ou responsáveis legais) e plena capacidade civil para utilizar o site.</p><br/>

		<p><strong>2. Conduta</strong></p>

		<p>Os recursos disponíveis no site são destinados exclusivamente ao seu uso pessoal. É vedado, no uso do site</p><br/>
		
		<p>
			<ul>
				<li>• Disseminar conteúdo ilegal, ofensivo, violento ou que viole a privacidade e a intimidade de terceiros.</li>

				<li>• Violar propriedade intelectual de terceiros.</li>

				<li>• Disseminar vírus ou código de computador malicioso, arquivos ou programas desenvolvidos para coletar dados não autorizados, interromper, destruir ou limitar a funcionalidade do site, o próprio site, qualquer software ou hardware de computador ou equipamentos de telecomunicações em geral (malware ou spyware).</li>

				<li>• Interferir na arquitetura do site por qualquer forma, meio ou técnica.</li>

				<li>• Disfarçar ou ocultar o número de IP (internet protocol), que identifica sua conexão à internet.</li>

				<li>• Usar qualquer dos elementos do site para fins comerciais, publicitários ou qualquer outro que não represente sua finalidade principal.</li>

				<li>• Criar ou utilizar mais de uma única forma de identificação para utilizar o site.</li>

				<li>• Utilizar identificação de um terceiro ou permitir que terceiros utilizem a sua identificação.</li>
			</ul>
		</p><br/>

		<p>A Knauf do Brasil se reserva o direito de impedir que você utilize o site e seus recursos em caso de violação de quaisquer das condições destes termos de uso. A suspensão de uso do site será comunicada imediatamente a você.</p><br/>

		<p><strong>3. Licença dos seus conteúdos e propriedade intelectual</strong></p>

		<p>Ao enviar qualquer conteúdo ou informação para o site, na forma de texto, imagem ou áudio, você declara autorizar, de forma gratuita, não exclusiva e livre de remuneração, compensação ou indenização, o uso do conteúdo pela Knauf do Brasil Ltda., por qualquer modalidade e suporte, por tempo indeterminado.</p> 

		<p><strong>Caso não concorde com a autorização acima ou não tenha os direitos necessários para emitir essa autorização com relação a um conteúdo, você não deve enviar o conteúdo para o site.</strong></p>

		<p>Você só deve enviar para ou pelo site materiais originais seus. Todos os direitos autorais patrimoniais sobre o material submetido por você continuam sendo seus. A Knauf do Brasil não tem qualquer obrigação de uso dos seus conteúdos e nem perderá o direito de usá-los, ainda que não venha a exercer esse direito.</p><br/>

		<p><strong>4. Configuração e atualização do site</strong></p>

		<p>A Knauf do Brasil se reserva o direito de modificar a qualquer momento a apresentação, a configuração e a disponibilização do site.</p><br/>

		<p><strong>5. Emissão de certificados</strong></p>

		<p>O participante que concluir qualquer um dos cursos oferecidos pela Akademia Knauf terá direito ao respectivo certificado. Este será enviado ao e-mail de cada participante 5 (cinco) dias úteis após a conclusão do curso, de acordo com as informações registradas em “Cursos concluídos”, na área “Minha agenda” do site e estará disponível nos sistemas da Knauf do Brasil por até 3 (três) meses após a conclusão do curso.</p><br/>

		<p><strong>6. Mais informações</strong></p>

		<p>Para esclarecimento de dúvidas ou solução de problemas relacionados ao site, entre em contato conosco pelo email <a href="mailto:sak@knauf.com.br;">sak@knauf.com.br</a>.</p>

</div>
</div>