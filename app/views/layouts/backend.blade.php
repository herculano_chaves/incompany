<!doctype html>
<html lang="en">
<head>
	@section('homemeta')
	<link rel="shortcut icon" href="{{ asset('assets/icon/favicon.ico') }}">
	<link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic" rel="stylesheet" type="text/css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
		{{ HTML::style('css/admin/font-awesome.min.css') }}
		{{ HTML::style('css/admin/reset.css') }}
		{{ HTML::style('css/admin/icons.css') }}
		{{ HTML::style('css/admin/screen.css') }}
		{{ HTML::style('css/responsive.css') }}
		{{ HTML::style('css/admin/colorpicker.css') }}
		{{ HTML::style('css/datepicker.css') }} 
		{{ HTML::style('css/modal.css') }}
		<!--[if gte IE 8]>
		<link rel="stylesheet" href="css/cross-browser-ie.css">
		<![endif]-->	
		{{ HTML::script('js/jquery.js') }}
		{{ HTML::script('js/jquery.ui.js') }}
		{{ HTML::script('js/bootstrap.min.js') }}
		{{-- HTML::script('js/admin/jquery.mask.js') --}}
		{{ HTML::script('js/admin/jquery.validate.js') }}
		{{ HTML::script('js/jquery-multipledatepicker.js') }}
		{{ HTML::script('js/jquery.pg.modal.js') }}
   
		{{ HTML::script('js/admin/jquery.scripts.js') }}
		<title>Knauf In Company - Administrador</title>
	@show
</head>
<body>
	@if(Sentry::check())
		<div class="app-header">
			<div class="app-header-logo text-center" style="background:#84a6b8;height: 60px;padding-top: 19px;">
				<img src="{{URL::to('/')}}/images/logo_incompany.png" width="163" alt="">
			</div>
			<div class="app-header-userinfo">{{ Sentry::getUser()->name }}</div>
			<div class="app-header-logout"><a href="{{URL::route('user/logout')}}">Logout</a></div>
		</div>
	@endif	
	@yield('content')
		
</body>
</html>
