<!doctype html>
<html lang="en">
<head>
	@section('homemeta')
	<link rel="shortcut icon" href="{{ asset('assets/icon/favicon.ico') }}">
		{{-- HTML::style('css/font.css') --}}
		{{ HTML::style('css/style.css') }}
		{{ HTML::style('css/datepicker.css') }}
		{{ HTML::style('css/reveal.css') }}
		{{ HTML::style('css/font-awesome.min.css') }}
		<!--[if gte IE 8]>
		<link rel="stylesheet" href="css/cross-browser-ie.css">
		<![endif]-->	
		{{ HTML::script('js/jquery.js') }}
		{{ HTML::script('js/jquery.ui.js') }}
		{{ HTML::script('js/jquery.mask.js') }}
		{{ HTML::script('js/jquery.validate.js') }}
		{{ HTML::script('js/jquery.validate.util.js') }}
	 	{{-- HTML::script('js/jquery.pollyfill.js') --}}
		{{ HTML::script('js/jquery.modal.js') }}
		{{ HTML::script('js/jquery-multipledatepicker.js') }}

		
		<title>Knauf In Company</title>
	@show
	<script>
	
	   
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-55466439-1', 'auto');
	  ga('send', 'pageview');
	</script>
</head>
<body>
	<div id="wrap">
		<section id="header">
			<div class="top_header"></div>	
			<div id="mid_header">
				<div class="main">
					<a href="{{URL::to('/')}}"><h2 title="Knauf Akademia"></h2></a>
					<h1 title="Knauf In Company"><a href="{{URL::to('/')}}"><img src="{{ URL::to('/') }}/images/logo_incompany.png" alt=""></a></h1>
					<div id="login">
						@if(Sentry::check())
							<p class="white ft14 b">Olá, {{Sentry::getUser()->name}}</p>
							{{HTML::linkRoute('user/account/index','Meus dados',null, ['class'=>'ft11 u white'])}} <span class="ft11 white">|</span> {{ HTML::linkRoute('user/logout','Sair',null, ['class'=>'ft11 u white']) }}
						@else
							<p>{{HTML::link('login', 'Entre',['class'=>'bt orange bt_large bt_login'])}}</p>
							<p class="white b i ft12">ou {{HTML::link('cadastro', 'Cadastre-se', ['class'=>'white b bt_cadastre'])}}</p>
						@endif
					</div>
				</div>
			</div>
			<nav id="main_menu">
				<ul>
					<li><a href="{{URL::to('/')}}" class="{{ URL::to('/') === URL::current() ? 'active' : '' }}">Home</a></li>
					<li><a href="{{URL::route('cursos/index')}}" class="{{ URL::route('cursos/index') === URL::current() ? 'active' : '' }}">Cursos In Company</a></li>
					<li><a href="{{URL::route('site/contact')}}" class="{{ URL::route('site/contact') === URL::current() ? 'active' : '' }}">Fale Conosco</a></li>
					@if(Sentry::check())
						<li><a href="{{URL::route('user/account/agenda')}}" class="mainItem {{ URL::to('usuario/agenda') === URL::current() ? 'active' : '' }}">Minha agenda</a></li>
					@endif
				</ul>
			</nav>
		</section>
		<section id="content">
			<div class="main">
				@yield('content')
			</div>
		</section>
		<section id="footer">
			<div class="main subfooter">
				<a href="{{ URL::to('http://knauf.com.br') }}" target="_blank"><img src="{{ URL::to('/') }}/images/logo_knauf.png" alt="Knauf Drywall"></a>
			</div>
			<div class="bottom_footer">
				<div class="main">
					<p>{{HTML::link('http://dizain.com.br','Dizain',['target'=>'_blank'])}}</p>
				</div>
			</div>
		</section>
	</div>

	{{ HTML::script('https://jamesallardice.github.io/Placeholders.js/assets/js/placeholders.jquery.min.js') }}
	{{ HTML::script('js/scripts.js') }}
</body>
</html>
