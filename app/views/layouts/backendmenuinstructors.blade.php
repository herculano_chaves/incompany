<div class="app-aside">
	<nav>
		<ul class="app-aside-nav-list">

			{{--<li><a href="{{URL::route('admin/cursos/instrutor',['caracter_id'=>1])}}">Meus cursos</a></li>
			<li><a href="{{URL::route('admin/cursos/instrutor', ['caracter_id'=>2])}}">Minhas palestras</a></li>--}}
			<li><a href="{{ route('admin.instructors.calendar') }}"><i class="fa fa-calendar-o"></i> Calendário</a></li>
			<li><a href="{{URL::route('admin/instructors')}}"><i class="fa fa-sticky-note-o"></i> Agendas</a></li>
			<li><a href="{{ route('instructor.events') }}"><i class="fa fa-list"></i> Eventos</a></li>
			
			<li><a href="{{ route('instructor.locale.index') }}"><i class="fa fa-map-pin"></i> Cidades</a></li>
			<li><a href="{{URL::route('instructor/edit/instructor', ['id'=>Sentry::getUser()->id,'default'=>'1'])}}"><i class="fa fa-user"></i> Dados pessoais</a></li>
			
		</ul>
	</nav>
</div>