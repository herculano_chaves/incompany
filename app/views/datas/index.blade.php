@section('content')
		@include('layouts.backendmenu')
	<div class="content">
		<div class="panel">
			<div class="panel-heading">
				<strong>Exportação de dados</strong>

			</div>
			<div class="panel-body">
				@include('layouts.notifications-list')
				<table class="table table-striped table-bordered">
		          <thead>
	                  <tr>
	                    <th>Dado</th>
	                    <th>Download</th>
	                   </tr>
	               </thead>
	               <tbody>
	               	<tr>
	               		<td>Participantes</td>
	               		<td>{{ link_to_route('data/employers','',null,['class'=>'bt bt_excel']) }}</td>
	               	</tr>
	               	<tr>
	               		
	               	</tr>
	               </tbody>
	            </table>
			</div>	
@stop