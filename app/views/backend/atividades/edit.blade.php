@extends('layouts.backend')
@section('content')
	@include('layouts.backendmenu')
	<div class="content">
		<div class="panel">
			<div class="panel-heading">
				<strong>Atividades</strong>

			</div>
			<div class="panel-body">
				@include('layouts.notifications-list')
				{{ Form::open(['action'=>'atividade/update', 'method'=>'post']) }}
				{{ Form::hidden('id', $atividade->id) }}
				<table class="table table-striped table-bordered">
		          <thead>
	                  <tr>
	                    <th colspan="2">Editar Atividade</th>
	                   </tr>
	               </thead>
	               <tbody>
	               	<tr>
	               		<td>{{ Form::label('name', 'Atividade:') }}</td><td>{{ Form::text('name',$atividade->name) }}</td>
	               	</tr>
	               	<tr>
	               		<td colspan="2">{{ Form::submit('Enviar') }}</td>
	               	</tr>
	               </tbody>
	            </table>
	            {{ Form::close() }}
	            {{ link_to_route('atividades/index','< Voltar para Atividades') }}
			</div>	

@stop