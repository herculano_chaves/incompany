@extends('layouts.backend')
@section('content')
	@include('layouts.backendmenu')
	<div class="content">
		<div class="panel">
			<div class="panel-heading">
				<strong>Atividades</strong>

			</div>
			<div class="panel-body">
				@include('layouts.notifications-list')
				{{ Form::open(['action'=>'atividade/create', 'method'=>'post']) }}
				<table class="table table-striped table-bordered">
		          <thead>
	                  <tr>
	                    <th colspan="2">Inserir Atividade</th>
	                   </tr>
	               </thead>
	               <tbody>
	               	<tr>
	               		<td>{{ Form::label('name', 'Atividade:') }}</td><td>{{ Form::text('name') }}</td>
	               	</tr>
	               	<tr>
	               		<td colspan="2">{{ Form::submit('Enviar') }}</td>
	               	</tr>
	               </tbody>
	            </table>
	            {{ Form::close() }}
	            <table class="table table-striped table-bordered">
		          <thead>
	                  <tr>
	                    <th>Inserir</th>
	                    <th>Editar</th>
	                    <th>Apagar</th>
	                   </tr>
	               </thead>
	               <tbody>
	               	@foreach($atividades as $a)
	               	<tr>
	               		<td>{{ $a->name }}</td>
	               		<td>{{ link_to_route('atividade/edit','Editar',['id'=>$a->id]) }}</td>
	               		<td>{{ link_to_route('atividade/delete','Apagar',['id'=>$a->id]) }}</td>
	               	</tr>
	               	@endforeach
	               </tbody>
	            </table>
			</div>	

@stop