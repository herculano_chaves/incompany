@extends('layouts.backend')



@section('content')

	@include('layouts.backendmenu')

	<div class="content">
		<div class="panel-body">	

			<h2>Incluir 
				@if($caracter_id == '1') 
					novo curso 
				@else 
					nova palestra 
				@endif
			</h2>

			@include('layouts.notifications-list')

			{{ Form::open(['route'=>'curso/generate', 'id'=>'frmAddCurso']) }}
			{{ Form::input('hidden', 'caracter', $caracter_id) }}
			<table class="table table-striped table-bordered">

                <thead>

                    <tr>

                        <th colspan="2">Inserir</th>

            				

                    </tr>

                </thead>

                <tbody>

      

                		<tr>
                            <td colspan="2">{{ Form::select('consumer_id', [''=>'Selecione nome do curso']+$consumers) }}</td>      
                        </tr>
                		<tr>
                			@if($caracter_id == '1') 
                			<td>{{Form::text('min_quorum',null,['placeholder'=>'Mínimo participantes'])}}</td>
                			@else
                			<td>{{Form::text('min_quorum',30,['placeholder'=>'Mínimo participantes','readonly'=>'readonly'])}}</td>
                			@endif

                			<td>{{Form::text('max_quorum',null,['placeholder'=>'Máximo participantes'])}}</td>

                		</tr>

                		<tr>
                            
                            <td><select name="tipo_id">
                                <option value="">Tipos</option>
                                @foreach($tipos as $tipo)
                                    <option value="{{$tipo->id}}">{{$tipo->name}}</option>
                                @endforeach
                            </select></td>
                			<td>{{ Form::select('atividade_id[]', $atividades,null, ['multiple']) }}</td>

                		</tr>

                		<tr>

                			<td>{{ Form::select('instructor_id', [''=>'Instrutores']+$instrutores, null) }}</td>

                			

                		</tr>

                		<tr>

                			<td colspan="2">{{ Form::submit('Inserir') }}</td>

                		</tr>

                	</tr>

                </tbody>

            </table>

            {{ Form::close() }}

		</div>
	</div>
@stop