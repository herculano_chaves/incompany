 
 @extends('layouts.backend')

@section('content')
	@include('layouts.backendmenu')
	<div class="content">
		@include('layouts.notifications')
		<div class="panel">
			<div class="panel-heading">
				<strong>Cursos de ({{ $instructor->user->name }})</strong>
			</div>
			
			<div class="panel-body">
			
				<div style="float:left">
					<strong>Filtrar</strong>
					{{ Form::open(['route'=>array('filter/instructor/courses', 'id'=>$instructor->user->id, 'caracter_id'=> Route::input('caracter_id') ), 'method'=>'get']) }}
					{{ Form::label('cidade_id', 'Por cidade:') }}
					{{ Form::select('cidade_id', [''=>'Selecione']+$cidades, Input::get('cidade_id')) }}
					{{ Form::submit('Filtrar') }}
					{{ Form::close() }}
				</div>
				<div style="float:right">

				{{ Form::open(['route'=>'cursos/action']) }}
				{{ Form::label('action', 'Com os selecionados') }}
				{{ Form::select('action', [''=>'Escolha sua ação','1'=>'Alterar Disponibilidade']) }}
				{{ Form::submit('Fazer') }}
				</div>
				<table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Nome do curso</th>
                            <th>Status Agendas</th>
                              <th>Atividade</th>

                            <th>Cidade</th> 
                           
                           
                           
           
                            <th>Min Part.</th>
                            <th>Max Part.</th>
                           
                       

                          	<th>Disponibilidade</th>
                          	<th>Selecionar todos <br> <input type="checkbox" class="selec_all"></th>
                          	
                            
                        </tr>
                    </thead>
                    <tbody>
						@forelse($cursos as $c)
							  <tr>
							    <td valign="middle">{{$c->name}}</td>
							    <td valign="middle">
							    	@if($c->agendas->count() < 1 )
							    		Agenda não preenchida até o momento
							    	@endif 
							    	@foreach($c->agendas as $a)
							    		<p><strong>{{ $a->statusText() }}</strong> | {{Helper::ConverterBR($a->date_ini, true)}} às {{$a->periodo->hour_ini}}h - {{ $a->endereco }}</p>
							    	@endforeach
							    </td>
							    <td valign="middle">
							    	
							    </td>
							    <td valign="middle">
							    	
							    </td>
							  
							    <td valign="middle">{{$c->min_quorum}}</td>
							    <td valign="middle">{{$c->max_quorum}}</td>
							   
							    <td valign="middle"><a href="{{route('instructor/disponibilidade',['id'=>$c->id])}}">Alterar Disponibilidade</a></td>

							    <td valign="middle"> <input type="checkbox" name="cursos[]" class="selec_cursos" value="{{$c->id}}"> </td>
							   
							  </tr>
                    	
						@empty
						<tr><td colspan="8">Ainda não tem curso cadastrado</td></tr>
  						@endforelse
                    </tbody>
                </table>
                {{ Form::close() }}
                 {{ $cursos->appends($_GET)->links() }}
                 {{link_to_route('instructors/index', '< Voltar')}}
                
			</div>
		
	</div>
<div id="datepic" style="display:none;">
	<div id="datepicker"></div>
</div>
@stop
