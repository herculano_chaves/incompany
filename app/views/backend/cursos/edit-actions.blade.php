@extends('layouts.backend')

@section('content')

	@include('layouts.backendmenu')

	<div class="content">

		<div class="panel">

			<div class="panel-heading" style="overflow:hidden;">
				<strong>Alterar ações dos cursos selecionados</strong>
			</div>
			<div class="panel-body">
				<h2>Eventos</h2>
				{{ Form::open(['route'=>array('cursos.update.actions',$caracter_id)]) }}
				{{ Form::hidden('checkpoint',$checkpoint) }}
				<ul>
					@foreach($cursos as $c)

						<li class="alert">{{ $c->consumer_name }}{{ Form::hidden('curso_ids[]', $c->id) }}</li>
					@endforeach
				</ul>
				<h3>Ação de alteração</h3>
				{{ $form_action }}
				{{ Form::button('Alterar', ['type'=>'submit','class'=>'btn btn-primary']) }}
				{{ Form::close() }}
			</div>
			<a href="javaScript:history.back(1);">Voltar</a>
		</div>
	</div>
