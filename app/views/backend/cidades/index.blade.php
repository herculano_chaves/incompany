@extends('layouts.backend')
@section('content')
	@include('layouts.backendmenu')
	<div class="content">
		<div class="panel">
			<div class="panel-heading">
				<strong>Cidades</strong>

			</div>
			<div class="panel-body">
				@include('layouts.notifications-list')
				{{ Form::open(['action'=>'cidade/create', 'method'=>'post']) }}
				<table class="table table-striped table-bordered">
		          <thead>
	                  <tr>
	                    <th colspan="2">Inserir cidade</th>
	                    <th colspan="2">Inserir Estado</th>
	                   </tr>
	               </thead>
	               <tbody>
	               	<tr>
	               		<td>{{ Form::label('name', 'Cidade:') }}</td><td>{{ Form::text('name') }}</td>
	               	
	               		<td>{{ Form::label('name', 'Estado:') }}</td>
	               		<td>
		               		<select name="estado_id"><option>Estados</option>
		               			@foreach($estados as $e) <option value="{{ $e->id }}">{{ $e->uf }}</option> @endforeach
		               		</select>
	               		</td>
	               	</tr>
	               	<tr>
	               		<td colspan="4">{{ Form::submit('Enviar') }}</td>
	               	</tr>
	               </tbody>
	            </table>
	            {{ Form::close() }}
	            <table class="table table-striped table-bordered">
		          <thead>
	                  <tr>
	                    <th>Nome</th>
	                    <th>Estado</th>
	                    <th>Editar</th>
	                    <th>Apagar</th>
	                   </tr>
	               </thead>
	               <tbody>
	               	@foreach($cidades as $a)
	               	<tr>
	               		<td>{{ $a->name }}</td>
	               		<td>{{ $a->estado->uf }}</td>
	               		<td>{{ link_to_route('cidade/edit','Editar',['id'=>$a->id]) }}</td>
	               		<td>{{ link_to_route('cidade/delete','Apagar',['id'=>$a->id]) }}</td>
	               	</tr>
	               	@endforeach
	               </tbody>
	            </table>
			</div>	

@stop