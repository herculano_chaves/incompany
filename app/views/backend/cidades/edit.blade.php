@extends('layouts.backend')
@section('content')
	@include('layouts.backendmenu')
	<div class="content">
		<div class="panel">
			<div class="panel-heading">
				<strong>Cidades</strong>

			</div>
			<div class="panel-body">
				@include('layouts.notifications-list')
				{{ Form::open(['action'=>'cidade/update', 'method'=>'post']) }}
				{{ Form::hidden('id', $cidade->id) }}
				<table class="table table-striped table-bordered">
		          <thead>
	                  <tr>
	                    <th colspan="4">Editar cidade</th>
	                   </tr>
	               </thead>
	               <tbody>
	               	<tr>
	               		<td>{{ Form::label('name', 'Cidade:') }}</td><td>{{ Form::text('name',$cidade->name) }}</td>
	               		<td>{{ Form::label('name', 'Estado:') }}</td>
	               		<td>
	               			<select name="estado_id"><option>Estados</option>
		               			@foreach($estados as $e) 
		               				@if($e->id == $cidade->estado->id)
		               					<option selected="selected" value="{{ $e->id }}">{{ $e->uf }}</option> 
		               				@else
										<option value="{{ $e->id }}">{{ $e->uf }}</option> 
		               				@endif
		               				
		               			@endforeach
		               		</select>
		               		
	               		</td>
	               	</tr>
	               	<tr>
	               		<td colspan="4">{{ Form::submit('Enviar') }}</td>
	               	</tr>
	               </tbody>
	            </table>
	            {{ Form::close() }}
	            {{ link_to_route('cidades/index','< Voltar para cidades') }}
			</div>	

@stop