@extends('layouts.admin')

@section('content')
	@include('layouts.notifications')
	{{ Form::open(['route'=>'admin/login', 'method'=>'post']) }}
	<p>
		{{ Form::label('email', 'Email: ') }}
		{{ Form::email('email') }}
	</p>
	<p>
		{{ Form::label('password', 'Senha: ') }}
		{{ Form::password('password') }}
	</p>
	<p>
		{{ Form::submit('Logar') }}
	</p>
	<p>{{ link_to_route('password.remind','Esqueci minha senha') }}</p>
	{{ Form::close() }}
@stop
