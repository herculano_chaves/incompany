@extends('layouts.backend')
@section('content')
	@include('layouts.backendmenu')
	<div class="content">
		<div class="panel">
			<div class="panel-heading">
				<h2>Tipos</h2>

			</div>
			<div class="panel-body">
				@include('layouts.notifications-list')
				{{ Form::open(['action'=>'tipo/update', 'method'=>'post']) }}
				{{ Form::hidden('id', $tipo->id) }}
				<table class="table table-striped table-bordered">
		          <thead>
	                  <tr>
	                    <th colspan="2">Editar Tipo</th>
	                   </tr>
	               </thead>
	               <tbody>
	               	<tr>
	               		<td>{{ Form::label('name', 'Tipo:') }}</td><td>{{ Form::text('name', $tipo->name, ['style'=>'width:400px;']) }}</td>
	               	</tr>
	               	<tr>
	               		<td>{{ Form::label('caracter_id', 'Selecione Caracter:') }}</td>
	               		<td>{{ Form::select('caracter_id', [''=>'Selecione Caracter']+$caracters, $tipo->caracter->id) }}</td>
	               	</tr>
	               	 <tr>
	               		<td>{{ Form::label('duracao', 'Duração (em horas):') }}</td><td>{{ Form::text('duracao', $tipo->duracao, ['style'=>'width:400px;']) }}</td>
	               	</tr>
	               	<tr>
	               		<td colspan="2">{{ Form::submit('Enviar') }}</td>
	               	</tr>
	               </tbody>
	            </table>
	            {{ Form::close() }}
	           {{ link_to_route('tipos/index','< Voltar para Tipos') }}
			</div>	

@stop