@extends('layouts.backend')
@section('content')
	@include('layouts.backendmenu')
	<div class="content">
		<div class="panel">
			<div class="panel-heading">
				<h2>Tipos</h2>

			</div>
			<div class="panel-body">
				@include('layouts.notifications-list')
				{{ Form::open(['action'=>'tipo/create', 'method'=>'post']) }}
				<table class="table table-striped table-bordered">
		          <thead>
	                  <tr>
	                    <th colspan="3">Inserir Tipo</th>
	                   </tr>
	               </thead>
	               <tbody>
	               	<tr>
	               		<td>{{ Form::label('name', 'Tipo:') }}</td><td>{{ Form::text('name') }}</td>

	               	</tr>
	               	<tr>
	               		<td>{{ Form::label('caracter_id', 'Selecione Caracter:') }}</td>
	               		<td>{{ Form::select('caracter_id', [''=>'Selecione Caracter']+$caracters) }}</td>
	               	</tr>
	               	<tr>
	               		<td>{{ Form::label('duracao', 'Duração em horas:') }}</td>
	               		<td>{{ Form::text('duracao') }}</td>
	               	</tr>
	               	<tr>
	               		<td colspan="3">{{ Form::submit('Enviar') }}</td>
	               	</tr>
	               </tbody>
	            </table>
	            {{ Form::close() }}
	            <table class="table table-striped table-bordered">
		          <thead>
	                  <tr>
	                    <th>Nome</th>
	                    <th>Duração (em horas)</th>
	                    <th>Editar</th>
	                    <th>Apagar</th>
	                   </tr>
	               </thead>
	               <tbody>
	               	@foreach($tipos as $a)
	               	<tr>
	               		<td>{{ $a->name }}</td>
	               		<td>{{ $a->duracao }}h</td>
	               		<td>{{ link_to_route('tipo/edit','Editar',['id'=>$a->id]) }}</td>
	               		<td>{{ link_to_route('tipo/delete','Apagar',['id'=>$a->id]) }}</td>
	               		
	               	</tr>
	               	@endforeach
	               </tbody>
	            </table>
			</div>	

@stop