@extends('layouts.backend')

@section('content')

	@include('layouts.backendmenu')

	<div class="content">

		<div class="panel">

			<div class="panel-heading" style="overflow:hidden;">

				<strong>
					@if($caracter_id == '1') 
					Cursos 
					@else 
					Palestras 
					@endif
				</strong>
				<small>({{ $cursos->getTotal() }})</small>
				<a class="pull-right btn btn-primary" href="{{ route('admin.cursos.create', $caracter_id) }}"><i class="fa fa-plus"></i> Novo</a>
			</div>

			


			<div class="panel-body">
				@include('layouts.notifications')
				<div class="search col-md-9" style="margin:20px 0;">
					{{ Form::open(['route' => ['admin/cursos', 'caracter_id'=>$caracter_id], 'method' => 'get']) }}

					{{ Form::label('cidade_id','Buscar por:') }}
					{{ Form::select('estado_id', [''=>'UF']+$estados, Input::get('estado_id'), ['style'=>'height:32px;','id'=>'estado_id']) }}
					{{ Form::select('cidade_id', [''=>'Cidades']+$cidades, Input::get('cidade_id'), ['style'=>'height:32px;','id'=>'cidade_id']) }}

					{{ Form::select('atividade', [''=>'Atividades']+$atividades, Input::get('atividade'), ['style'=>'height:32px;']) }}
					{{ Form::select('tipo', [''=>'Tipos']+$tipos->lists('name','id'), Input::get('tipo'), ['style'=>'height:32px;']) }}
					{{ Form::select('instructor_id', [''=>'Instrutores']+$instrutores, Input::get('instructor_id'), ['style'=>'height:32px;']) }}
					{{ Form::select('status', [''=>'Status','1'=>'Ativo', '0'=>'Inativo'], Input::get('status')) }}

					{{ Form::submit('Buscar', ['class'=>'btn btn-primary']) }}

					{{ Form::close() }} 
				</div>
				<div class="col-md-3">
					
					<span>Com os marcados: </span>
					{{ Form::select('action_select', [''=>'Selecionar ação','1'=>'Alterar Local','2'=>'Alterar Atividade','3'=>'Alterar Tipo', '4'=>'Alterar Instrutor','5'=>'Alterar Status','6'=>'Min.Max Participantes']) }}
					{{ Form::button('Fazer',['id'=>'btnEditActions']) }}

					
				</div>

				@if($cursos->count()>0)
				{{ Form::open(['route'=>array('cursos.edit.actions', 'caracter_id'=>$caracter_id), 'id'=>'frmEditActions', 'method'=>'get']) }}
				
				{{ Form::hidden('action') }}
				
				{{ Form::hidden('checkpoint_saved', Route::current()->getPath().'?estado_id='.Input::get('estado_id').'&cidade_id='.Input::get('cidade_id').'&atividade='.Input::get('atividade').'&tipo='.Input::get('tipo').'&instructor_id='.Input::get('instrutor_id').'&status='.Input::get('status').'&page='.$cursos->getCurrentPage()) }}
				<table class="table table-striped table-bordered">

                    <thead>

                        <tr>

                            <th>Nome</th>

                            <th>Atividade</th>

                            <th>Tipo</th>


                            <th>Instrutor</th>

                            <th>Min Part.</th>

                            <th>Max Part.</th>
                            <th>Status</th>
                            <th>Sel. {{ Form::checkbox('selec_all') }}</th>

                            <th>Editar</th>

                            <th>Apagar</th>

                        </tr>

                    </thead>
					
                    <tbody>
					
                        @foreach($cursos as $c)
                    
							  <tr>
							  	

							    <td valign="middle">{{$c->consumer->name}}</td>

							    <td valign="middle">
							    	
							    	@forelse($c->atividades as $atividade)
										{{ $atividade->name}}
							    	@empty
							    	N/A
							    	@endforelse
							    </td>

							    <td valign="middle">{{$c->tipo->name}}</td>

							    <td valign="middle">{{$c->instructor->name}}</td>

							    <td valign="middle">{{$c->min_quorum}}</td>

							    <td valign="middle">{{$c->max_quorum}}</td>

							    <td valign="middle">{{ $c->statusText() }}</td>

							    <td valign="middle">{{ Form::checkbox('curso_ids[]', $c->id, false, ['class'=>'curso_ids']) }}</td>

							    <td valign="middle"><a href="{{url('admin/curso/edit')}}/{{$c->id}}">Editar</a></td>

							    <td valign="middle"><a href="javaScript:;" class="clearCurso" data-link="{{url('admin/curso/destroy')}}/{{$caracter_id}}/{{$c->id}}">Apagar</a></td>
								
							  </tr>
							
  						@endforeach

                    </tbody>
					
                </table>
                {{ Form::close() }}
                {{$cursos->appends($_GET)->links()}}
               

                @else

                	Não foram encontrados cursos até o momento.

                @endif

			</div>

		

	</div>

@stop