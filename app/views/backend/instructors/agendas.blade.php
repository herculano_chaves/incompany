@extends('layouts.backend')



@section('content')

	@include('layouts.backendmenuinstructors')

	<div class="content">

		<div class="panel">

			@include('layouts.notifications')

			<div class="panel-heading">

				<strong>Agendas do curso {{ $curso->consumer->name }}</strong>

			</div>

			

			<div class="panel-body">

				<table class="table table-striped table-bordered">

                    <thead>

                        <tr>

                            <th>Data</th>

                            <th>Horário</th>

                            <th>Email resposável</th>

                            <th>Endereço</th>

                            <th>Telefone/Celular</th>

                          	<th>Ver alunos</th>
                          	<th>Status</th>

                        </tr>

                    </thead>

                    <tbody>

                        @foreach($curso->agendas as $agenda)

							  <tr>

							    <td valign="middle">{{ Helper::ConverterBR($agenda->date_ini, true) }}</td>

							    <td valign="middle">De {{ $agenda->periodo->hour_ini }}h às {{$agenda->periodo->hour_ini+$curso->tipo->duracao}}h</td>

							    <td valign="middle">{{ $agenda->email }}</td>

							    <td valign="middle">{{ $agenda->endereco }}</td>

							    <td valign="middle">{{ $agenda->tel }} / {{ $agenda->cel }}</td>

							    <td valign="middle"><a href="{{route('employers/show',['id'=>$agenda->id])}}">Alunos</a></td>

							    <td valign="middle">{{ $agenda->statusText(true) }}</td>

							  </tr>

  						@endforeach

                    </tbody>

                </table>

               {{ link_to_route('admin/instructors','< Voltar para cursos') }}

			</div>

		

	</div>

@stop