@extends('layouts.backend')

@section('content')
	{{$menu}}
	<div class="content">
		@include('layouts.notifications')
		<div class="panel minhas-agendas">
			<div class="panel-heading">
				<strong>Minhas agendas ({{ $instructor->user->name }})</strong>
			</div>

            <div class="row box-inline">
                <div class="col-md-12">
                    {{ Form::open(['route'=>array('admin/instructors/search',$status), 'method'=>'get','class'=>'form-inline']) }}
                    <span>Filtrar por:</span>
                    <select name="course_id" class="form-control" id="">
                        <option value="">Cursos</option>
                        @foreach(array_unique($agendas->lists('curso_id')) as $c)
                         
                        <option @if(Input::has('course_id')) selected @endif value="{{$c}}">{{ Curso::getNameById($c) }}</option>
                          
                        @endforeach
                    </select>
                    {{ Form::select('state_id', [''=>'Estados']+$states, Input::get('state_id'), ['class'=>'form-control','id'=>'estado_id']) }}
                    {{ Form::select('city_id', [''=>'Cidades']+$cities, Input::get('city_id'), ['class'=>'form-control','id'=>'cidade_id']) }}
                    <!--<div class="form-group">
                        <div class="form-icon">
                            <input type="text" class="form-control" placeholder="Data início">
                            <i class="fa fa-calendar-o"></i>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-icon">
                            <input type="text" class="form-control" placeholder="Data fim">
                            <i class="fa fa-calendar-o"></i>
                        </div>
                    </div>-->
                    {{ Form::button('Buscar', ['type'=>'submit','class'=>'btn btn-primary']) }}
                    {{ Form::close() }}
                </div>
            </div>
			
			<div class="panel-body">
				{{$buttons}}
				<table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Nome do curso</th>
                            <th>Informações</th>
                              <th>Email responsável</th>

                            <th>Telefone/Celular</th> 
                           
                            <th>Atividade</th>
                           
           
                            <th>Min Part.</th>
                            <th>Max Part.</th>
                           
                       

                          	<th>Ver alunos</th>
                          	<th>Status (alterar)</th>
                            
                        </tr>
                    </thead>
                    <tbody>
						
                    	@forelse($agendas as $a)
                    	
						
							
						
                    	<tr>
                    		<td valign="middle">{{ $a->curso->consumer_name }}</td>
                    	
                    		<td valign="middle"><p>{{Helper::ConverterBR($a->date_ini,true)}} às {{$a->periodo->hour_ini}}h - {{ $a->endereco }}, {{$a->cidade->name}} - {{$a->cidade->estado->uf}}</p>
                    		</td>
                    		    <td valign="middle">{{ $a->email }}</td>

						    <td valign="middle">{{ $a->tel }} / {{ $a->cel }}</td>
                    		<td valign="middle">{{$a->atividade->name}}</td>
                    		<td valign="middle">{{$a->curso->min_quorum}}</td>
							<td valign="middle">{{$a->curso->max_quorum}}</td>
						

						    <td valign="middle">{{$a->getLink()}}</td>

						    <td valign="middle">{{ $a->statusText(true) }}</td>
                    	</tr>
						

                    	
						@empty
						<tr><td colspan="8">Ainda não tem curso cadastrado</td></tr>
  						@endforelse
                    </tbody>
                </table>
                
			</div>
		
	</div>
   
@stop

                   