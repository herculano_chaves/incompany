@extends('layouts.backend')
@section('content')
	@include('layouts.backendmenuinstructors')
	<div class="content">
		@include('layouts.notifications')
		<div class="panel">
			<div class="panel-heading">
				<strong>Alterar disponibilidades</strong>
			</div>
			
			<div class="panel-body">
				{{ Form::open(['route'=>'instructor/update/disp/all']) }}

				<div class="row" style="margin-left:7px;">
					@foreach($cursos as $c)

						{{ Form::hidden('cursos[]', $c->id) }}
						<div class="marquer">	{{ $c->consumer_name }} </div>

					@endforeach
				</div>
				  <p>Remarque as opções de disponibilidade, <strong>só serão válidos dias marcados na cor AZUL.</strong></p>
                                <style>.custom-highlight a{
                                      color: #fff !important;
                                      background-color: #E7B88E !important; 
                                      text-shadow: 0 1px 0 rgba(0, 0, 0, .26); 
                                      -moz-box-shadow: inset 0 4px 9px rgba(0, 0, 0, .24);
                                      -webkit-box-shadow: inset 0 4px 9px rgba(0, 0, 0, .24); 
                                      box-shadow: inset 0 4px 9px rgba(0, 0, 0, .24); 
                                    }</style>
                                <script>
                                    $(document).ready(function(){
                                        
                                        function available(date) {
                                             var day = date.getDay();
    
                                          dmy = ('0' + date.getDate()).slice(-2) + "/" + ('0' + (date.getMonth()+1)).slice(-2)+ "/" + date.getFullYear();
                                         
                                         var availableDates = null;

                                         if(availableDates==null){
                                                availableDates = dmy;
                                          }
                                         // console.log(dmy);
                                          //console.log(availableDates);
                                          if ( ($.inArray(dmy, availableDates) != -1)  ) {
                                            return [true, 'custom-highlight', 'Esta ativo este dia'];
                                          } else {
                                            return [true,"","Disponível"];
                                          }
                                        };
                                        $('#datepicker').multiDatesPicker({
                                            minDate: "+15D", 
                                            maxDate: "+6M",
                                            beforeShowDay: available,
                                            onSelect: function (dateText, inst){

                                               /*var availableDates = $('.passDate').val();
                                                availableDates = availableDates.split(',');
                                                if($.inArray(dateText, availableDates) != -1){
                                                    $('a.ui-state-default:contains('+inst.selectedDay+')').parent().removeClass('custom-highlight');
                                                }*/

                                                $('.passDate').val('');
                                                $('#datepicker').multiDatesPicker('resetDates', 'disabled');

                                                var dates = $('#datepicker').multiDatesPicker('getDates');
                                                var pastDates = $('.passDate').val();

                                                if(pastDates != ''){
                                                    $('.passDate').val(pastDates+', '+dates);
                                                }else{

                                                    $('.passDate').val(dates);
                                                }
                                                 //$('.passDate').val(dates);
                                                
                                            } 
                                        });
                                       
                                    });
                                </script>
                                {{ Form::hidden('disponibilidade', null, ['class'=>'passDate']) }}
                                <div id="datepicker"></div>

                                <div class="row">
                                	@foreach($horarios as $k => $h)

                                @if($un_horario != null and in_array($h->id, $un_horario))
                                 <?php $c = "checked"; ?> 
                                @else
                                 <?php $c=null; ?> 
                                @endif
                                    {{ Form::checkbox('disponibilidade_horarios[]', $h->id, $c, ['id'=>'disp_h_'.$h->id]) }} {{ Form::label('disp_h_'.$h->id, $h->hour_ini) }} hrs
                                    @if($k < ($horarios->count()-1))
                                        <span> | </span>
                                    @endif

                                @endforeach
                                </div>

                                {{ Form::submit('Editar para estes cursos') }}

                                {{ Form::close() }}
			</div>
		</div>
	</div>
@stop