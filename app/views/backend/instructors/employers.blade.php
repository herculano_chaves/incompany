@extends('layouts.backend')

@section('content')
	@include('layouts.backendmenuinstructors')
	<div class="content">
		<div class="panel">
			@include('layouts.notifications')
			<div class="panel-heading">
				<strong>Alunos do curso {{ $agenda->curso->consumer->name }} <br/> {{Helper::ConverterBR($agenda->date_ini, true)}} - {{ $agenda->periodo->hour_ini }}h às {{$agenda->periodo->hour_ini+$agenda->curso->tipo->duracao}}h <br/> {{ $agenda->endereco }}, {{ $agenda->cidade->name }} - {{ $agenda->cidade->estado->uf }}</strong>
			</div>
			@if($agenda->status == '0')
			<div class="alert alert-warning">
			{{ $agenda->shareUrl('<p><strong>ATENÇÃO: </strong>Você precisa confirmar a solicitação, para este agendamento. <a href="javaScript:;" class="confirm_agenda" data-agenda="'.$agenda->id.'">Confirme agora!</a></p>') }}
			</div>
			@endif
		
			<div class="panel-body">
				<p style="text-align:right;">Com os selecionados: <select style="height:25px;" name="action_employers" id="action_employers"><option value="">--</option><option value="1">Confirmar presença e gerar certificado</option></select> <input type="submit" id="trigger_edit_all_employers" value="Ok"><span style="display:none;" id="loaderD">Carregando...</span></p>
				<table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Email</th>
                            <th>Profissão</th>
                            <th>Confirmar presença</th>
                            <th>Gerar Certificado</th>
                            <th><input type="checkbox" value="1" class="select_all_employers"></th>
                        </tr>
                    </thead>
                    <tbody>
                    	<form action="{{route('edit/employers',['a'=>$agenda->id])}}" method="post" id="edit_all_employers">
                        @forelse($agenda->employers as $e)
							  <tr>
							    <td valign="middle">{{ $e->name }}</td>
							    <td valign="middle">{{ $e->email }}</td>
							    <td valign="middle">{{ $e->profissao }}</td>
							    <td valign="middle">
							    	@if($e->presence=='0')
							    		<a href="{{route('employers/presence',['id'=>$e->id, 'a'=>$agenda->id])}}">Confirmar presença</a>
							    	@else
							    		Confirmado
							    	@endif
							    </td>
							    <td valign="middle">
							    	@if($e->certified=='0' and $e->presence=='1')
							    		<a href="{{route('employers/certify',['id'=>$e->id, 'a'=>$agenda->id])}}">Gerar certificado</a>
							    	@elseif($e->presence=='0')
							    	--
							    	@else
							    		Já gerado
							    	@endif
							    </td>
							    <td valign="middle">
							  		<input type="checkbox" name="employers[]" class="select_employer" value="{{$e->id}}">
							    </td>
							  </tr>
						@empty
							<tr>
								<td colspan="6">Ainda não tem participantes cadastrados</td>
							</tr>
  						@endforelse
  						</form>
                    </tbody>
                </table>
               {{ link_to_route('agenda/show','< Voltar para agenda',['id'=>$agenda->curso->id]) }}
			</div>
		
	</div>
@stop