@extends('layouts.backend')

@section('content')
	@include('layouts.backendmenuinstructors')
	<div class="content">
		@include('layouts.notifications')
		<div class="panel">
			<div class="panel-heading">
				<strong>Disponibilidade de agenda</strong>
			</div>
			
			<div class="panel-body">
				{{ Form::open(['action'=>'instructor/disponibilidade/edit', 'method'=>'post']) }}
				{{ Form::hidden('id', $instructor->id) }}
				<table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th colspan="7">Marque aqui sua disponibilidade para agendamento de cursos</th>
                        </tr>
                        
                    </thead>
                    <tbody>
                    	<tr><td colspan="7">Dias da semana</td></tr>
                    	<tr>
                    		<td>{{ Form::checkbox('disponibilidade[]', 1, $c1, ['id'=>'disp_1']) }} {{ Form::label('disp_1','Segunda') }}</td>
                    		<td>{{ Form::checkbox('disponibilidade[]', 2, $c2, ['id'=>'disp_2']) }} {{ Form::label('disp_2','Terça') }}</td>
                    		<td>{{ Form::checkbox('disponibilidade[]', 3, $c3, ['id'=>'disp_3']) }} {{ Form::label('disp_3','Quarta') }}</td>
                    		<td>{{ Form::checkbox('disponibilidade[]', 4, $c4, ['id'=>'disp_4']) }} {{ Form::label('disp_4','Quinta') }}</td>
                    		<td>{{ Form::checkbox('disponibilidade[]', 5, $c5, ['id'=>'disp_5']) }} {{ Form::label('disp_5','Sexta') }}</td>
                    		<td>{{ Form::checkbox('disponibilidade[]', 6, $c6, ['id'=>'disp_6']) }} {{ Form::label('disp_6','Sábado') }}</td>
                    		<td>{{ Form::checkbox('disponibilidade[]', 0, $c7, ['id'=>'disp_0']) }} {{ Form::label('disp_0','Domingo') }}</td>
                    	</tr>
                    	<tr><td>Período</td><td colspan="6">Até {{ Form::text('periodo_fim', Helper::ConverterBR($instructor->periodo_fim, true), ['id'=>'to']) }}</td></tr>
                    	<tr>
                    		<td colspan="7">{{ Form::submit('Enviar') }}</td>
                    	</tr>
                    </tbody>
                </table>
					
					
				{{ Form::close() }}
			</div>
	</div>

@stop