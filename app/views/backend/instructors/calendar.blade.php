@extends('layouts.backend') 
@section('content') 
@include('layouts.backendmenuinstructors')
<div class="content">
    @include('layouts.notifications')
    <div class="panel">
        <div class="panel-heading">
            <strong>Meu calendário</strong>
        </div>

        <div class="panel-body">
            <div class="">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab-cursos">Cursos</a></li>
                    <li><a href="#tab-palestras">Palestras</a></li>
                </ul>
                <div class="tab-content">
                    <div id="tab-cursos" class="tab-pane fade in active">
                        <div id="cursos"></div>
                        {{ Form::open(['route'=>'admin.instructors.calendar.save']) }}
                            {{ Form::hidden('savedDays',  null, ['id'=>'altFieldCursos']) }}
                            {{ Form::hidden('caracter_id',  'cursos') }}
                            {{ Form::submit('Editar selecionados') }}
                        {{ Form::close() }}
                         <div class="row">
                            <div class="col-md-12">
                              <h2 data-toggle="collapse" data-target="#resumo-disponibilidade" class="resumo-disponibilidade">Resumo de disponibilidade <i class="fa"></i></h2>
                                <div id="resumo-disponibilidade" class="collapse">
                                @foreach($cursos as $curso)
                                    <p><i style="font-size:10px;" class="alert-info fa fa-circle"></i> {{ $curso->consumer_name }}
                                      @if($curso->availables->count() > 0)
                                        <div class="alert alert-info">
                                          <table class="table table-striped table-bordered">
                                            @foreach($curso->availables as $a)
                                                  <tr>
                                                    <td>
                                                      <i class="fa fa-calendar"></i> {{ Helper::ConverterBR($a->date_at,true) }}
                                                    </td>
                                                    <td width="300">
                                                      <i class="fa fa-clock-o"></i>
                                                      @foreach($a->unHorario() as $k => $h)
                                                       {{ Available::getHourText($h) }} h 
                                                       @if($k+1 < count($a->unHorario()))
                                                        <strong> | </strong>
                                                       @endif
                                                      @endforeach
                                                    </td>
                                                    <td>
                                                      @if($a->cidades->count() > 0)
                                                        <a data-toggle="modal" data-target="#cid_{{ $a->id }}" href="javaScript:void(0);"><i class="fa fa-home"></i></a>
                                                        <div id="cid_{{ $a->id }}" class="modal fade" role="dialog">
                                                           <div class="modal-dialog">
                                                                <!-- Modal content-->
                                                                <div class="modal-content">
                                                                    <div class="modal-body">
                                                                      <h4>Cidades disponíveis para esta data <small>({{ Helper::ConverterBR($a->date_at, true) }})</small></h4>
                                                                      <p>{{ $a->curso->consumer_name }}</p>
                                                                      <ul>
                                                                        @foreach($a->cidades as $cidade)
                                                                          <li><i class="fa fa-location-arrow"></i> {{ $cidade->name }} - {{$cidade->estado->uf}}</li>
                                                                        @endforeach
                                                                      </ul>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                      <button type="button" class="btn btn-info" data-dismiss="modal">Fechar</button>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                          </div>
                                                        @else
                                                        --
                                                        @endif
                                                    </td>
                                                  </tr>
                                            @endforeach
                                          </table>
                                        </div>
                                      @endif
                                    </p>
                                @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="tab-palestras" class="tab-pane fade">
                        <div id="palestras"></div>
                        {{ Form::open(['route'=>'admin.instructors.calendar.save']) }}
                            {{ Form::hidden('savedDays',  null, ['id'=>'altFieldPalestras']) }}
                            {{ Form::hidden('caracter_id', 'palestras') }}
                            {{ Form::submit('Editar selecionados') }}
                        {{ Form::close() }}
                        <div class="row">
                            <div class="col-md-12">
                              <h2>Resumo de disponibilidade</h2>
                                @foreach($palestras as $palestra)
                                    <p><i style="font-size:10px;" class="alert-info fa fa-circle"></i> {{ $palestra->consumer_name }}
                                      @if($palestra->availables->count() > 0)
                                               <div class="alert alert-info">
                                          <table class="table table-striped table-bordered">
                                            @foreach($palestra->availables as $a)
                                                  <tr>
                                                    <td>
                                                      <i class="fa fa-calendar"></i> {{ Helper::ConverterBR($a->date_at,true) }}
                                                    </td>
                                                    <td width="300">
                                                      <i class="fa fa-clock-o"></i>
                                                      @foreach($a->unHorario() as $k => $h)
                                                       {{ Available::getHourText($h) }} h 
                                                       @if($k+1 < count($a->unHorario()))
                                                        <strong> | </strong>
                                                       @endif
                                                      @endforeach
                                                    </td>
                                                    <td>
                                                        @if($a->cidades->count() > 0)
                                                        <a data-toggle="modal" data-target="#cidp_{{ $a->id }}" href="javaScript:void(0);"><i class="fa fa-home"></i></a>
                                                        <div id="cidp_{{ $a->id }}" class="modal fade" role="dialog">
                                                           <div class="modal-dialog">
                                                                <!-- Modal content-->
                                                                <div class="modal-content">
                                                                    <div class="modal-body">
                                                                      <h4>Cidades disponíveis para esta data <small>({{ Helper::ConverterBR($a->date_at, true) }})</small></h4>
                                                                      <p>{{ $a->curso->consumer_name }}</p>
                                                                      <ul>
                                                                        @foreach($a->cidades as $cidade)
                                                                          <li><i class="fa fa-location-arrow"></i> {{ $cidade->name }} - {{$cidade->estado->uf}}</li>
                                                                        @endforeach
                                                                      </ul>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                      <button type="button" class="btn btn-info" data-dismiss="modal">Fechar</button>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                          </div>
                                                        @else
                                                        --
                                                        @endif
                                                    </td>
                                                  </tr>
                                            @endforeach
                                          </table>
                                        </div>  
                                      @endif
                                    </p>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="display:none; border:1px solid #ccc; background:white; padding:20px;" class="info"></div>
            <script>
            $(document).ready(function() {
                                       
                                        var availableDates_c = $('.passDate_cursos').val();
                                         availableDates_c = availableDates_c.split(',');
                                        // console.log(availableDates_c);
                                        function available_cursos(date) {
                                             var day = date.getDay();
    
                                          dmy = date.getFullYear() + "-" + ('0' + (date.getMonth()+1)).slice(-2)+ "-" + ('0' + date.getDate()).slice(-2);
                                         
                                         if(availableDates_c==null){
                                                availableDates_c = dmy;
                                          }
                                         // console.log(dmy);
                                         
                                          if ( ($.inArray(dmy, availableDates_c) != -1)  ) {
                                            return [true, 'custom-highlight', ''];
                                          } else {
                                            return [true,"","Disponível"];
                                          }

                                        };




                                         var availableDates_p = $('.passDate_palestras').val();
                                         availableDates_p = availableDates_p.split(',');
                                        function available_palestras(date) {
                                             var day = date.getDay();
    
                                          dmy = date.getFullYear() + "-" + ('0' + (date.getMonth()+1)).slice(-2)+ "-" + ('0' + date.getDate()).slice(-2);
                                         
                                         if(availableDates_p==null){
                                                availableDates_p = dmy;
                                          }
                                         // console.log(dmy);
                                          //console.log(availableDates);
                                          if ( ($.inArray(dmy, availableDates_p) != -1)  ) {
                                            return [true, 'custom-highlight', ''];
                                          } else {
                                            return [true,"","Disponível"];
                                          }
                                        };
                                       
                $('#cursos').multiDatesPicker({
                    minDate: new Date(),
                    maxDate: "+6M",
                    altField: '#altFieldCursos',
                    beforeShowDay: available_cursos
                });


                $('#palestras').multiDatesPicker({
                    minDate: new Date(),
                    maxDate: "+6M",
                    altField: '#altFieldPalestras',
                    beforeShowDay: available_palestras
                });
                  $("html").on("mouseenter",".ui-state-default", function(e) {
                    var day = $(this).text();
                    day = ("0" + Number(day)).slice(-2);
                    var month = $(this).parent('td').data('month');
                    var year = $(this).parent('td').data('year');

                    var date = day+'/'+(month+1)+'/'+year;
                    var type = $(this).parent().parent().parent().parent().parent().parent('div').attr('id');
                    var t;
                    if(type=='cursos'){
                      t = 1;
                    }else{
                      t = 2;
                    }
                   /* console.log(t);
                    console.log(date);*/
                    $(this).parent('td').css({position:'relative'});
                     var parentOffset = $(this).offset(); 
                     //or $(this).offset(); if you really just want the current element's offset
                     var relX =  parentOffset.left - 50;
                     var relY = parentOffset.top - 50;

                   /* if(month){
                      $.ajax({
                        url:'/instrutores/buscar/cursos/por/data',
                        type:'post',
                        data:{date:date,t:t}
                      }).done(function(data){
                        if(data.cursos != ''){
                          //console.log(data.cursos);
                          $('.info').empty();
                          var text = '<ul>';
                          $.each(data.cursos,function(k,v){
                            text += '<li>'+v.consumer.name+'</li>';
                            
                          });
                          text += '</ul>';
                          $('.info').html(text).css({position:'absolute',top:relY,left:relX});
                          $('.info').fadeIn();
                        }
                         
                      });
                    }*/
                  });

  $("html").on("mouseleave",".ui-state-default", function() {
    $('.info').fadeOut();
  });
            });
            </script>

        </div>
    </div>
    @if($cursos->count() > 0)
      <input type="hidden" class="passDate_cursos" value="{{ $arr_cursos }}">
    @else
  <input type="hidden" class="passDate_cursos" value="">
    @endif

    @if($palestras->count() >0)
      <input type="hidden" class="passDate_palestras" value="{{ $arr_palestras }}">
      @else
     
      <input type="hidden" class="passDate_palestras" value="">
    @endif
    @stop