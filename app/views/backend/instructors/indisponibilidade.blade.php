@section('content')

@include('layouts.backendmenu')

<div class="content">
	<div class="panel">
		<div class="panel-heading">
			<strong>Indisponibilidade</strong>
		</div>
		<div class="panel-body">
			@include('layouts.notifications-list')
			<table class="table table-striped table-bordered">
				<tr>
					<th>Marqui aqui dias indisponíveis</th>
					<th>Instrutor</th>
					<th>Usuário</th>
					<th>Status</th>
					<th>Justificativa</th>
					<th>Data</th>
				</tr>
				@forelse($logs as $l)
					<tr>
						<td>{{ $l->agenda->curso->consumer->name }}
						<br/>
						<strong>Data:</strong> {{ Helper::ConverterBR($l->agenda->date_ini, true) }} | {{ $l->agenda->periodo->hour_ini }}h
						<br>
						<strong>Local:</strong> {{ $l->agenda->endereco }}, {{ $l->agenda->curso->cidade->name }} - {{ $l->agenda->curso->cidade->estado->uf }}
 						</td>
						<td>{{ $l->instructor->user->name }}<br/>({{ $l->instructor->user->email }})</td>
						<td>{{ $l->user->name }}<br/>({{ $l->user->email }})</td>
						<td>{{ $l->statusText() }}</td>
						<td>{{ $l->justify }}</td>
						<td>{{ Helper::ConverterBR($l->created_at) }} às {{ Helper::Hora($l->created_at) }}</td>
						
					</tr>
				@empty
					<tr>
						<td colspan="5">Ainda não temos logs</td>
					</tr>
				@endforelse
			</table>
		</div>
	</div>
</div>
@stop