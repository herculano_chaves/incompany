
 @extends('layouts.backend')

@section('content')
	@include('layouts.backendmenuinstructors')
	<div class="content">
		@include('layouts.notifications')
		<div class="panel ">
			<div class="panel-heading">
				<strong>Eventos</strong> <small>({{ $cursos->getTotal() }})</small>
			</div>
			
			<div class="panel-body datas">
		

				{{--<div class="row">
					<div class="col-md-12">
						<div class="form-inline" role="form">
							{{ Form::open(['route'=>array('filter/instructor/courses', 'id'=>$instructor->user->id, 'caracter_id'=> Route::input('caracter_id') ), 'method'=>'get']) }}
					
					
						    <div class="form-group">
						        {{ Form::select('course_id', [''=>'Filtrar nome do curso']+$cursos->lists('consumer_name','id'), Input::get('course_id'), ['class'=>'form-control']) }}
						    </div>
						    <div class="form-group">
						       {{ Form::select('cidade_id', [''=>'Filtrar cidades']+$cidades, Input::get('cidade_id'), ['class'=>'form-control']) }}
						    </div>
						    <button type="submit" class="btn btn-primary">Filtrar</button>
						    {{ Form::close() }}
						</div>
					</div>
				</div>--}}
				 <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab-cursos">Cursos</a></li>
                    <li><a href="#tab-palestras">Palestras</a></li>
                </ul>
                <div class="tab-content">
                   	<div id="tab-cursos" class="tab-pane fade in active">
						<table  class="table table-bordered">
		                    <thead>
		                        <tr>
		                            <th>Nome do curso</th>
		                    
		                            <th>Descrição</th>

		                          	<th>Datas disponíveis</th>
		                        </tr>
		                    </thead>
		                    <tbody>
								@forelse($cursos as $c)
									  <tr>
									    <td valign="middle">{{$c->consumer_name}}</td>
									    <td valign="middle">{{$c->consumer->description}}</td>
									    <td valign="middle">
									    	 @foreach($c->availables as $a)
		                                          <p>{{ Helper::ConverterBR($a->date_at,true) }}</p> 
		                                      @endforeach
									    </td>
									 
									  
									  
									  </tr>
		                    	
								@empty
								<tr><td colspan="8">Ainda não tem curso cadastrado</td></tr>
		  						@endforelse
		                    </tbody>
		                </table>
					
		                {{ $cursos->appends($_GET)->links() }}
		              </div>

                  <div id="tab-palestras" class="tab-pane fade">
					<table  class="table table-bordered">
	                    <thead>
	                        <tr>
	                            <th>Nome da palestra</th>
	                    
	                            <th>Descrição</th>

	                          	<th>Datas disponíveis</th>
	                        </tr>
	                    </thead>
	                    <tbody>
							@forelse($palestras as $c)
								  <tr>
								    <td valign="middle">{{$c->consumer_name}}</td>
								    <td valign="middle">{{$c->consumer->description}}</td>
								    <td valign="middle">
								    	 @foreach($c->availables as $a)
	                                          <p>{{ Helper::ConverterBR($a->date_at,true) }}</p> 
	                                      @endforeach
								    </td>
								 
								  
								  
								  </tr>
	                    	
							@empty
							<tr><td colspan="8">Ainda não tem palestra cadastrada</td></tr>
	  						@endforelse
	                    </tbody>
	                </table>
			
                {{ $palestras->appends($_GET)->links() }}
                </div>
            </div>
			</div>

			
	</div>

@stop
