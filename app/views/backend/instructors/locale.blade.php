@extends('layouts.backend')
@section('content')
	@include('layouts.backendmenuinstructors')
	<div class="content">
		@include('layouts.notifications-list')
		<div class="panel editar-meus-cursos">
			<div class="panel-heading">
				<strong>Cidades</strong> - <small>editar cidades onde atendem meus cursos</small>
			</div>
			
			<div class="panel-body datas">
				@if($states)
				{{ Form::open(['route'=>array('instructor.locale.store', $instructor->id)]) }}	
				<div class="row">
				  	<div class="col-md-6">
				  		 {{ Form::select('state_id', [''=>'Estados']+$states, Input::get('state_id'), ['class'=>'form-control','id'=>'estado_id']) }}
                    
				  	</div>
				</div> 
				<div class="row">
				  	<div class="col-md-6">
				  
				  		 {{ Form::select('city_id', [''=>'Cidades']+$cities, Input::get('city_id'), ['class'=>'form-control','id'=>'cidade_id']) }}
                    
				  	</div>
				</div> 
				<div class="row">
					<div class="col-md-6">
						{{ Form::button('<i class="fa fa-plus"></i> Adicionar', ['type'=>'submit', 'class'=>'btn btn-info']) }}
					</div>
				</div> 
				{{ Form::close() }}
				@endif
				
				<div class="row">
					<div class="col-md-12">
						@if($instructor->cidades->count() > 0)
						<table class="table-bordered table-striped table">
							<tr>
								<th>Localização</th>
								<th>Apagar</th>
							</tr>
							@foreach($instructor->cidades as $c)
								<tr>
									<td>{{ $c->name }} - {{ $c->estado->uf }}</td>
									<td>
										{{ Form::open(['route'=>array('instructor.locale.delete',$instructor->id), 'method'=>'delete']) }}
										{{ Form::hidden('city_id', $c->id) }}
										{{ Form::button('<i class="fa fa-trash"></i>', ['type'=>'submit', 'class'=>'btn btn-danger']) }}
										{{ Form::close() }}
									</td>
								</tr>
							@endforeach
						</table>
						@else 
						<p>Ainda não tem cidades cadastradas</p>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
@stop
