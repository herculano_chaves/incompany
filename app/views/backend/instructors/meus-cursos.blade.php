
 @extends('layouts.backend')

@section('content')
	@include('layouts.backendmenuinstructors')
	<div class="content">
		@include('layouts.notifications')
		<div class="panel editar-meus-cursos">
			<div class="panel-heading">
				<strong>EDITAR DISPONIBILIDADES</strong> ({{ $cursos->count() }})
			</div>
			
			<div class="panel-body datas">
		
				<div class='row box-datas'>
					<div class='col-md-6'>
						<p><b>Datas selecionadas para edição:</b></p>
						@if(!empty($dates))
						
							@foreach($dates as $k => $d)
			
								<p style="border:1px solid #ccc; padding:20px 10px; margin-bottom:10px;" id="{{trim($k)}}" class="data-selecionada"><i class="fa fa-circle"></i><input type="text" name="dates[]" value="{{ trim($d) }}" readonly><a href="javaScript:;"><i class="fa fa-close"></i></a> 
									@if(Available::getByDate(Helper::ConverterUS(trim($d)), $c_id,  $instructor->id)->count() >0)
									<a class="pull-right btn btn-danger" href="{{ route('available.delete.all', Helper::ConverterUS(trim($d)) ) }}"><i class="fa fa-trash"></i></a>
									@endif
								</p>
								<table class="table table-bordered">
								@foreach(Available::getByDate(Helper::ConverterUS(trim($d)), $c_id, $instructor->id) as $curso)
									<tr>
										<td>{{ $curso->consumer_name }}</td>
										<td><a class="btn btn-danger" href="{{route('available.delete', $curso->findAvailable(Helper::ConverterUS(trim($d))) ) }}">Retirar <i class="fa fa-trash"></i></a></td>
									</tr>
								@endforeach
								</table>
								
							@endforeach
						
						@else
							<p class="alert-danger alert"><strong>Atenção</strong>: para editar qualquer disponibilidade agrupada, você deverá selecionar datas na área de <a class="link" href="{{ route('admin.instructors.calendar') }}">Calendário</a></p>
						@endif

						<p class="alert-danger alert no-select-date" style="display:none;"><strong>Atenção</strong>: para editar qualquer disponibilidade, você deverá selecionar datas na área de <a class="link" href="{{ route('admin.instructors.calendar') }}">Calendário</a></p>
					</div>
					<div class='col-md-6'>
						<a href="{{ route('admin.instructors.calendar.cancel') }}" type="button" class="btn btn-default pull-right">Cancelar edição</a>
					</div>
				</div>

			
				<div class="row">
					<div class="col-md-12">
						<div class="form-inline" role="form">
							{{ Form::open(['route'=>array('filter/instructor/courses', 'id'=>$instructor->user->id, 'caracter_id'=> Route::input('caracter_id') ), 'method'=>'get']) }}
					
					
						    <div class="form-group">
						        {{ Form::select('course_id', [''=>'Filtrar nome do curso']+$cursos->lists('consumer_name','id'), Input::get('course_id'), ['class'=>'form-control']) }}
						    </div>
						    <div class="form-group">
						       {{-- Form::select('cidade_id', [''=>'Filtrar cidades']+$instructor->cidades->lists('name','id'), Input::get('cidade_id'), ['class'=>'form-control']) --}}
						    </div>
						    <button type="submit" class="btn btn-primary">Filtrar</button>
						    {{ Form::close() }}
						</div>
					</div>
				</div>
				{{ Form::open(['route'=>'instructor/update/disp/all']) }}
				<table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Nome do curso</th>
                    
                            <th>Horário disponíveis</th>

                          	<th>Cidades</th>

                          	<th>Selecionar todos <input type="checkbox" class="selec_all"></th>

                        </tr>
                    </thead>
                    <tbody>
						@forelse($cursos as $c)
							  <tr>
							    <td valign="middle">{{$c->consumer_name}}</td>
							 
							  
							    <td valign="middle">
							    	<a data-toggle="modal" data-target="#h_{{$c->id}}" href="javaScript:;">Escolher horários</a> 

							    	
							    	{{-- $c->availables->count() < '1' ? '<i class="fa fa-close"></i>' : '<i class="fa fa-check"></i>'--}}

							    	
       
							    </td>

						   <td valign="middle">
							  <a data-toggle="modal" data-target="#c_{{ $c->id }}" href="javaScript:void(0);"><i class="fa fa-home"></i></a>
							</td>
							  

							    <td valign="middle" class="align-center"> <input type="checkbox" name="cursos[]" class="selec_cursos" value="{{$c->id}}"> </td>

							   
							  </tr>
                    	
						@empty
						<tr><td colspan="8">Ainda não tem curso cadastrado</td></tr>
  						@endforelse
                    </tbody>
                </table>
				<div class=" box-horarios">
					<div class="row">
					    <div class="col-md-12">
					        <p>Escolher horários disponíveis dos itens selecionados (* escolhendo por agrupamento você subscreve estes itens)</p>
					        <div class="box-horarios-check">
						        @foreach($cursos->first()->getCursoHorario() as $k => $h)
								<label class="checkbox-inline">
								    <input type="checkbox" name="disponibilidade_horarios[]" value="{{ $h->id }}">{{ $h->hour_ini}} hrs
								</label>
						        @endforeach
					        </div>
					    </div>
					</div>
				</div>
				<div class="row" style='margin-bottom: 0;'>
					<div class="col-md-12">
						{{ Form::submit('SALVAR', ['class'=>'btn btn-primary']) }}
					</div>
				</div>
				@foreach($dates as $k => $d)
					<input type="hidden" id="pot_{{$k}}" name="dates[]" value="{{ Helper::ConverterUS(trim($d),true) }}" readonly>
				@endforeach

                
			</div>

			{{-- Modal com horarios --}}
				@foreach($cursos as $c)

					<div id="c_{{ $c->id }}" class="modal fade" role="dialog">
						 <div class="modal-dialog">
					        <!-- Modal content-->
					        <div class="modal-content">
					            <div class="modal-body">
					            	<div class="row">
					            		<div class="col-md-12">
					            			<h3>Selecione as cidades para estes dias</h3>
					            			<p><i class="fa fa-calendar"></i> 
					            				@foreach($dates as $k => $d)
													<span>{{ trim($d) }}</span> |
												@endforeach
					            			</p>	
					            			<div style="display:none;">
					            				<?php $arr_d = array(); ?>
					            				@foreach($dates as $k => $d)
													<?php array_push($arr_d,Helper::ConverterUS($d)); ?> 
					            				@endforeach
					            			</div>
					            			<p><strong><i class="fa fa-hand-o-right"></i> {{ $c->consumer_name }}</strong></p>
					            			<p><strong>{{ Form::checkbox('selec_all_cities') }} Selecionar todas as cidades</strong></p>
					            			 @foreach($c->instructor->cidades as $cidade)
												<p>{{ Form::checkbox('city_ids['.$c->id.'][]', $cidade->id, in_array($cidade->id, $c->getCidadesThroughAvailablesDate($c->id, $arr_d)->lists('cidades.id')), ['class'=>'check_cities']) }}
													{{ $cidade->name }} - {{ $cidade->estado->uf }}</p>
											@endforeach
					            		</div>
					            	</div>
					            </div>
					            <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                </div>
					        </div>
					    </div>
					</div>

				@endforeach
				
				{{ Form::close() }}

				@foreach($cursos as $c)

					<div id="h_{{$c->id}}" class="modal fade" role="dialog">
					    <div class="modal-dialog">
					        <!-- Modal content-->
					        <div class="modal-content">
					            <div class="modal-body">

					            	@if($c->availables->count() > '0')
					            	{{ Form::open(['route'=>'update.availables.hour']) }}
	                                <div class='row'>
	                                    <div class="col-md-12">
	                                    	<p><b>Escolha os horários disponíveis deste evento ({{ $c->consumer_name }}):</b></p>
	                                    	
	                                    	<h3>Datas</h3>

											{{ Form::hidden('caracter_id', Route::input('caracter_id')) }}
											
	                                    	@foreach($c->availables as $a)
	                                    	
	                                    		@if(in_array(' '.Helper::ConverterBR($a->date_at, true), $dates) or in_array(Helper::ConverterBR($a->date_at, true), $dates))
												<li>
													{{ Form::hidden('availables[]', $a->id) }}
													<i class="fa fa-calendar"></i> {{ $a->date_at }}
													<div class="alert alert-warning">
														<span><i class="fa fa-clock-o"></i> </span>
														@foreach($c->getCursoHorario() as $h)
															
																{{ Form::checkbox('disponibilidade_horarios_exc[]', $h->id, in_array($h->id, $a->unHorario())) }} 
																{{ $h->hour_ini}} hrs
															
														@endforeach
													</div>
												</li>

												@endif
	                                    	@endforeach

	                                    	
	                                    </div>
	                                </div>
                    				<div class="row">
                    					<div class="col-md-12">
                    						<button type="submit" class="btn btn-primary">SALVAR</button>
                    						<button type="button" class="btn btn-link" data-dismiss="modal">Cancelar</button>
                    					</div>
                    				</div>	
                    				{{ Form::close() }}
                    				@else
										<p>Adicione disponibilidades para este curso nesta data;</p>
                    				@endif
					            </div>
					        </div>
					    </div>
					</div>

					@endforeach

					


	</div>
<div id="datepic" style="display:none;">
	<div id="datepicker"></div>
</div>
@stop
