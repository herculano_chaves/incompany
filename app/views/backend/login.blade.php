@extends('layouts.backend')

@section('content')
	<div class="login-header">
		<h1 class="login-header-title text-center">Knauf In Company</h1>
	</div>
	<div class="login-section">
		<div class="login-section-content">
			<?php $u = explode('/',URL::current()); ?>
			<div id="erro"> @include('layouts.notifications') </div>
			<form action="@if(Route::current()->getUri() == 'instrutores'){{URL::route('instructor/login')}}@else{{URL::route('backend/login')}}@endif" method="post" class="login-section-content-form">
				@if(Route::current()->getUri() == 'instrutores')<input type="hidden" name="instructor" value="1" >@endif
				<div class="input-group row">
					<span class="input-group-lb">
						<span class="icon icon-mail"></span>
					</span>
					<input type="text" name="email" value="{{Input::old('email')}}" class="form-control" placeholder="Email do usuário">
					{{ $errors->first('email') }}
				</div>
				<div class="input-group row">
					<span class="input-group-lb">
						<span class="icon icon-lock"></span>
					</span>
					<input type="password" name="password" class="form-control" placeholder="Senha">
					{{ $errors->first('password') }}
				</div>
				<input class="btn-login row" type="submit" value="Logar">
				<div class="login-links text-center">
					<p>{{ link_to_route('password.remind','Esqueci minha senha') }}</p>
				</div>
				   
			</form>
		</div>
	</div>
@stop