@section('content')
	
	<div id="cadastro">
		@if($message = Session::get('success'))
			@include('layouts.notifications')
		@else
		
		<h2>Cadastre-se para participar dos cursos</h2>
		
		{{ Form::open(['route'=>'user/register', 'method'=>'post', 'id'=>'frmCadastro']) }}
		
		<div class="blc">
			<span>
				<p>{{ Form::text('name', '',['placeholder'=>'Nome completo (Responsável pela empresa) *']) }}</p>
				<p class="error">{{ $errors->first('name') }}</p>
			</span>
			<span>
				<p>{{ Form::text('cpf','',['placeholder'=>'CPF *','id'=>'cpf']) }}</p>
				<p class="error">{{ $errors->first('cpf') }}</p>
			</span>	
		</div>
		<div class="blc">
			<span>
				<p>{{Form::select('profissao',[''=>'Profissão *', 'Instalador de drywall'=>'Instalador de drywall', 'pintor'=>'pintor', 'marceneiro'=>'marceneiro', 'instalador de elétrica'=>'instalador de elétrica', 'instalador de hidráulica'=>'instalador de hidráulica', 'pedreiro'=>'pedreiro', 'cargos administrativos'=>'cargos administrativos', 'engenheiro civil'=>'engenheiro civil', 'técnico em edificações'=>'técnico em edificações', 'arquiteto'=>'arquiteto', 'design de interiores'=>'design de interiores', 'projetista'=>'projetista', 'estudante de arquitetura'=>'estudante de arquitetura', 'estudante de engenharia civil'=>'estudante de engenharia civil', 'estudante de design de interiores'=>'estudante de design de interiores', 'estudante de técnica em edificações'=>'estudante de técnica em edificações', 'mestre de obras'=>'mestre de obras', 'assentador cerâmico'=>'assentador cerâmico', 'decorador'=>'decorador', 'paisagista'=>'paisagista', 'outros'=>'outros'],null,['id'=>'profissao'])}}</p>
				<p class="error">{{ $errors->first('profissao') }}</p>
			</span>	
			<span style="display:none;" id="informe">
				<p>{{ Form::text('informe','',['placeholder'=>'Caso não tenha sua profissão listada ao lado']) }}</p>
				<p class="error">{{ $errors->first('informe') }}</p>
			</span>	
		</div>
		<div class="blc">
			<span>
				<p>{{ Form::text('empresa','',['placeholder'=>'Empresa *']) }}</p>
				<p class="error">{{ $errors->first('empresa') }}</p>
			</span>	
		
			<span>
				<p>{{ Form::text('cargo','',['placeholder'=>'Cargo *']) }}</p>
				<p class="error">{{ $errors->first('cargo') }}</p>
			</span>	
		</div>
		<hr/>
		<div class="blc">
			<span>
				<p>{{ Form::select('estado_id', [''=>'UF *']+$estados, null, ['id'=>'estado_id']) }}</p>
				<p class="error">{{ $errors->first('estado_id') }}</p>
			</span>	
		
			<span>
				<p>{{ Form::select('cidade_id',[''=>'Selecione cidade *'], null, ['id'=>'cidade_id', 'disabled']) }}</p>
				<p class="error">{{ $errors->first('cidade_id') }}</p>
			</span>	
		</div>
		<div class="blc">
			
			<span>
				<p>{{ Form::text('endereco','',['placeholder'=>'Endereço completo da empresa *', 'class'=>'large']) }}</p>
				<p class="error">{{ $errors->first('endereco') }}</p>
			</span>	
		</div>
		<div class="blc">
			
			<span>
				<p>{{ Form::text('cep','',['placeholder'=>'CEP *','id'=>'cep']) }}</p>
				<p class="error">{{ $errors->first('cep') }}</p>
			</span>	
		
			
			<span>
				<p>{{ Form::email('email','',['placeholder'=>'E-mail *']) }}</p>
				<p class="error">{{ $errors->first('email') }}</p>
			</span>	
		</div>
		<div class="blc">
			
			<span>
				<p>{{ Form::text('tel','',['placeholder'=>'Telefone fixo (ddd+número) *','id'=>'tel']) }}</p>
				<p class="error">{{ $errors->first('tel') }}</p>
			</span>	
		
			<span>
				<p>{{ Form::text('cel','',['placeholder'=>'Celular (ddd+número) *','id'=>'cel']) }}</p>
				<p class="error">{{ $errors->first('cel') }}</p>
			</span>	
		</div>
		<div class="blc"><p class="ft12">*Todos os campos são obrigatórios</p></div>
		<div class="blc">
			{{ Form::submit('Enviar',['class'=>'bt blue bt_large']) }}
			<span style="float:none; margin-left:20px;" class="wrap_loader"></span>
		</div>
		{{ Form::close() }}

		@endif

		{{link_to('/','< Voltar',['class'=>'ft11 u gray','style'=>'float:right;'])}}
	</div>
	
@stop