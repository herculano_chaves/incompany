@extends('layouts.backend')



@section('content')

	@include('layouts.backendmenu')

	<div class="content">

		<div class="panel">

			@include('layouts.notifications')

			<div class="panel-heading">

				<strong>Alunos do curso {{ $agenda->curso->consumer->name }}</strong>

			</div>

			

			<div class="panel-body">
				@if($agenda->employers->count() > 0)
				<table class="table table-striped table-bordered">

                    <thead>

                        <tr>

                            <th>Nome</th>

                            <th>Email</th>

                            <th>Profissão</th>

                        
                        </tr>

                    </thead>

                    <tbody>

                        @foreach($agenda->employers as $e)

							  <tr>

							    <td valign="middle">{{ $e->name }}</td>

							    <td valign="middle">{{ $e->email }}</td>

							    <td valign="middle">{{ $e->profissao }}</td>

							   
							  </tr>

  						@endforeach

                    </tbody>

                </table>

               @else
					<p>Agenda sem convidados</p>
               @endif
				
				{{link_to_route('user/cursos','< Voltar para agenda do usuario '.$agenda->user->name,['id'=>$agenda->user->id])}}
			</div>

		

	</div>

@stop