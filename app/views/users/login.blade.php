@section('content')

	@include('layouts.notifications')

	<div id="login">

		<h2 class="ft24">Acesse a sua conta</h2>

		

		{{ Form::open(['route'=>'user/login', 'method'=>'post']) }}

		<p>

			{{ Form::email('email', null, ['placeholder'=>'Seu e-mail', 'id'=>'gro']) }}

			<span class="error">{{ $errors->first('email') }}</span>

		</p>

		<p>

			{{ Form::text('password',null,['placeholder'=>'Sua senha', 'id'=>'pass']) }}

			<span class="error">{{ $errors->first('password') }}</span>

		</p>

		<p style="margin-bottom:15px;">{{ HTML::link('password/reset','Esqueci minha senha',['class'=>'u ft12 i black']) }}</p>

		<p>

			{{ Form::submit('Enviar',['class'=>'bt blue bt_large']) }}

		</p>

		

		@if($message = Session::get('login_error'))

			<p><span class="error">{{ $message }}</span></p>

		@endif

		{{ Form::close() }}
		<p>{{link_to('/','< Voltar',['class'=>'ft11 u gray','style'=>'float:right;margin-right: 94px;'])}}</p>

	</div>

	<div id="access">

		<h3 class="ft20">Não tenho cadastro:</h3>

		{{HTML::link('cadastro', 'Criar cadastro agora!',['class'=>'bt orange bt_xlarge'])}}

	</div>
	
@stop