@section('homemeta')
	@parent
	<link rel="stylesheet" type="text/css" href="{{asset('css/admin/table.css')}}" />
@stop
@section('content')
	<!-- html minha agenda -->
	@include('layouts.notifications')
	<div id="minha-agenda">
		<h2 class="ft24">Meus cursos</h2>
		<div id="cursos-agendados">
			<p class="sub">Cursos confirmados ({{ $agendas->count() }})</p>
			@foreach($agendas as $a)
				<div class="curso">
					<div class="data-curso"><span>{{ Helper::Dia($a->date_ini) }}</span><br>{{ Helper::Extenso(null,$a->date_ini, true); }}<br>{{ Helper::Ano($a->date_ini) }}<br></div>
					<div class="descricao-curso">
						<h2>{{ $a->curso->consumer->name }}</h2>
						<p>{{ $a->curso->consumer->excerpt }} <a href="{{route('agenda/show/detalhes',['id'=>$a->id])}}" class="orange b">Ver detalhes...</a></p>

						<div class="linker">
							<span>Copie e o link abaixo para divulgar sua agenda de curso para seus participantes.</span><br/>
							<a href="{{ $a->shareUrl() }}">{{ $a->shareUrl() }}</a>
						</div>
					</div>
					<div class="dados-curso">
						<div class="item um"><span>{{$a->employers()->count()}}</span> pessoas inscritas.</div>
					</div>
					<div class="acao-curso">
						<a href="{{URL::current()}}/cancelar/{{$a->id}}" data-dia="{{Helper::Dia($a->date_ini)}}" data-mes="{{Helper::Mes($a->date_ini)}}" data-ano="{{Helper::Ano($a->date_ini)}}" class="bt orange bt_acao bt_cancela_curso">Cancelar</a>
					</div>
				</div>
			@endforeach
			<p style="margin-top:15px; display:inline-block;" class="ft11 red">* Lembrando que: Os cursos só poderão ser cancelados até 3 dias antes do agendamento.</p>
		</div>
		
		<div id="cursos-concluidos">
			<p class="sub">Cursos concluídos ({{ $agendas_conc->count() }})</p>
			@if($agendas_conc)
				@foreach($agendas_conc as $a)
					<div class="curso">
						<div class="data-curso"><span>{{ Helper::Dia($a->date_ini) }}</span><br>{{ Helper::Extenso(null,$a->date_ini,true); }}<br>{{ Helper::Ano($a->date_ini) }}<br></div>
						<div class="descricao-curso">
							<h2>{{ $a->curso->consumer->name }}</h2>
							<p>{{ $a->curso->consumer->excerpt }}
								<!-- <a href="{{route('agenda/show/detalhes',['id'=>$a->id])}}">Ver detalhes...</a> -->
							</p>
						</div>
						<div class="dados-curso">
							<div class="item um"><span>{{$a->employers()->count()}}</span> pessoas inscritas.</div>
							{{-- Verifico se há  participantes pra não dar erro --}}
							@if($a->employers()->count()>0) 
							<div class="item dois"><span>{{ round(($a->employers()->presents()->count()/$a->employers()->count())*100) }}%</span> de presença no curso.</div> 
							@endif
						</div>
						<div class="acao-curso">
							<a href="#participantes_{{$a->id}}" class="bt orange bt_acao bt_certificado" data-agenda="{{$a->id}}">Certificado</a>
						</div>
						
						<div style="display:none;">
							<div id="participantes_{{$a->id}}">
								<div class="panel">
								<div class="panel-body">
								<table class="table table-striped table-bordered">
									<tr>
										<th>Nome</th>
										<th>Email</th>
										<th>Profissão</th>
										<th>Gerar certificado</th>
									</tr>

									@foreach($a->employers as $e)
										@if($e->presence == '1')
										<tr>
											<td valign="middle">{{ $e->name }}</td>
											<td valign="middle">{{ $e->email }}</td>
											<td valign="middle">{{ $e->profissao }}</td>
											<td valign="middle">
													@if($e->certified == '0')
								    				<a href="{{route('user/employers/certify',['id'=>$e->id, 'a'=>$a->id, 'byUser'=>1])}}">Gerar certificado</a>
										   			@else
										   			Gerado
										   			@endif
											</td>
										</tr>
										@endif
									@endforeach
								</table>
							</div>
							</div></div>
						</div>
					</div>
				@endforeach
			@else
				<p>Você ainda não conclui nenhum curso até o momento.</p>
			@endif
		
		</div>
	</div>
@stop