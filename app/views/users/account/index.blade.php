@section('content')
	<!-- Pagina do Usuário -->
	<div id="cadastro">
		@include('layouts.notifications')
		<h2>Meus dados</h2>
		
		{{ Form::open(['route'=>'user/edit', 'method'=>'post']) }}
		{{ Form::input('hidden', 'user_id', $user->id) }}
		<div class="blc">
			<span>
				<p>{{ Form::text('name', $user->name, ['placeholder'=>'Nome completo (Responsável pela empresa)']) }}</p>
				<p class="error">{{ $errors->first('name') }}</p>
			</span>
			<span>
				<p>{{ Form::text('cpf', $user->cpf, ['placeholder'=>'CPF','id'=>'cpf']) }}</p>
				<p class="error">{{ $errors->first('cpf') }}</p>
			</span>	
		</div>
		<div class="blc">
			<span>
				<p>{{Form::select('profissao',[''=>'Profissão', 'Instalador de drywall'=>'Instalador de drywall', 'pintor'=>'pintor', 'marceneiro'=>'marceneiro', 'instalador de elétrica'=>'instalador de elétrica', 'instalador de hidráulica'=>'instalador de hidráulica', 'pedreiro'=>'pedreiro', 'engenheiro civil'=>'engenheiro civil', 'técnico em edificações'=>'técnico em edificações', 'arquiteto'=>'arquiteto', 'design de interiores'=>'design de interiores', 'projetista'=>'projetista', 'estudante de arquitetura'=>'estudante de arquitetura', 'estudante de engenharia civil'=>'estudante de engenharia civil', 'estudante de design de interiores'=>'estudante de design de interiores', 'estudante de técnica em edificações'=>'estudante de técnica em edificações', 'mestre de obras'=>'mestre de obras', 'assentador cerâmico'=>'assentador cerâmico', 'decorador'=>'decorador', 'paisagista'=>'paisagista', 'outros'=>'outros'], $user->profissao,['id'=>'profissao'])}}</p>
				<p class="error">{{ $errors->first('profissao') }}</p>
			</span>	
			<span style="display:none;" id="informe">
				<p>{{ Form::text('informe','',['placeholder'=>'Caso não tenha sua profissão listada ao lado']) }}</p>
				<p class="error">{{ $errors->first('informe') }}</p>
			</span>	
		</div>
		<div class="blc">
			<span>
				<p>{{ Form::text('empresa', $user->empresa,['placeholder'=>'Empresa']) }}</p>
				<p class="error">{{ $errors->first('empresa') }}</p>
			</span>	
		
			<span>
				<p>{{ Form::text('cargo', $user->cargo,['placeholder'=>'Cargo']) }}</p>
				<p class="error">{{ $errors->first('cargo') }}</p>
			</span>	
		</div>
		<hr/>
		<div class="blc">
			
			<span>
				<p>{{ Form::text('endereco', $user->endereco,['placeholder'=>'Endereço completo da empresa', 'class'=>'large']) }}</p>
				<p class="error">{{ $errors->first('endereco') }}</p>
			</span>	
		</div>
		<div class="blc">
			
			<span>
				<p>{{ Form::text('cep', $user->cep,['placeholder'=>'CEP','id'=>'cep']) }}</p>
				<p class="error">{{ $errors->first('cep') }}</p>
			</span>	
		
			
			<span>
				<p>{{ Form::email('email', $user->email,['placeholder'=>'E-mail']) }}</p>
				<p class="error">{{ $errors->first('email') }}</p>
			</span>	
		</div>
		<div class="blc">
			
			<span>
				<p>{{ Form::text('tel', $user->tel,['placeholder'=>'Telefone fixo (ddd+número)','id'=>'tel']) }}</p>
				<p class="error">{{ $errors->first('tel') }}</p>
			</span>	
		
			<span>
				<p>{{ Form::text('cel', $user->cel,['placeholder'=>'Celular (ddd+número)','id'=>'cel']) }}</p>
				<p class="error">{{ $errors->first('cel') }}</p>
			</span>	
		</div>
		<div class="blc"><p class="ft12">*Todos os campos são obrigatórios</p></div>
		<div class="blc">
			{{ Form::submit('Editar',['class'=>'bt blue bt_large']) }}
			<span style="float:none; margin-left:20px;" class="wrap_loader"></span>
		</div>
		{{ Form::close() }}
	</div>
@stop