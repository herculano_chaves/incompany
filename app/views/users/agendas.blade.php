@extends('layouts.backend')



@section('content')

	@include('layouts.backendmenu')

	<div class="content">

		<div class="panel">

			@include('layouts.notifications')

			<div class="panel-heading">

				<strong>Agendas do usuario {{ $user->name }}</strong>

			</div>

			

			<div class="panel-body">
				@if($user->agendas->count() > 0)
				<table class="table table-striped table-bordered">

                    <thead>

                        <tr>
							<th>Curso</th>
                            <th>Data</th>

                            <th>Horário</th>

                            <th>Email resposável</th>

                            <th>Endereço</th>

                            <th>Telefone/Celular</th>

                          	<th>Ver alunos</th>
                          	<th>Status</th>
                          	<th>Instrutor</th>
                          	<th>Cancelar</th>

                        </tr>

                    </thead>

                    <tbody>

                        @foreach($user->agendas as $agenda)

							  <tr>
								 <td valign="middle">{{ $agenda->curso->consumer->name }}</td>
							    <td valign="middle">{{ Helper::ConverterBR($agenda->date_ini, true) }}</td>

							    <td valign="middle">De {{ $agenda->periodo->hour_ini }}h às {{$agenda->periodo->hour_ini}}h</td>

							    <td valign="middle">{{ $agenda->email }}</td>

							    <td valign="middle">{{ $agenda->endereco }}</td>

							    <td valign="middle">{{ $agenda->tel }} / {{ $agenda->cel }}</td>

							    <td valign="middle"><a href="{{route('user/employers/show',['id'=>$agenda->id])}}">Alunos</a></td>
							    <td valign="middle"> {{$agenda->statusText()}} </td>
							    <td valign="middle"> {{$agenda->curso->instructor->name}} </td>

							    <td valign="middle"><a href="{{route('admin/account/agenda/cancelar', ['id'=>$agenda->id]) }}">Cancelar</a></td>

							  </tr>

  						@endforeach

                    </tbody>

                </table>
				@else
					<p>A agenda deste usuário, até o momento se encontra vazia</p>
				@endif
               
				{{link_to('admin/empresas','Voltar')}}
			</div>

		

	</div>

@stop