@section('content')
	<div id="grid_cursos" class="reset_pass">
	{{ Form::open(array('route' => array('user/set-password'))) }}
 
 	{{ Form::input('hidden','code',$code) }}
 	{{ Form::input('hidden','user_id',$id) }}

	  <p>
		  {{ Form::label('password', 'Crie sua nova senha') }}
		  {{ Form::password('password') }}
		  <span class="error">{{ $errors->first('password') }}</span>
	  </p> 
	  <p>
		  {{ Form::label('password_confirmation', 'Confirme sua nova senha') }}
		  {{ Form::password('password_confirmation') }}
		  <span class="error">{{ $errors->first('password_confirmation') }}</span>
	  </p>
	 	 
	  <p>{{ Form::submit('Enviar',['class'=>'bt blue bt_large']) }}</p>
	 
	{{ Form::close() }}
	</div>
@stop