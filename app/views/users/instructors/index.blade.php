@extends('layouts.backend')

@section('content')
	@include('layouts.backendmenu')

	<div class="content">
		<div class="panel">
			<div class="panel-heading">
				<strong>Instrutores</strong>
			</div>

			<div class="panel-body">	
				@include('layouts.notifications-list')
			     
                @include('users.instructors.addform')

			<table class="table table-striped table-bordered">

                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Email</th>
                            <th>CPF</th>
                            <th>Empresa</th>
                            <th>Cargo</th>
                            <th>Endereço</th>
                            <th>Tel</th>
                            <th>Cel</th>
                            <th>Cursos agendados</th>
                            <th>Editar</th>
                            <th>Apagar</th>
                        </tr>

                    </thead>
                    <tbody>
                    	@foreach($users as $user)
                    	<tr>
                    		<td valign="middle">{{$user->name}}</td>
                    		<td valign="middle">{{$user->email}}</td>
                    		<td valign="middle">{{$user->cpf}}</td>
                    		<td valign="middle">{{$user->empresa}}</td>
                    		<td valign="middle">{{$user->cargo}}</td>
                    		<td valign="middle">{{$user->endereco}}</td>
                    		<td valign="middle">{{$user->tel}}</td>
                    		<td valign="middle">{{$user->cel}}</td>
                             <td valign="middle">
                               
                                <a href="{{route('instructor/cursos',['id'=>$user->id, 'caracter_id'=>1])}}">Ver cursos</a>
                                <span> | </span>
                                <a href="{{route('instructor/palestras',['id'=>$user->id,'caracter_id'=>2])}}">Ver palestras</a>
                               
                            </td>
                             <td valign="middle"><a href="{{route('instructor/edit',['id'=>$user->id])}}">Editar</a></td>
                    		 <td valign="middle"><a href="{{route('instructor/delete',['id'=>$user->id])}}">Apagar</a></td>
                    	</tr>
                    	@endforeach
                    </tbody>
            </table>
           

@stop