{{ Form::open(['action'=>'instructor/create', 'method'=>'post']) }}

<table class="table table-striped table-bordered">

<thead>

    <tr>

        <th colspan="2"><h4>Adicionar Instrutor</h4></th>

       

    </tr>



</thead>

<tbody>

   

    <tr>

        <td>{{ Form::label('name','Nome:') }}</td><td>{{ Form::text('name',null,['class'=>'large'])  }}</td>

    </tr>

    <tr>

        <td>{{ Form::label('email','Email:') }}</td><td>{{ Form::email('email') }}</td>

    </tr>
     <tr>

        <td>{{ Form::label('password','Senha:') }}</td><td>{{ Form::password('password') }}</td>

    </tr>

    <tr>

        <td>{{ Form::label('cpf','CPF:') }}</td><td>{{ Form::text('cpf') }}</td>

    </tr>

    <tr>

        <td>{{ Form::label('empresa','Empresa:') }}</td><td>{{ Form::text('empresa') }}</td>

    </tr>

    <tr>

        <td>{{ Form::label('cargo','Cargo:') }}</td><td>{{ Form::text('cargo') }}</td>

    </tr>

    <tr>

        <td>{{ Form::label('endereco','Endereço: ') }}</td><td>{{ Form::text('endereco') }}</td>

    </tr>

    <tr>

        <td>{{ Form::label('cep','CEP: ') }}</td><td>{{ Form::text('cep') }}</td>

    </tr>

    <tr>

        <td>{{ Form::label('tel','Telefone: ') }}</td><td>{{ Form::text('tel') }}</td>

    </tr>

    <tr>

        <td>{{ Form::label('cel','Celular: ') }}</td><td>{{ Form::text('cel') }}</td>

    </tr>
   

     {{--  <tr>

        <td>{{ Form::label('name','Cursos responsável (tipo): ') }}</td>

        <td>

            @foreach($tipos as $t)

                <p>{{ Form::checkbox('tipo', $t->id) }} {{ $t->name }}</p>

            @endforeach

        </td>

    </tr> --}}

    <tr>

        <td colspan="2">{{ Form::submit('Enviar') }}</td>

    </tr>

    

</tboby>

</table>

{{ Form::close() }}