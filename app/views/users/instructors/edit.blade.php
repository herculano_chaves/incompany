@extends('layouts.backend')



@section('content')

	{{$menu}}

	<div class="content">



		<div class="panel">

			<div class="panel-heading">

				<strong>Instrutor</strong>

			</div>

			<div class="panel-body">	

				@include('layouts.notifications-list')

		{{ Form::open(['route'=>'user/edit', 'method'=>'post']) }}

		{{ Form::input('hidden', 'user_id', $user->id) }}

		{{ Form::input('hidden', 'is_admin', $user->id) }}

		<div class="blc">

			<span>

				<p>Nome: </p>

				<p>{{ Form::text('name', $user->name, ['placeholder'=>'Nome completo (Responsável pela empresa)']) }}</p>

				<p class="error">{{ $errors->first('name') }}</p>

			</span>

			<span>

				<p>CPF: </p>

				<p>{{ Form::text('cpf', $user->cpf, ['placeholder'=>'CPF','id'=>'cpf']) }}</p>

				<p class="error">{{ $errors->first('cpf') }}</p>

			</span>	

		</div>

		<div class="blc">

			<span>

				<p>Profissão: </p>

				<p>{{Form::select('profissao',[''=>'Profissão', 'Instalador de drywall'=>'Instalador de drywall', 'pintor'=>'pintor', 'marceneiro'=>'marceneiro', 'instalador de elétrica'=>'instalador de elétrica', 'instalador de hidráulica'=>'instalador de hidráulica', 'pedreiro'=>'pedreiro', 'engenheiro civil'=>'engenheiro civil', 'técnico em edificações'=>'técnico em edificações', 'arquiteto'=>'arquiteto', 'design de interiores'=>'design de interiores', 'projetista'=>'projetista', 'estudante de arquitetura'=>'estudante de arquitetura', 'estudante de engenharia civil'=>'estudante de engenharia civil', 'estudante de design de interiores'=>'estudante de design de interiores', 'estudante de técnica em edificações'=>'estudante de técnica em edificações', 'mestre de obras'=>'mestre de obras', 'assentador cerâmico'=>'assentador cerâmico', 'decorador'=>'decorador', 'paisagista'=>'paisagista', 'outros'=>'outros'], $user->profissao,['id'=>'profissao'])}}</p>

				<p class="error">{{ $errors->first('profissao') }}</p>

			</span>	

			<span style="display:none;" id="informe">

				<p>{{ Form::text('informe','',['placeholder'=>'Caso não tenha sua profissão listada ao lado']) }}</p>

				<p class="error">{{ $errors->first('informe') }}</p>

			</span>	

		</div>

		<div class="blc">

			<span>

				<p>Empresa: </p>

				<p>{{ Form::text('empresa', $user->empresa,['placeholder'=>'Empresa']) }}</p>

				<p class="error">{{ $errors->first('empresa') }}</p>

			</span>	

		

			<span>

				<p>Cargo: </p>

				<p>{{ Form::text('cargo', $user->cargo,['placeholder'=>'Cargo']) }}</p>

				<p class="error">{{ $errors->first('cargo') }}</p>

			</span>	

		</div>

		<hr/>

		<div class="blc">

			

			<span>

				<p>Endereço: </p>

				<p>{{ Form::text('endereco', $user->endereco,['placeholder'=>'Endereço completo da empresa', 'class'=>'large']) }}</p>

				<p class="error">{{ $errors->first('endereco') }}</p>

			</span>	

		</div>

		<div class="blc">

			

			<span>

				<p>CEP: </p>

				<p>{{ Form::text('cep', $user->cep,['placeholder'=>'CEP','id'=>'cep']) }}</p>

				<p class="error">{{ $errors->first('cep') }}</p>

			</span>	

		

			

			<span>

				<p>Email: </p>

				<p>{{ Form::email('email', $user->email,['placeholder'=>'E-mail']) }}</p>

				<p class="error">{{ $errors->first('email') }}</p>

			</span>	

		</div>

		<div class="blc">

			

			<span>

				<p>Telefone: </p>

				<p>{{ Form::text('tel', $user->tel,['placeholder'=>'Telefone fixo (ddd+número)','id'=>'tel']) }}</p>

				<p class="error">{{ $errors->first('tel') }}</p>

			</span>	

		

			<span>

				<p>Celular: </p>

				<p>{{ Form::text('cel', $user->cel,['placeholder'=>'Celular (ddd+número)','id'=>'cel']) }}</p>

				<p class="error">{{ $errors->first('cel') }}</p>

			</span>	

			<span>

				<p>Nova senha: </p>

				<p>{{ Form::password('password') }}</p>

				<p class="error">{{ $errors->first('password') }}</p>

			</span>	

		</div>

		<div class="blc"><p class="ft12">*Todos os campos são obrigatórios</p></div>

		<div class="blc">

			{{ Form::submit('Salvar',['class'=>'bt blue bt_large']) }}

			<span style="float:none; margin-left:20px;" class="wrap_loader"></span>

		</div>

		{{ Form::close() }}
		
		@if(Route::current()->getPrefix() == 'admin')
		<div class="row">
			<div class="col-md-12">
				<h3>Adicionar Estados disponíveis para este instrutor</h3>
				{{ Form::open(['route'=>array('instructor.state.store', $user->instrutor->id)]) }}	
					<div class="row">
					  	<div class="col-md-6">
					  		 {{ Form::select('state_id', [''=>'Estados']+$states, Input::get('state_id'), ['class'=>'form-control','id'=>'estado_id']) }}
		                
					  	</div>
					</div> 
			
					<div class="row">
						<div class="col-md-6">
							{{ Form::button('<i class="fa fa-plus"></i> Adicionar', ['type'=>'submit', 'class'=>'btn btn-info']) }}
						</div>
					</div> 
				{{ Form::close() }}
			</div>
		</div>

		
		@if($estados_disp->count() > 0)
		<table class="table-bordered table-striped table">
			<tr>
				<th>Estado</th>
				<th>Apagar</th>
			</tr>

			@foreach($estados_disp as $e)
				<tr>
					<td>({{ $e->uf }}) {{ $e->name }}</td>
					<td>
						{{ Form::open(['route'=>array('instructor.state.delete',$user->instrutor->id), 'method'=>'delete']) }}
						{{ Form::hidden('state_id', $e->id) }}
						{{ Form::button('<i class="fa fa-trash"></i>', ['type'=>'submit', 'class'=>'btn btn-danger']) }}
						{{ Form::close() }}
					</td>
				</tr>
			@endforeach
		</table>
		@else
			<p>Não tem Estado cadastrado.</p>
		@endif



		@endif

		@stop