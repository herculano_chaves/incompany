@extends('layouts.base')

@section('content')
<div id="grid_cursos" class="reset_pass">

 <h2>Crie aqui sua nova senha:</h2>
 @include('layouts.notifications-list')
{{ Form::open(array('route' => array('password.update', $token))) }}
 
  <p>{{ Form::label('email', 'Email') }}
  {{ Form::email('email') }}</p>
 
  <p>{{ Form::label('password', 'Sua nova senha') }}
  {{ Form::password('password') }}</p>
 
  <p>{{ Form::label('password_confirmation', 'Confirme sua senha') }}
  {{ Form::password('password_confirmation') }}</p>
 
  {{ Form::hidden('token', $token) }}
 
  <p>{{ Form::submit('Enviar',['class'=>'bt blue bt_large']) }}</p>
 
{{ Form::close() }}
</div>
@stop