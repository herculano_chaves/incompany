@extends('layouts.base')

@section('content')
<div id="grid_cursos" class="reset_pass">
	<h2>Esqueceu a senha de acesso? Preencha o seu e-mail cadastrado para solicitar nova senha:</h2>

	@include('layouts.notifications')

	{{ Form::open(array('route' => 'password.request')) }}
	 
	  <p> {{ Form::text('email',null,['placeholder'=>'Seu e-mail cadastrado']) }}</p>
	 
	  <p>{{ Form::submit('Enviar',['class'=>'bt blue bt_large']) }}</p>
	 
	{{ Form::close() }}

	{{link_to_route('user/login','< Voltar',null,['class'=>'ft11 u gray','style'=>'float:right;'])}}
</div>
@stop