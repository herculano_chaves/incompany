<?php



/*

|--------------------------------------------------------------------------

| Application Routes

|--------------------------------------------------------------------------

|

| Here is where you can register all of the routes for an application.

| It's a breeze. Simply tell Laravel the URIs it should respond to

| and give it the Closure to execute when that URI is requested.

|

*/

# Site

Route::get('/', ['uses'=>'SiteController@index', 'as'=>'home']);

Route::get('fale-conosco',['uses'=>'SiteController@contact', 'as'=>'site/contact']);

Route::post('fale-conosco',['uses'=>'SiteController@postContact', 'as'=>'site/contact']);



# Login user

Route::get('login', ['as'=>'user/login', 'uses'=>'UsersController@index']);

Route::post('login', ['as'=>'user/login', 'uses'=>'UsersController@login']);

Route::get('logout',['as'=>'user/logout','uses'=>'UsersController@getLogout']);



# Register

Route::get('cadastro', ['as'=>'user/register', 'uses'=>'UsersController@register']);

Route::post('cadastro', ['as'=>'user/register', 'uses'=>'UsersController@post_register']);


# Register Employer
Route::get('cadastro/participante/{agenda_id}/agenda/{share_url}', ['uses' => 'AgendasController@getRegisterEmployer',  'as' => 'cadastro/participante']);
Route::post('cadastro/participante/{agenda_id}/agenda/{share_url}', ['uses' => 'AgendasController@postRegisterEmployer',  'as' => 'post/cadastro/participante']);



# Forgot password

Route::get('password/reset', ['uses' => 'PasswordController@remind',  'as' => 'password.remind']);

Route::post('password/reset', ['uses' => 'PasswordController@request', 'as' => 'password.request']);

Route::get('password/reset/{token}', ['uses' => 'PasswordController@reset', 'as' => 'password.reset']);

Route::post('password/reset/{token}', ['uses' => 'PasswordController@update', 'as' => 'password.update']);



# Confirmação de cadastro

Route::get('confirmar-usuario',['uses'=>'UsersController@updatepass', 'as'=>'user/set-password']);

Route::post('confirmar-usuario',['uses'=>'UsersController@postUpdatePass', 'as'=>'user/set-password']);



# Cursos

Route::get('cursos', ['as'=>'cursos/index', 'uses'=>'CursosController@index']);

Route::get('cursos/busca', ['as'=>'cursos/search', 'uses'=>'CursosController@searchall']);

Route::post('cursos/busca', ['as'=>'cursos/search', 'uses'=>'CursosController@search']);
Route::get('cursos/busca/test', ['as'=>'cursos/search/teste', 'uses'=>'CursosController@search']);

Route::post('curso/data',['as'=>'curso/data', 'uses'=>'CursosController@data_confirm']);

Route::post('curso/preconfirma',['as'=>'curso/preconfirma', 'uses'=>'CursosController@preconfirm']);

# Buscas
Route::post('city/search', 'UsersController@searchByState');

# Certificado

Route::get('certificado/{id}/{code}',['as'=>'certify/index', 'uses'=>'AgendasController@employerCertifyGenerate']);
Route::post('certificado/generate',['as'=>'certify/generate', 'uses'=>'AgendasController@employerCertifyAction']);

# Account

Route::group(['prefix'=>'usuario','before'=>'auth'], function(){



	Route::get('agenda',['as'=>'user/account/agenda', 'uses'=>'CursosController@agenda']);

	Route::get('agenda/cancelar/{id}',['as'=>'user/account/agenda/cancelar', 'uses'=>'AgendasController@cancelar']);

	Route::get('agenda/desfazer/{id}',['as'=>'user/account/agenda/desfazer', 'uses'=>'AgendasController@desfazer']);

	Route::get('conta',['as'=>'user/account/index', 'uses'=>'UsersController@show']);

	Route::post('conta',['as'=>'user/edit', 'uses'=>'UsersController@update']);

	Route::post('agenda/update',['as'=>'agenda/update', 'uses'=>'AgendasController@update']);

	Route::get('agenda/{a}/aluno/{id}/certify', ['as'=>'user/employers/certify', 'uses'=>'AgendasController@updateEmployerCertified']);

});



# Curso

Route::group(['prefix'=>'curso','before'=>'auth'], function(){

	Route::get('confirmacao', ['as'=>'cursos/create', 'uses'=>'CursosController@create']);

	Route::post('confirmacao', ['as'=>'cursos/store', 'uses'=>'CursosController@store']);

	Route::get('detalhes/{id}',['as'=>'agenda/show/detalhes', 'uses'=>'AgendasController@showDetalhes']);

	Route::get('delete/employer/{id}', ['as'=>'employer/delete', 'uses'=>'AgendasController@destroyEmployer']);

});



# Admin

Route::get('admin', ['as'=>'backend/login', 'uses'=>'UsersController@getAdminLogin']);

Route::post('admin', ['as'=>'backend/login', 'uses'=>'UsersController@postAdminLogin']);

Route::group(['before'=>'auth.admin', 'prefix'=>'admin'], function(){

	Route::get('cursos/{caracter}', ['as'=>'admin/cursos', 'uses'=>'CursosController@adminCursos']);

	Route::get('cursos/{caracter}/create', ['as'=>'admin.cursos.create', 'uses'=>'CursosController@adminCursosCreate']);

	Route::post('cursos', ['as'=>'curso/generate', 'uses'=>'CursosController@generate']);

	Route::get('curso/edit/{id}', ['as'=>'curso/edit', 'uses'=>'CursosController@edit']);

	Route::post('curso/update', ['as'=>'curso/update', 'uses'=>'CursosController@update']);
	Route::get('cursos/edit/actions/{caracter}',['as'=>'cursos.edit.actions', 'uses'=>'CursosController@editActions']);
	Route::post('cursos/edit/actions/{caracter}',['as'=>'cursos.update.actions', 'uses'=>'CursosController@updateActions']);

	Route::get('curso/destroy/{caracter}/{id}', ['as'=>'curso/destroy', 'uses'=>'CursosController@destroy']);

	# Empresas
	Route::get('empresas',['as'=>'users/index','uses'=>'UsersController@listagem']);
	Route::get('empresa/{id}/cursos',['as'=>'user/cursos', 'uses'=>'AgendasController@agendaEmpresa']);
	Route::get('agenda/{id}/alunos', ['as'=>'user/employers/show', 'uses'=>'AgendasController@getUserEmployers']);
	Route::get('agenda/cancelar/{id}',['as'=>'admin/account/agenda/cancelar', 'uses'=>'AgendasController@cancelar']);

	#Intrutores
	Route::get('instrutores', ['as'=>'instructors/index', 'uses'=>'UsersController@instructorsIndex']);
	Route::post('instrutores', ['as'=>'instructor/create', 'uses'=>'UsersController@createInstructor']);
	Route::get('instrutores/{id}/editar', ['as'=>'instructor/edit', 'uses'=>'UsersController@editInstructor']);
	Route::get('instrutor/{id}/cursos/{caracter_id?}',['as'=>'instructor/cursos', 'uses'=>'CursosController@adminInstrutoresIns']);
	Route::get('instrutor/{id}/palestras/{caracter_id?}',['as'=>'instructor/palestras', 'uses'=>'CursosController@adminInstrutoresIns']);
	Route::get('instrutor/{id}/delete',['as'=>'instructor/delete', 'uses'=>'UsersController@deleteInstrutor']);
	Route::get('agenda/{id}/alunos', ['as'=>'employers/show/admin', 'uses'=>'AgendasController@getEmployers']);
	Route::get('filter/instructor/{id}/courses/{caracter_id?}', ['as'=>'filter/instructor/courses', 'uses'=>'CursosController@adminInstrutoresIns']);
	
	# Instrutores/Estados
	Route::post('{id}/meus-estados', ['as'=>'instructor.state.store', 'uses'=>'InstructorsController@localeStateStore']);
	Route::delete('{id}/meus-estados', ['as'=>'instructor.state.delete', 'uses'=>'InstructorsController@localeStateDelete']);
	
	# Atividades
	Route::get('atividades',['as'=>'atividades/index', 'uses'=>'CursosController@indexAtividades']);
	Route::get('atividades/delete/{id}',['as'=>'atividade/delete', 'uses'=>'CursosController@deleteAtividade']);
	Route::get('atividades/edit/{id}',['as'=>'atividade/edit', 'uses'=>'CursosController@editAtividade']);
	Route::post('atividades/edit/',['as'=>'atividade/update', 'uses'=>'CursosController@updateAtividade']);
	Route::post('atividades',['as'=>'atividade/create', 'uses'=>'CursosController@createAtividade']);

	# Tipos
	Route::get('tipos',['as'=>'tipos/index', 'uses'=>'CursosController@indexTipos']);
	Route::get('tipos/delete/{id}',['as'=>'tipo/delete', 'uses'=>'CursosController@deleteTipo']);
	Route::get('tipos/edit/{id}',['as'=>'tipo/edit', 'uses'=>'CursosController@editTipo']);
	Route::post('tipos/edit/',['as'=>'tipo/update', 'uses'=>'CursosController@updateTipo']);
	Route::post('tipos',['as'=>'tipo/create', 'uses'=>'CursosController@createTipo']);

	# Cidades
	Route::get('cidades',['as'=>'cidades/index', 'uses'=>'CursosController@indexCidades']);
	Route::get('cidades/delete/{id}',['as'=>'cidade/delete', 'uses'=>'CursosController@deleteCidade']);
	Route::post('cidades',['as'=>'cidade/create', 'uses'=>'CursosController@createCidade']);
	Route::get('cidades/edit/{id}',['as'=>'cidade/edit', 'uses'=>'CursosController@editCidade']);
	Route::post('cidades/edit/',['as'=>'cidade/update', 'uses'=>'CursosController@updateCidade']);

	# Datas
	Route::get('data-center', ['as'=>'data/index', 'uses'=>'DatasController@index']);
	Route::get('data/download/participantes', ['before'=>'admin.permission', 'as'=>'data/employers', 'uses'=>'DatasController@employers']);

	# Disponibilidade
	Route::get('disponibilidade/curso/{id}',['as'=>'instructor/disponibilidade','uses'=>'CursosController@avaliableCurso']);
	Route::post('disponibilidade/curso',['as'=>'instructor/disponibilidade/edit','uses'=>'CursosController@avaliableCursoEdit']);
	Route::get('alter/disp/{c}', ['as'=>'alter/disp', 'uses'=>'CursosController@alterDisp']);
	Route::post('update/disp/all', ['as'=>'update/disp/all', 'uses'=>'CursosController@alterDispAll']);

	# Consumers
	Route::resource('eventos', 'ConsumersController');
	
	# Actions
	Route::post('cursos/action', ['as'=>'cursos/action', 'uses'=>'CursosController@actions']);

	# Logs
	Route::get('logs', ['as'=>'logs/index', 'uses'=>'AdminlogsController@index']);
});



#Instructors Admin

Route::get('instrutores', ['as'=>'instructor/login', 'uses'=>'UsersController@getInstructorLogin']);

Route::post('instrutores', ['as'=>'instructor/login', 'uses'=>'UsersController@postAdminLogin']);

Route::group(['before'=>'auth.instructor', 'prefix'=>'instrutores'], function(){

	# Calendario
	Route::get('calendario', ['as'=>'admin.instructors.calendar', 'uses'=>'CursosController@calendar']);
	Route::post('calendario/guardar', ['as'=>'admin.instructors.calendar.save', 'uses'=>'CursosController@calendarSaveDays']);
	Route::get('calendario/cancela/edicao',  ['as'=>'admin.instructors.calendar.cancel', 'uses'=>'CursosController@cancelDisp']);

	Route::get('{caracter_id?}/disponiveis', ['as'=>'admin/cursos/instrutor', 'uses'=>'CursosController@adminInstrutoresCursos']);

	Route::get('agendas/{status?}', ['as'=>'admin/instructors', 'uses'=>'CursosController@adminInstrutores']);

	# Buscar cursos por datas
	Route::post('buscar/cursos/por/data', 'CursosController@searchInByDate');

	# retira disponibiidades
	Route::get('apaga/disponibiidade/{id}', ['as'=>'available.delete', 'uses'=>'CursosController@delDisp']);
	Route::get('apaga/disponibiidade/data/{date}', ['as'=>'available.delete.all', 'uses'=>'CursosController@delDispAll']);

	# Altera horarios (edita)
	Route::post('editar/horarios', ['as'=>'update.availables.hour', 'uses'=>'CursosController@InstructorAlterDispHour']);

	# Buscar em minhas agendas
	Route::get('agendas/{status?}', ['as'=>'admin/instructors/search', 'uses'=>'CursosController@adminInstrutores']);

	# Página de evento do instrutor
	Route::get('eventos', ['as'=>'instructor.events', 'uses'=>'InstructorsController@events']);
	
	# Cidades do instrutor
	Route::get('minhas-cidades', ['as'=>'instructor.locale.index', 'uses'=>'InstructorsController@localeIndex']);
	
	Route::post('{id}/minhas-cidades', ['as'=>'instructor.locale.store', 'uses'=>'InstructorsController@localeStore']);
	Route::delete('{id}/minhas-cidades', ['as'=>'instructor.locale.delete', 'uses'=>'InstructorsController@localeDelete']);

	Route::get('curso/{id}/agenda', ['as'=>'agenda/show', 'uses'=>'AgendasController@show']);

	Route::get('agenda/{id}/alunos', ['as'=>'employers/show', 'uses'=>'AgendasController@getEmployers']);

	Route::get('agenda/{a}/aluno/{id}/presence', ['as'=>'employers/presence', 'uses'=>'AgendasController@updateEmployerPresence']);

	Route::get('agenda/{a}/aluno/{id}/certify', ['as'=>'employers/certify', 'uses'=>'AgendasController@updateEmployerCertified']);

	Route::post('agenda/{id}/alunos/editar',['as'=>'edit/employers', 'uses'=>'AgendasController@updateAllEmployers']);

	Route::get('agenda/{id}/finish', ['as'=>'agenda/finish', 'uses'=>'AgendasController@finish']);
	Route::get('agenda/{id}/desfazer', ['as'=>'agenda/back', 'uses'=>'AgendasController@backFin']);

	# Confirmar
	Route::get('agenda/{id}/confirm', ['as'=>'agenda/confirmar', 'uses'=>'AgendasController@getConfirm']);
	Route::get('agenda/{id}/confirm/{action}', ['as'=>'agenda/confirmar/post', 'uses'=>'AgendasController@postConfirm']);

	# Editar instrutor 
	Route::get('editar/{id}', ['as'=>'instructor/edit/instructor', 'uses'=>'UsersController@editInstructor']);

	# Buscas
	Route::get('filter/courses/{caracter_id?}', ['as'=>'filter/instructor/courses', 'uses'=>'CursosController@adminInstrutoresCursos']);

	# Actions
	Route::post('cursos/action', ['as'=>'instructor/cursos/action', 'uses'=>'CursosController@InstructorActions']);
	
	# Avaliabilidade
	Route::get('disponibilidade/curso/{id}',['as'=>'instructor/disponibilidade/ins','uses'=>'CursosController@InstructorAvaliableCurso']);
	Route::post('disponibilidade/curso',['as'=>'instructor/disponibilidade/edit/ins','uses'=>'CursosController@InstructorAvaliableCursoEdit']);
	Route::get('alter/disp/{c}', ['as'=>'instructor/alter/disp', 'uses'=>'CursosController@InstructorAlterDisp']);
	Route::post('update/disp/all', ['as'=>'instructor/update/disp/all', 'uses'=>'CursosController@InstructorAlterDispAll']);
});

